/**
 * Created by Kirill on 12/26/2018.
 */
// Definition for singly-linked list.
class ListNode(var `val`: Int = 0) {
    var next: ListNode? = null
}
