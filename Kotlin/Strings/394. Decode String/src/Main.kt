/**
 * Created by Kirill on 12/28/2018.
 */
import java.util.Stack


class Main {

    fun decodeString(s: String): String {

        var res = ""
        val countStack = Stack<Int>()
        val resStack = Stack<String>()
        var i = 0
        while (i < s.length) {
            if (Character.isDigit(s[i])) {
                var count = 0
                while (Character.isDigit(s[i])) {
                    count = 10 * count + (s[i] - '0')
                    i++
                }
                countStack.push(count)
            } else if (s[i] === '[') {
                resStack.push(res)
                res = ""
                i++
            } else if (s[i] === ']') {
                val temp = StringBuilder(resStack.pop())
                val repeatTimes = countStack.pop()
                for (i in 0..repeatTimes - 1) {
                    temp.append(res)
                }
                res = temp.toString()
                i++
            } else {
                res += s[i++]
            }
        }
        return res
    }
}