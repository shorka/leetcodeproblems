import org.junit.jupiter.api.Assertions.*

/**
 * Created by Kirill on 12/28/2018.
 */
internal class MainTest {

    @org.junit.jupiter.api.Test
    fun decodeString_3a2bc() {

        assertEquals("aaabcbc", Main().decodeString("3[a]2[bc]"))
    }

    @org.junit.jupiter.api.Test
    fun decodeString_accaccacc() {

        assertEquals("accaccacc", Main().decodeString("3[a2[c]]"))
    }


}