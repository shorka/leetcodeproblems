import org.junit.jupiter.api.Assertions.*

/**
 * Created by Kirill on 12/21/2018.
 */
internal class MainTest {

    @org.junit.jupiter.api.Test
    fun isPalindrome_deified() {
        assertEquals(true, Main().isPalindrome("deified"))
    }

    @org.junit.jupiter.api.Test
    fun isPalindrome_civic() {
        assertEquals(true, Main().isPalindrome("civic"))
    }

    @org.junit.jupiter.api.Test
    fun isPalindrome_kyrylo() {
        assertEquals(false, Main().isPalindrome("kyrylo"))
    }

    @org.junit.jupiter.api.Test
    fun isPalindrome_Empty() {
        assertEquals(true, Main().isPalindrome(""))
    }

    @org.junit.jupiter.api.Test
    fun isPalindrome_Maam() {
        assertEquals(true, Main().isPalindrome("maam"))
    }

    @org.junit.jupiter.api.Test
    fun isPalindrome_Madom() {
        assertEquals(false, Main().isPalindrome("madom"))
    }
}