/**
 * Created by Kirill on 12/21/2018.
 */
class Main {

    fun main(args: Array<String>) {

    }

    fun isPalindrome2(word: String): Boolean {

        val len = word.length
        for (i in 0..len / 2) {

            val j = len - i - 1
            if (i >= j) break

            if (word.get(i) != word.get(j))
                return false
        }

        return true
    }

    fun isPalindrome(word: String): Boolean {

        var head = 0
        var tail = word.length - 1

        while (head <= tail) {
            if (word[head] != word[tail])
                return false

            head++
            tail--
        }
        return true
    }
}