import org.junit.jupiter.api.Assertions.*

/**
 * Created by Kirill on 12/24/2018.
 */
internal class SolutionTest {

    @org.junit.jupiter.api.Test
    fun numberToWords_123() {

        assertEquals("One Hundred Twenty Three", Solution().numberToWords(123))
    }

    @org.junit.jupiter.api.Test
    fun numberToWords_1234567() {

        assertEquals("One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven",
                Solution().numberToWords(1234567))
    }

    @org.junit.jupiter.api.Test
    fun numberToWords_12345() {

        assertEquals("Twelve Thousand Three Hundred Forty Five",
                Solution().numberToWords(12345))
    }

    @org.junit.jupiter.api.Test
    fun numberToWords_1234567891() {

        assertEquals("One Billion Two Hundred Thirty Four Million Five Hundred Sixty Seven Thousand Eight Hundred Ninety One",
                Solution().numberToWords(1234567891))
    }

}