import org.junit.jupiter.api.Assertions.*

/**
 * Created by Kirill on 12/21/2018.
 */
internal class MainTest {

    @org.junit.jupiter.api.Test
    fun isPalindrome_121() {
        assertEquals(true, Main().isPalindrome(121))
    }

    @org.junit.jupiter.api.Test
    fun isPalindrome_5665() {
        assertEquals(true, Main().isPalindrome(5665))
    }
}