/**
 * Created by Kirill on 12/21/2018.
 */
class Main {

    fun isPalindrome(x: Int): Boolean {
        var xNumb = x
        if (x < 0 || (x % 10 == 0 && x != 0))
            return false

        var revNumb = 0

        while (xNumb > revNumb) {
            revNumb = revNumb * 10 + xNumb % 10
            xNumb /= 10
        }

        return xNumb == revNumb || xNumb == revNumb/10
    }
}