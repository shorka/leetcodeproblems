/**
 * Created by Kirill on 12/21/2018.
 */
class Main {

    fun twoSum(nums: IntArray,target: Int) : IntArray{

        for (i in nums.indices){
            for (j in i+1..nums.size-1){
                if (nums[i] + nums[j] == target)
                    return intArrayOf(i,j)
            }
        }
        return  intArrayOf()
    }
}