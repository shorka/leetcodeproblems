import org.junit.jupiter.api.Assertions.*

/**
 * Created by Kirill on 12/21/2018.
 */
internal class MainTest {

    @org.junit.jupiter.api.Test
    fun twoSum_9() {
        assertArrayEquals(intArrayOf(0, 1),
                Main().twoSum(intArrayOf(2, 7, 11, 15), 9))
    }

    @org.junit.jupiter.api.Test
    fun twoSum_6() {
        assertArrayEquals(intArrayOf(1, 2),
                Main().twoSum(intArrayOf(3, 2, 4), 6))
    }
}