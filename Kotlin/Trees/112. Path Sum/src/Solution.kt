import java.util.LinkedList

/**
 * Created by Kirill on 12/24/2018.
 */
class Solution {

    fun hasPathSum(root: TreeNode?, sum: Int): Boolean {

        if (root == null)
            return false

        var sumRes = sum
        sumRes -= root.nodeVal
        if (root.left == null && root.right == null)
            return sumRes == 0
        return hasPathSum(root.left, sumRes) || hasPathSum(root.right, sumRes)
    }

    fun hasPathSum2(root: TreeNode?, sum: Int): Boolean {
        if (root == null) return false

        val listNodes = mutableListOf<TreeNode>()
        val listSum = mutableListOf<Int>()
        listNodes.add(root)
        listSum.add(sum - root.`nodeVal`)

        var node: TreeNode
        var curr_sum: Int
        while (!listNodes.isEmpty()) {
            node = listNodes.removeAt(listNodes.size - 1)
            curr_sum = listSum.removeAt(listSum.size - 1)
            if (node.right == null && node.left == null && curr_sum == 0)
                return true

            node.right?.let {
                listNodes.add(it)
                listSum.add(curr_sum - it.nodeVal)
            }

            node.left?.let {
                listNodes.add(it)
                listSum.add(curr_sum - it.nodeVal)
            }
        }
        return false
    }


}