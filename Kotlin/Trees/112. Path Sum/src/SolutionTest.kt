import org.junit.jupiter.api.Assertions.*

/**
 * Created by Kirill on 12/24/2018.
 */
internal class SolutionTest {

    private fun getNode(): TreeNode{
        val node5 = TreeNode(5)
        val node8 = TreeNode(8)
        val node11 = TreeNode(11)
        val node13 = TreeNode(13)
        val node4 = TreeNode(4)
        val node4_2 = TreeNode(4)
        val node1 = TreeNode(1)
        val node7 = TreeNode(7)
        val node2 = TreeNode(2)

        node5.left = node4
        node5.right = node8

        node4.left = node11

        node11.left = node7
        node11.right = node2

        node8.right = node4_2
        node8.left = node13

        node4_2.right = node1

        return node5
    }

    @org.junit.jupiter.api.Test
    fun hasPathSum() {

        BTreePrinter().printNode<Int>(getNode())
        assertEquals(true, Solution().hasPathSum(getNode(),22))
    }

    @org.junit.jupiter.api.Test
    fun hasPathSum_2() {

        BTreePrinter().printNode<Int>(getNode())
        assertEquals(true, Solution().hasPathSum2(getNode(),22))
    }
}