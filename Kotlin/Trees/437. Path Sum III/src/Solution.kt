/**
 * Created by Kirill on 12/25/2018.
 */


class Solution {

    fun pathSum(root: TreeNode?, sum: Int): Int {

        if (root == null)
            return 0;
        val a = pathSumFrom(root, sum)
        val b = pathSum(root.left, sum)
        val c = pathSum(root.right, sum)
        return a + b + c;
    }

    private fun pathSumFrom(node: TreeNode?, sum: Int): Int {
        if (node == null)
            return 0
        val qty = if (node.nodeVal == sum) 1 else 0

        val sumNew = sum - node.nodeVal
        val left = pathSumFrom(node.left, sumNew)
        val right = pathSumFrom(node.right, sumNew)
        return qty + left + right
    }

    fun pathSumIter(root: TreeNode?, sum: Int): Int {

        val preSum = mutableMapOf<Int, Int>(0 to 1)
        return helper(root, 0, sum, preSum)
    }

    fun helper(root: TreeNode?, currSum: Int, target: Int, preSum: MutableMap<Int, Int>): Int {
        if (root == null)
            return 0
        var currSum = currSum
        currSum += root.nodeVal
        var res = 0

        if (preSum.containsKey(currSum - target)) {
            res = preSum.get(currSum - target) ?: 0;
        }

        if (!preSum.containsKey(currSum))
            preSum.put(currSum, 1);
        else
            preSum.put(currSum, (preSum.get(currSum) ?: 0) + 1);


        res += helper(root.left, currSum, target, preSum);
        res += helper(root.right, currSum, target, preSum);
        preSum.put(currSum, (preSum.get(currSum) ?: 0)-1);
        return res
    }
}