import org.junit.jupiter.api.Assertions.*

/**
 * Created by Kirill on 12/25/2018.
 */
internal class SolutionTest {

    private fun getNode(): TreeNode{

        val node10 = TreeNode(10)
        val node5 = TreeNode(5)
        val nodeMinus3 = TreeNode(-3)
        val node3 = TreeNode(3)
        val node2 = TreeNode(2)
        val node11 = TreeNode(11)
        val node3_2 = TreeNode(3)
        val nodeMinus2 = TreeNode(-2)
        val node1 = TreeNode(1)

        node10.left = node5
        node10.right = nodeMinus3

        nodeMinus3.right = node11

        node5.right = node2
        node5.left = node3

        node3.left = node3_2
        node3.right = nodeMinus2

        node2.right = node1



        return node10
    }

    @org.junit.jupiter.api.Test
    fun pathSum() {
        BTreePrinter().printNode<Int>(getNode())
        assertEquals(3, Solution().pathSum(getNode(), 8))
    }
    @org.junit.jupiter.api.Test
    fun pathSumIter() {
        BTreePrinter().printNode<Int>(getNode())
        assertEquals(3, Solution().pathSumIter(getNode(), 8))
    }
}