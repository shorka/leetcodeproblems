/**
 * Created by Kirill on 12/24/2018.
 */
class TreeNode(var `nodeVal`: Int = 0) {
    var left: TreeNode? = null
    var right: TreeNode? = null
}