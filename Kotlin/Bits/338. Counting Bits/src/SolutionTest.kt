import org.junit.jupiter.api.Assertions.*

/**
 * Created by Kirill on 12/27/2018.
 */
internal class SolutionTest {

    @org.junit.jupiter.api.Test
    fun countBits() {

        val list1 = intArrayOf(0,1,1,2,1,2)
//        Solution().countBits(5)
        assertArrayEquals(list1, Solution().countBits(5))
    }

}