/**
 * Created by Kirill on 12/27/2018.
 */
class Solution {

    fun countBits(num: Int): IntArray {

        val resArray = IntArray(num+1)
        for (i in 1..num) {

            val shift = i shr 1
            val oper2 = i and 1
            resArray[i] = resArray[shift] + (oper2)
        }

        return resArray
    }
}