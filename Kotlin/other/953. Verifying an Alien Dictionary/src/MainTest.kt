import org.junit.jupiter.api.Assertions.*

/**
 * Created by Kirill on 12/21/2018.
 */
internal class MainTest {

    @org.junit.jupiter.api.Test
    fun isAlienSorted_false_1() {

        val words = arrayOf("word", "world", "row")
        val order = "worldabcefghijkmnpqstuvxyz"
        assertEquals(false, Main().isAlienSorted(words,order))
    }

    @org.junit.jupiter.api.Test
    fun isAlienSorted_false_2() {

        val words = arrayOf("apple", "app")
        val order = "abcdefghijklmnopqrstuvwxyz"
        assertEquals(false, Main().isAlienSorted(words,order))
    }

    @org.junit.jupiter.api.Test
    fun isAlienSorted_true_1() {

        val words = arrayOf("hello","leetcode")
        val order = "hlabcdefgijkmnopqrstuvwxyz"
        assertEquals(true, Main().isAlienSorted(words,order))
    }

    @org.junit.jupiter.api.Test
    fun isAlienSorted_true_2() {

        val words = arrayOf("kuvp", "q")
        val order = "ngxlkthsjuoqcpavbfdermiywz"

        assertEquals(true, Main().isAlienSorted(words,order))
    }
}