/**
 * Created by Kirill on 12/21/2018.
 */
class Main {

    fun isAlienSorted(words: Array<String>, order: String): Boolean {

        val index = IntArray(26)
        for (i in 0..order.length - 1)
            index[order[i] - 'a'] = i

        search@ for (i in 1..words.size - 1) {
            val wordA = words[i - 1]
            val wordB = words[i]

            val minLen = Math.min(wordA.length,  wordB.length)

            for (j in 0..minLen - 1) {

                if (wordA[j] == wordB[j]) continue

                val orderA = index[wordA.get(j) - 'a']
                val orderB = index[wordB.get(j) - 'a']

                if (orderA > orderB) return false

                continue@search
            }

            if (wordA.length > wordB.length) return false
        }

        return true
    }

}