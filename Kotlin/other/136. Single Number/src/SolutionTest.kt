import org.junit.jupiter.api.Assertions.*
import sun.applet.Main

/**
 * Created by Kirill on 12/23/2018.
 */
internal class SolutionTest {

    @org.junit.jupiter.api.Test
    fun singleNumber_4() {
        val array = intArrayOf(4,1,2,1,2)
        assertEquals(4, Solution().singleNumber(array))
    }

    @org.junit.jupiter.api.Test
    fun singleNumber_2() {
        val array = intArrayOf(2,2,1)
        assertEquals(1, Solution().singleNumber(array))
    }

    @org.junit.jupiter.api.Test
    fun singleNumber_4_set() {
        val array = intArrayOf(4,1,2,1,2)
        assertEquals(4, Solution().singleNumber(array))
    }

    @org.junit.jupiter.api.Test
    fun singleNumber_2_set() {
        val array = intArrayOf(2,2,1)
        assertEquals(1, Solution().singleNumber(array))
    }
}