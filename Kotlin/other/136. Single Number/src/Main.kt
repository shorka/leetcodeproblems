import java.util.*

/**
 * Created by Kirill on 12/23/2018.
 */


class Solution {

    fun singleNumber_hashMap(nums: IntArray): Int {

        val map = HashMap<Int, Int>()

        for (item in nums) {
            val value = map[item] ?: 0
            map.put(item, value + 1)
        }

        var res = nums[0]
        map.forEach { key, value ->
            if (value == 1) res = key;
        }
        return res
    }

    fun singleNumber_hashset(nums: IntArray): Int {

        val set = HashSet<Int>()
        val res = nums[0]
        for (item in nums) {
            if (!set.add(item))
                set.remove(item)
        }

        return set.single()
    }


    fun singleNumber(nums: IntArray): Int {

        var res = 0
        for (item in nums) res = res xor item

        return res
    }
}