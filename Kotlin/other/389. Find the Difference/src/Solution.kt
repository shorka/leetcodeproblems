import java.util.*

/**
 * Created by Kirill on 12/23/2018.
 */
class Solution {

    fun findTheDifference(s: String, t: String): Char {

        val map = HashMap<Char, Int>()
        for (ch in s) {
            val value = map[ch] ?: 0
            map.put(ch, value + 1)
        }

        for (ch in t) {
            if (map.containsKey(ch)) {
                val value = map[ch] ?: 0
                if (value <= 1)
                    map.remove(ch)
                else
                    map.put(ch, value - 1)
            } else
                return ch
        }
        return ' '
    }

//    fun findTheDifference_bit(s: String, t: String): Char {
//        val n = t.length
//        var c = t[n - 1]
//        for (i in 0..n - 1 - 1) {
//            c = c c s[i]
//            c = c xor
//        }
//        return c
//    }

}