import org.junit.jupiter.api.Assertions.*

/**
 * Created by Kirill on 12/23/2018.
 */
internal class SolutionTest {

    @org.junit.jupiter.api.Test
    fun findTheDifference() {
        val s = "abcd"
        val t = "abcde"
        assertEquals('e', Solution().findTheDifference(s,t))
    }


    @org.junit.jupiter.api.Test
    fun findTheDifference_aa() {
        val s = "a"
        val t = "aa"
        assertEquals('a', Solution().findTheDifference(s,t))
    }

    @org.junit.jupiter.api.Test
    fun findTheDifference_bit() {
        val s = "abcd"
        val t = "abcde"
        assertEquals('e', Solution().findTheDifference(s,t))
    }


    @org.junit.jupiter.api.Test
    fun findTheDifference_bit2() {
        val s = "a"
        val t = "aa"
        assertEquals('a', Solution().findTheDifference(s,t))
    }
}