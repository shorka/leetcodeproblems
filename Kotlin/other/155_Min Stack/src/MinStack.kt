/**
 * Created by Kirill on 12/29/2018.
 */
class MinStack {
    class MinStack() {

        val list = mutableListOf<Int>()


        fun push(x: Int) {
            list.add(x)
        }

        fun pop() {
            list.removeAt(list.size - 1)
        }

        fun top(): Int {
            return list[list.size - 1]
        }

        fun getMin(): Int {

            var min = Int.MAX_VALUE
            for (item in list) {
                if (item < min)
                    min = item
            }

            return min
        }

    }
}