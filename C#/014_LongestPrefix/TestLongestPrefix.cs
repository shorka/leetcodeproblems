﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _014_LongestPrefix
{
    [TestFixture]
    class TestLongestPrefix
    {
        [Test]
        public void Test_ReturnGEE()
        {
            string[] str = new string[] { "geeksforgeeks", "geeks", " geek", "geezer" };
            Assert.AreEqual("gee", ProgramLongestPrefix.LongestCommonPrefix_Horizontal(str));
        }
    }
}
