﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _014_LongestPrefix
{
    //Write a function to find the longest common prefix string amongst an array of strings.
    class ProgramLongestPrefix
    {
        static void Main(string[] args)
        {
            string[] strs = new string[] { "leets", "leetcode", "leet", "leeds" };

            //string example = "asvsa";
            //Console.WriteLine("Index of: " + example.IndexOf("sa"));
            Console.WriteLine("Longest prefix: " + LongestCommonPrefix_Horizontal(strs));
            //Console.WriteLine("#2Longest prefix: " + LongestCommonPrefix_Vertical(str));
            //Console.WriteLine("#2Longest common prefix: " + LongestPrefixDivideAndConquer.LongestCommonPrefix(strs));
        }

        /// <summary>
        /// Approach #1 (Horizontal scanning)
        /// </summary>
        public static string LongestCommonPrefix_Horizontal(string[] strs)
        {
            if (strs == null || strs.Length == 0)
                return string.Empty;

            string prefix = strs[0];
            int i = 1;
            while (i < strs.Length)
            {
                while (!strs[i].StartsWith(prefix))
                    prefix = prefix.Substring(0, prefix.ToCharArray().Length - 1);

                i++;
            }
            return prefix;
        }

        /// <summary>
        /// Approach #2 (Horizontal scanning)
        /// </summary>
        /// <remarks>
        /// Imagine a very short string is at the end of the array. The above approach will still do SS comparisons.
        /// One way to optimize this case is to do vertical scanning
        /// . We compare characters from top to bottom on the same column (same character index of the strings) before moving on to the next column.
        /// </remarks>
        public static string LongestCommonPrefix_Vertical(string[] strs)
        {
            if (strs == null || strs.Length == 0)
                return string.Empty;


            int len = strs[0].ToCharArray().Length;
            for (int i = 0; i < len; i++)
            {
                char c = strs[0][i];
                for (int j = 0; j < strs.Length; j++)
                {
                    if (i == strs[j].Length || strs[j][i] != c)
                        return strs[0].Substring(0, i);
                }
            }

            return strs[0];
        }

    }
}
