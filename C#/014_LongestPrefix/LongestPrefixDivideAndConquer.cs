﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _014_LongestPrefix
{
    class LongestPrefixDivideAndConquer
    {      

        private static int recurCount1 = 0;
        private static int recurCount2 = 0;
        private static int recurCountCommonPrefix = 0;

        public static string LongestCommonPrefix(string[] strs)
        {
            recurCount1++;
            if (strs == null || strs.Length == 0)
                return string.Empty;

            return longestCommonPrefix(strs, 0, strs.Length - 1);
        }

        private static string longestCommonPrefix(string[] strs, int leftN, int rightN)
        {
            recurCount2++;

            if (leftN == rightN)
            {
                recurCount2--;
                return strs[leftN];
            }


            int mid = (leftN + rightN) / 2;
            string lcpLeft = longestCommonPrefix(strs, leftN, mid);
            string lcpRight = longestCommonPrefix(strs, mid + 1, rightN);
            return commonPrefix(lcpLeft, lcpRight);
        }

        private static string commonPrefix(string left, string right)
        {
            recurCountCommonPrefix++;
            int min = Math.Min(left.Length, right.Length);
            for (int i = 0; i < min; i++)
            {
                if (left[i] != right[i])
                    return left.Substring(0, i);
            }
            string resCommonPref = left.Substring(0, min);
            return resCommonPref;
        }
    }
}
