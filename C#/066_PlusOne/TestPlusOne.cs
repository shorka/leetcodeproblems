﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _066_PlusOne
{
    [TestFixture]
    class TestPlusOne
    {
        [Test]
        public void Test_Return1357()
        {
            int[] arr = new int[] { 1, 3, 5, 6 };
            int[] expectedArr = new int[] { 1, 3, 5, 7 };
            Assert.AreEqual(expectedArr, ProgramPlusOne.PlusOne(arr));
        }

        [Test]
        public void Test_Return8900()
        {
            int[] arr = new int[] { 8, 8, 9, 9 };
            int[] expectedArr = new int[] { 8, 9, 0, 0 };
            Assert.AreEqual(expectedArr, ProgramPlusOne.PlusOne(arr));
        }

        [Test]
        public void Test_Return0()
        {
            int[] arr = new int[] { 0 };
            int[] expectedArr = new int[] { 1 };
            Assert.AreEqual(expectedArr, ProgramPlusOne.PlusOne(arr));
        }

        [Test]
        public void Test_Return10()
        {
            int[] arr = new int[] { 9 };
            int[] expectedArr = new int[] { 1, 0 };
            Assert.AreEqual(expectedArr, ProgramPlusOne.PlusOne(arr));
        }

        [Test]
        public void Test_Return100()
        {
            int[] arr = new int[] { 9, 9 };
            int[] expectedArr = new int[] { 1, 0, 0 };
            Assert.AreEqual(expectedArr, ProgramPlusOne.PlusOne(arr));
        }
    }
}
