﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _066_PlusOne
{
    //    Given a non-negative integer represented as a non-empty array of digits, plus one to the integer.

    //You may assume the integer do not contain any leading zero, except the number 0 itself.

    //The digits are stored such that the most significant digit is at the head of the list.
    //[9] +1 = [1,0]
    class ProgramPlusOne
    {
        static void Main(string[] args)
        {
            int[] arr = new int[] { 1, 3, 5, 6 };
            arr = PlusOne(arr);
            foreach (var item in arr)
            {
                Console.Write(item.ToString() + " ");
            }
        }

        public static int[] PlusOne(int[] digits)
        {
            int len = digits.Length;
            for (int i = len - 1; i >= 0; i--)
            {
                if (digits[i] == 9)
                {
                    digits[i] = 0;
                    if (i == 0)
                    {
                        int[] digitsNew = new int[len + 1];
                        digitsNew[0] = 1;
                        for (int k = 1; k < digitsNew.Length; k++)
                            digitsNew[k] = digits[k - 1];

                        return digitsNew;
                    }
                }
                else
                {
                    digits[i]++;
                    break;
                }
            }

            return digits;
        }

        public static int[] PlusOneDiscuss(int[] digits)
        {
            int len = digits.Length;
            for (int i = len - 1; i >= 0; i--)
            {
                if (digits[i] < 9)
                {
                    digits[i]++;
                    return digits;
                }

                digits[i] = 0;
            }

            int[] newNumber = new int[len + 1];
            newNumber[0] = 1;

            return newNumber;
        }
    }
}
