﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
namespace _013_RomanToInteger
{
    [TestFixture]
    class TestRomanToInteger
    {
        [Test]
        public void Test_Return90()
        {
            Assert.AreEqual(90, ProgramRomanToInteger.RomanToInt("XC"));
        }

        [Test]
        public void Test_Return30()
        {
            Assert.AreEqual(30, ProgramRomanToInteger.RomanToInt("XXX"));
        }

        [Test]
        public void Test_Return1954()
        {
            Assert.AreEqual(1954, ProgramRomanToInteger.RomanToInt("MCMLIV"));
        }
    }
}
