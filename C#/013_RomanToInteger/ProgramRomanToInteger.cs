﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _013_RomanToInteger
{
    class ProgramRomanToInteger
    {
        static void Main(string[] args)
        {
        }

        public static int RomanToInt(string s)
        {
            int sum = 0;
            if (s.IndexOf("IV") != -1) sum -= 2;

            if (s.IndexOf("IX") != -1) sum -= 2;

            if (s.IndexOf("XL") != -1) sum -= 20;

            if (s.IndexOf("XC") != -1) sum -= 20;

            if (s.IndexOf("CD") != -1) sum -= 200;

            if (s.IndexOf("CM") != -1) sum -= 200;

            char[] arrChar = s.ToCharArray();

            foreach (char item in arrChar)
            {
                if (item == 'M') sum += 1000;
                if (item == 'D') sum += 500;
                if (item == 'C') sum += 100;
                if (item == 'L') sum += 50;
                if (item == 'X') sum += 10;
                if (item == 'V') sum += 5;
                if (item == 'I') sum += 1;
            }

            return sum;
        }
    }
}
