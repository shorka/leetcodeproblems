﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _067_AddBinary
{
    [TestFixture]
    class TestAddBinary
    {

        [Test]
        public void Test_Return100()
        {
            string a = "11";
            string b = "1";
            string expected = "100";
            Assert.AreEqual(expected, ProgramAddBinary.AddBinaryDiscuss(a, b));
        }

        [Test]
        public void Test_Return10()
        {
            string a = "1";
            string b = "1";
            string expected = "10";
            Assert.AreEqual(expected, ProgramAddBinary.AddBinaryDiscuss(a, b));
        }


        [Test]
        public void Test_Return1000()
        {
            string a = "11100";
            string b = "101";
            string expected = "100001";
            Assert.AreEqual(expected, ProgramAddBinary.AddBinaryDiscuss(a, b));
        }
    }
}
