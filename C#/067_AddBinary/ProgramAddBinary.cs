﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _067_AddBinary
{
    //    Given two binary strings, return their sum(also a binary string).

    //For example,
    //a = "11"
    //b = "1"
    //Return "100".

    //0 + 0 = 0
    //0 + 1 = 1
    //1 + 0 = 1
    //1 + 1 = 10 (which is 0 carry 1)
    class ProgramAddBinary
    {
        static void Main(string[] args)
        {
            //AddBinary("11", "1");
        }

        /// <summary>
        /// Wrong result
        /// </summary>
        public static string AddBinary(string a, string b)
        {
            char[] numA = a.ToCharArray();
            char[] numB = b.ToCharArray();

            int iNumA = numA.Length - 1;
            int jNumB = numB.Length - 1;
            int carry = 0;
            int maxLen = Math.Max(numA.Length, numB.Length);

            string result = string.Empty;
            int k = maxLen;
            while (iNumA >= 0 && jNumB >= 0)
            {
                bool isNumAZero = numA[iNumA] == '0';
                bool isNumBZero = numB[jNumB] == '0';

                if (isNumAZero && isNumBZero)
                {
                    result = (carry >= 1 ? "1" : "0") + result;
                    carry = 0;
                }
                else if ((isNumAZero || isNumBZero) && (!isNumAZero || !isNumBZero))
                {
                    result = (carry >= 1 ? "0" : "1") + result;
                    carry = 0;
                }
                else if (!isNumAZero && !isNumBZero)
                {
                    result = (carry >= 1 ? "1" : "0") + result;
                    carry++;
                }

                iNumA--;
                jNumB--;
                k--;
            }

            for (int i = k; i >= 0; i--)
            {
                if (i > 0)
                    result = (carry >= 1 ? "0" : "1") + result;

                else
                    result = (carry >= 1 ? "1" : "0") + result;

                carry = 0;
            }


            Console.WriteLine("Result: " + result);
            return result;

            #region buf
            //for (int i = maxLen - 1; i >= 0; i--)
            //{
            //    if (numA[i] == '0' && numB[i] == '0')
            //    {
            //        result[i] = '0';
            //    }

            //    else if ((numA[i] == '0' || numB[i] == '0') && (numA[i] == '1' || numB[i] == '1'))
            //    {
            //        result[i] = '1';
            //    }

            //    else if (numA[i] == '1' && numB[i] == '1')
            //    {
            //        result[i] = '0';
            //    }
            //}
            #endregion
        }

        public static string AddBinaryDiscuss(string a, string b)
        {
            string result = string.Empty;
            int i = a.Length - 1;
            int j = b.Length - 1;
            int carry = 0;

            while (i >= 0 || j >= 0)
            {
                int sum = carry;
                //-'0' convert to int. Google ascII table
                if (j >= 0)
                    sum += b[j--] - '0';

                if (i >= 0)
                    sum += a[i--] - '0';

                result = (sum % 2) + result;
                carry = sum / 2;
            }
            if (carry != 0)
                result = carry + result;

            return result;
        }
    }
}
