﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _206.ReverseLinked_List
{
    //Reverse a singly linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int x) { val = x; }


        public string Printed
        {
            get
            {
                string res = "val : " + val + " next: ";
                res += next == null ? " NULL" : next.val.ToString();

                return res;
            }
        }
    }

    class ProgReverseList
    {
        private static ListNode ListNode1
        {
            get
            {
                ListNode node1 = new ListNode(1);
                ListNode node2 = new ListNode(4);
                ListNode node3 = new ListNode(8);
                ListNode node4 = new ListNode(12);

                node1.next = node2;
                node2.next = node3;
                node3.next = node4;

                return node1;
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Before reverse" + PrintList(ListNode1));
            Console.WriteLine("\nAfter reverse" +
                PrintList(ReverseListLeet1(ListNode1)));
        }

        /// <summary>
        /// NOT RIGHT. Works uncorrectly
        /// </summary>
        private static ListNode ReverseList(ListNode head)
        {
            ListNode reverseHead = null;
            ListNode endNode = head;
            ListNode befEndNode = head;
            ListNode befInsertNode = null;

            while (head.next != null)
            {
                //Find endNode and node BEFORE end
                while (endNode.next != null)
                {
                    befEndNode = endNode;
                    endNode = endNode.next;
                }

                befEndNode.next = null;
                endNode.next = head;
                if (reverseHead == null)
                    reverseHead = endNode;

                string list = PrintList(reverseHead);
                string strhead = head.Printed;
            }

            return reverseHead;
        }


        //Time complexity : O(n).
        //Space complexity : O(1)
        public static ListNode ReverseListLeet1(ListNode head)
        {
            ListNode prev = null;
            ListNode curr = head;
            while (curr != null)
            {
                ListNode nextTemp = curr.next;
                curr.next = prev;
                prev = curr;
                curr = nextTemp;
                //string list = PrintList(prev);
            }
            return prev;
        }

        public static string PrintList(ListNode current)
        {
            string res = string.Empty;
            while (current != null)
            {
                res += " " + current.val;
                //Console.Write(" " + current.val);
                current = current.next;
            }

            //res += "\n";
            return res;
            //Console.WriteLine();
        }


    }
}
