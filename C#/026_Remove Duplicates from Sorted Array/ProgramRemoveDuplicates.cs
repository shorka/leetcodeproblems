﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _026_Remove_Duplicates_from_Sorted_Array
{
    //Given a sorted array, remove the duplicates in place such that each element appear only once and return the new length.
    //Do not allocate extra space for another array, you must do this in place with constant memory.
    //For example,
    //Given input array nums = [1, 1, 2],

    //Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.
    //It doesn't matter what you leave beyond the new length.

    class ProgramRemoveDuplicates
    {
        static void Main(string[] args)
        {
            //var nums = new int[] { 1, 1, 2 };
            var nums = new int[] { 0, 2, 2, 2, 5, 6, 7 };
            Console.WriteLine("N: " + RemoveDuplicates_TwoPointers(nums));

            foreach (int item in nums)
            {
                Console.Write(item + " ");
            }
        }

        //public static int[] RemoveDuplicates(int[] nums)
        //{
        //    int lenUnique = 0;
        //    int numLength = nums.Length;
        //    for (int i = 0; i < numLength - 1; i++)
        //    {
        //        int iNum = nums[i];
        //        bool isRepeated = false;
        //        //int nonChangableIndex = i + 1;
        //        for (int j = i + 1; j < numLength - 1; j++)
        //        {
        //            if (iNum == nums[j])
        //            {
        //                isRepeated = true;
        //                continue;
        //            }

        //            #region buf

        //            //Console.WriteLine("Inside if");
        //            int buf = nums[j];
        //            nums[j] = nums[i + 1];
        //            nums[i + 1] = buf;
        //            #endregion

        //            //if (isRepeated)
        //            //{
        //            //    int buf = nums[j];
        //            //    nums[j] = nums[i + 1];
        //            //    nums[i + 1] = buf;
        //            //}
        //        }
        //    }

        //    return nums;
        //}

        //public static int[] RemoveDuplicates2(int[] nums)
        //{
        //    int numLength = nums.Length;
        //    for (int i = 0; i < numLength - 1; i++)
        //    {
        //        int iNum = nums[i];
        //        int firstRepeatedIndex = -1;
        //        for (int j = i + 1; j < numLength; j++)
        //        {
        //            if (iNum == nums[j])
        //            {
        //                if (firstRepeatedIndex == -1)
        //                    firstRepeatedIndex = j;

        //                continue;
        //            }

        //            if (firstRepeatedIndex == -1)
        //                continue;

        //            //Console.WriteLine("Inside if");
        //            int buf = nums[j];
        //            nums[j] = nums[firstRepeatedIndex];
        //            nums[firstRepeatedIndex] = buf;
        //            break;
        //        }
        //    }

        //    return nums;
        //}

        public static int RemoveDuplicates_TwoPointers(int[] nums)
        {
            if (nums.Length == 0)
                return 0;

            int uniqueValIndex = 0;
            for (int j = 1; j < nums.Length; j++)
            {
                if (nums[j] != nums[uniqueValIndex])
                {
                    uniqueValIndex++;
                    nums[uniqueValIndex] = nums[j];
                }
            }
            return uniqueValIndex + 1;
        }
    }
}
