﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _012_IntegerToRoman
{
    //    Given an integer, convert it to a roman numeral.

    //Input is guaranteed to be within the range from 1 to 3999.
    class ProgramIntegerToRoman
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Res: " + 2341 / 1000);
        }

        public static string IntToRoman(int num)
        {
            string[] M = { "", "M", "MM", "MMM" };
            string[] C = { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
            string[] X = { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
            string[] I = { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };
            return M[num / 1000] + C[(num % 1000) / 100] + X[(num % 100) / 10] + I[num % 10];
        }
    }
}