﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
namespace _012_IntegerToRoman
{
    [TestFixture]
    class TestIntegerToRoman
    {
        [Test]
        public void Test_IntToRoman()
        {
            Assert.AreEqual("MMCCC", ProgramIntegerToRoman.IntToRoman(2300));
        }
    }
}
