﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _035_SearchInsertionPosition
{
    // Given a sorted array and a target value, return the index if the target is found.
    // If not, return the index where it would be if it were inserted in order.

    //You may assume no duplicates in the array.
    //Here are few examples.
    //[1, 3, 5, 6], 5 → 2
    //[1,3,5,6], 2 → 1
    //[1,3,5,6], 7 → 4
    //[1,3,5,6], 0 → 0

    class ProgramSearchInsertionPos
    {
        static void Main(string[] args)
        {
            var nums = new int[] { 1, 3, 5, 6 };
            //Console.WriteLine("Index: " + SearchInsert(nums, 5));
            Console.WriteLine("Index: " + SearchInsert_DiffSolution(nums, 5));
        }

        public static int SearchInsert(int[] nums, int target)
        {
            if (nums == null)
                return -1;

            if (nums.Length == 0)
                return 0;

            int k = 0;
            while (target > nums[k])
            {
                k++;
                //Overflow case prevention
                if (k >= nums.Length)
                    break;
            }

            return k;
        }

        public static int SearchInsert_DiffSolution(int[] nums, int target)
        {
            int low = 0;
            int high = nums.Length;

            while (low < high)
            {
                int mid = (high + low) / 2;
                if (nums[mid] < target)
                    low = mid + 1;
                else
                    high = mid;
            }
            return low;
        }
    }
}
