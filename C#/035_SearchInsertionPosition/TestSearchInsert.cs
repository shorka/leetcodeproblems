﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _035_SearchInsertionPosition
{
    [TestFixture]
    class TestSearchInsert
    {
        [Test]
        public void Test_Return0()
        {
            var nums = new int[] { 1, 3, 5, 6 };
            Assert.AreEqual(0, ProgramSearchInsertionPos.SearchInsert(nums, 0));
        }

        [Test]
        public void Test_Return2()
        {
            var nums = new int[] { 1, 3, 5, 6 };
            Assert.AreEqual(2, ProgramSearchInsertionPos.SearchInsert(nums, 5));
        }

        [Test]
        public void Test_Return1()
        {
            var nums = new int[] { 1, 3, 5, 6 };
            Assert.AreEqual(1, ProgramSearchInsertionPos.SearchInsert(nums, 2));
        }

        [Test]
        public void Test_Return4()
        {
            var nums = new int[] { 1, 3, 5, 6 };
            Assert.AreEqual(4, ProgramSearchInsertionPos.SearchInsert(nums, 7));
        }
    }
}
