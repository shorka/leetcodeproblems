﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009.Palindrome_Number
{
    //Determine whether an integer is a palindrome.Do this without extra space.
    class ProgramPalindromeNumber
    {
        static void Main(string[] args)
        {
        }

        public bool IsPalindrome(int x)
        {
            if (x < 0)
                return false;

            int result = 0;
            while (x != 0)
            {

                result = result * 10 + x % 10;
                x /= 10;
                if (result == x)
                    return true;
            }

            return false;
        }
    }
}
