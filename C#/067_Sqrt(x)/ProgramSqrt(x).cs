﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _067_Sqrt_x_
{
    //    Implement int sqrt(int x).
    //Compute and return the square root of x.

    class ProgramSqrt
    {
        static void Main(string[] args)
        {
        }

        //WORKS properly
        public static int MySqrt(int x)
        {
            if (x < 0)
                return -1;

            int left = 0;
            int right = x;
            while (left <= right)
            {
                int mid = left + (right - left) / 2;

                int guess = mid * mid;
                if (guess == x)
                    return mid;

                if (guess > x)
                    right = mid - 1;

                else
                    left = mid + 1;
            }

            int middle2 = (left + right) / 2;
            return middle2;
        }

        public static int MySqrtDiscuss(int x)
        {
            if (x == 0)
                return 0;

            else if (x < 0)
                return -1;

            int left = 1;
            int right = x + 1;

            while (true)
            {
                int mid = left + (right - left) / 2;
                if (mid > x / mid)
                {
                    right = mid - 1;
                }
                else
                {
                    if (mid + 1 > x / (mid + 1))
                        return mid;

                    left = mid + 1;
                }
            }
        }
    }
}
