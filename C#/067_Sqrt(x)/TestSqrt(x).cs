﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _067_Sqrt_x_
{
    [TestFixture]
    class TestSqrt_x_
    {
        [Test]
        public void Test_Return9()
        {
            Assert.AreEqual(9, ProgramSqrt.MySqrtDiscuss(81));
        }

        [Test]
        public void Test_Return0()
        {
            Assert.AreEqual(0, ProgramSqrt.MySqrtDiscuss(0));
        }

        [Test]
        public void Test_Negative()
        {
            Assert.AreEqual(-1, ProgramSqrt.MySqrtDiscuss(-56));
        }

        [Test]
        public void Test_Return1_From2()
        {
            Assert.AreEqual(1, ProgramSqrt.MySqrtDiscuss(2));
        }

        [Test]
        public void Test_Return1_From5()
        {
            Assert.AreEqual(2, ProgramSqrt.MySqrtDiscuss(5));
        }
    }
}
