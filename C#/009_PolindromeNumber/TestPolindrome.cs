﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _009_PolindromeNumber
{
    [TestFixture]
    class TestPolindrome
    {
        [Test]
        public void Test_ReturnTrue_Of1221()
        {
            Assert.AreEqual(true, ProgramPolindrome.IsPolindrome(1221));
        }

        [Test]
        public void Test_ReturnFalse_of10221()
        {
            Assert.AreEqual(false, ProgramPolindrome.IsPolindrome(10221));
        }

        [Test]
        public void Test2_ReturnTrue_Of1221()
        {
            Assert.AreEqual(true, ProgramPolindrome.IsPolindrome2(1221));
        }

        [Test]
        public void Test2_ReturnFalse_of10221()
        {
            Assert.AreEqual(false, ProgramPolindrome.IsPolindrome2(10221));
        }
    }
}
