﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_PolindromeNumber
{
    class ProgramPolindrome
    {
        static void Main(string[] args)
        {
            Console.WriteLine("IS_Polindrom: " + IsPolindrome2(1));
        }

        public static bool IsPolindrome(int x)
        {
            if (x < 0)
                return false;
            int result = 0;
            while (x != 0)
            {
                result = result * 10 + x % 10;
                x /= 10;
                if (result == x)
                    return true;
            }
            return false;

        }

        public static bool IsPolindrome2(int x)
        {
            if (x < 0)
                return false;

            if (x != 0 && x % 10 == 0)
                return false;

            int rev = 0;
            while (x > rev)
            {
                rev = rev * 10 + x % 10;
                x = x / 10;
            }

            //x == rev / 10. Because x is one-digit number is polindrome as well
            return (x == rev || x == rev / 10);
        }
    }
}
