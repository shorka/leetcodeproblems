﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciNumber
{
    /// <summary>
    /// 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144,
    /// Fn = Fn-1 + Fn-2
    /// </summary>
    class Fibonacci
    {

        static void Main(string[] args)
        {
            int numb = 5;
            Console.WriteLine("Fib From number " + numb + " is " + Fib(numb));
        }

        public static int Fib(int n)
        {
            if (n <= 1)
                return n;

            return Fib(n - 1) + Fib(n - 2);
        }

        public static int FibDynamic(int n)
        {
            int[] arr = new int[n + 1];
            arr[1] = 1;

            for (int i = 2; i <= n; i++)
            {
                arr[i] = arr[i - 1] + arr[i - 2];
            }

            return arr[n];
        }

        public static int FibStoreOptimize(int n)
        {
            int a = 1; //Fn-1
            int b = 0;//Fn-2
            int c = 0;

            if (n == 0)
                return a;

            for (int i = 2; i <= n; i++)
            {
                c = a + b;
                b = a;
                a = c;
            }
            return a;
        }
    }
}
