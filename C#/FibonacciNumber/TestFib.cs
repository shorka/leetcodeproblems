﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace FibonacciNumber
{
    [TestFixture]
    class TestFib
    {
        [Test]
        public void TestFib9()
        {
            Assert.AreEqual(34, Fibonacci.Fib(9));
        }

        [Test]
        public void TestFibDynamic()
        {
            Assert.AreEqual(34, Fibonacci.FibDynamic(9));
        }

        [Test]
        public void TestFibStorageSave()
        {
            Assert.AreEqual(34, Fibonacci.FibStoreOptimize(9));
        }
    }
}
