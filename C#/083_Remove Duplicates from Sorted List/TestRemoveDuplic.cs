﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Tools;
namespace _083_Remove_Duplicates_from_Sorted_List
{
    [TestFixture]
    class TestRemoveDuplic
    {
        [Test]
        public void Test_Return123()
        {
            Assert.AreEqual(ListNode2, ProgRemoveDuplicate.DeleteDuplicates(ListNode1));
        }

        [Test]
        public void Test_Return123_Straight()
        {
            Assert.AreEqual(ListNode2, ProgRemoveDuplicate.DeleteDuplicatesStraight(ListNode1));
        }

        private ListNode ListNode1
        {
            get
            {
                ListNode node1 = new ListNode(1);
                ListNode node2 = new ListNode(1);
                ListNode node3 = new ListNode(2);
                ListNode node4 = new ListNode(3);
                ListNode node5 = new ListNode(3);

                node1.next = node2;
                node2.next = node3;
                node3.next = node4;
                node4.next = node5;

                return node1;
            }
        }

        private ListNode ListNode2
        {
            get
            {
                ListNode node1 = new ListNode(1);
                ListNode node2 = new ListNode(2);
                ListNode node3 = new ListNode(3);

                node1.next = node2;
                node2.next = node3;
                return node1;
            }
        }
    }
}
