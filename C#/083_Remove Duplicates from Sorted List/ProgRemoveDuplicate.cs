﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace _083_Remove_Duplicates_from_Sorted_List
{
    /// <summary>
    ///Given a sorted linked list, delete all duplicates such that each element appear only once.
    ///For example,
    /// Given 1->1->2, return 1->2.
    /// Given 1->1->2->3->3, return 1->2->3.
    /// </summary>
    class ProgRemoveDuplicate
    {
        private static ListNode ListNode1
        {
            get
            {
                ListNode node1 = new ListNode(1);
                ListNode node2 = new ListNode(1);
                ListNode node3 = new ListNode(2);
                ListNode node4 = new ListNode(3);
                ListNode node5 = new ListNode(3);

                node1.next = node2;
                node2.next = node3;
                node3.next = node4;
                node4.next = node5;

                return node1;
            }
        }

        static void Main(string[] args)
        {
            DisplayTools.DisplayList(ListNode1);
            Console.WriteLine("After Duplication\n");
            DisplayTools.DisplayList(DeleteDuplicatesStraight(ListNode1));
        }

        /// <summary>
        /// My approach, bad one. Doesnt work
        /// </summary>
        public static ListNode DeleteDuplicates(ListNode head)
        {
            ListNode nodeCurrHead = head;
            while (nodeCurrHead.next != null)
            {
                ListNode nodeCheck = head.next;
                ListNode nodePrev = head;
                int val = head.val;

                while (nodeCheck.val != val)
                {
                    nodePrev = nodeCheck;
                    nodeCheck = nodeCheck.next;
                }

                ListNode nextValid = nodeCheck.next.next;
                nodePrev.next = null;

                nodeCurrHead = nodeCurrHead.next;
                if (nodeCurrHead == null)
                    break;
            }
            return head;
        }

        public static ListNode DeleteDuplicatesStraight(ListNode head)
        {
            ListNode current = head;
            while (current != null && current.next != null)
            {
                if (current.next.val == current.val)
                    current.next = current.next.next;

                else
                    current = current.next;
            }
            return head;
        }
    }
}
