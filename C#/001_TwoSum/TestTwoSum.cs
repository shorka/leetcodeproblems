﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _001_TwoSum
{
    [TestFixture]
    class TestTwoSum
    {
        [Test]
        public void Test_Length_Returns2()
        {
            int[] nums = new int[] { 2, 7, 11, 15 };
            int[] sum = ProgramTwoSum.TwoSum_Brute(nums, 9);

            Assert.AreEqual(sum.Length, 2);
        }

        [Test]
        public void TestBruteForce_TwoSum_Returns0AND1()
        {
            int[] nums = new int[] { 2, 7, 11, 15 };
            int[] sum = ProgramTwoSum.TwoSum_Brute(nums, 9);

            Assert.AreEqual(sum, new int[] { 0, 1 });
        }

        [Test]
        public void TestBruteForce_TwoSum_Returns0AND4()
        {
            int[] nums = new int[] { 10, 2, 19, 315, 3 };
            int[] sum = ProgramTwoSum.TwoSum_Brute(nums, 13);

            Assert.AreEqual(sum, new int[] { 0, 4 });
        }

        [Test]
        public void TestTwoPassHash_TwoSum_Returns1AND2()
        {
            int[] nums = new int[] { 10, 2, 19, 315, 3 };
            int[] sum = ProgramTwoSum.TwoSum_TwoPassHash(nums, 21);

            Assert.AreEqual(sum, new int[] { 1, 2 });
        }

        [Test]
        public void TestTwoPassHash_TwoSum_Returns1AND3()
        {
            int[] nums = new int[] { 10, 2, 19, 315, 3 };
            int[] sum = ProgramTwoSum.TwoSum_TwoPassHash(nums, 317);

            Assert.AreEqual(sum, new int[] { 1, 3 });
        }

        [Test]
        public void TestOnePassHash_TwoSum_Returns1AND3()
        {
            int[] nums = new int[] { 10, 2, 19, 315, 3 };
            int[] sum = ProgramTwoSum.TwoSum_OnePassHash(nums, 317);

            Assert.AreEqual(sum, new int[] { 1, 3 });
        }
    }
}
