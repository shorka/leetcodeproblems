﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_TwoSum
{
    //Given an array of integers, return indices of the two numbers such that they add up to a specific target.
    //You may assume that each input would have exactly one solution, and you may not use the same element twice.

    //Example:
    //Given nums = [2, 7, 11, 15], target = 9,

    //Because nums[0] + nums[1] = 2 + 7 = 9,
    //return [0, 1].

    class ProgramTwoSum
    {
        static void Main(string[] args)
        {
            //int[] nums = new int[] { 2, 7, 11, 15 };
            //int target = 9;

            //int[] sum = TwoSum_Brute(nums, target);

        }

        public static int[] TwoSum_Brute(int[] nums, int target)
        {
            int len = nums.Length;
            for (int i = 0; i < nums.Length; i++)
            {
                int iNum = nums[i];
                for (int j = i + 1; j < nums.Length; j++)
                {
                    if ((iNum + nums[j]) == target)
                        return new int[] { i, j };
                }
            }

            throw new Exception("Didn't find sum of numbers, that have sum " + target);
        }

        public static int[] TwoSum_TwoPassHash(int[] nums, int target)
        {
            var dict = new Dictionary<int, int>();

            int len = nums.Length;
            for (int i = 0; i < len; i++)
                dict.Add(nums[i], i);

            for (int i = 0; i < len; i++)
            {
                int complement = target - nums[i];

                if (dict.ContainsKey(complement) && dict[complement] != i)
                {
                    return new int[] { i, dict[complement] };
                }
            }

            throw new Exception("Didn't find sum of numbers, that have sum " + target);
        }

        public static int[] TwoSum_OnePassHash(int[] nums, int target)
        {
            var dict = new Dictionary<int, int>();

            int len = nums.Length;
            for (int i = 0; i < len; i++)
            {
                int iNum = nums[i];
                int complement = target - iNum;

                if (dict.ContainsKey(complement) && dict[complement] != i)
                {
                    return new int[] { dict[complement], i };
                }

                dict.Add(iNum, i);
            }

            throw new Exception("Didn't find sum of numbers, that have sum " + target);
        }
    }
}
