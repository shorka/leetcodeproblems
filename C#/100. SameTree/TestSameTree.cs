﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Tools;

namespace _100.SameTree
{
    [TestFixture]
    class TestSameTree
    {
        public TreeNode TreeNode1
        {
            get
            {
                var treeNode1 = new TreeNode(1);
                var treeNode2 = new TreeNode(2);
                var treeNode3 = new TreeNode(3);

                treeNode1.left = treeNode2;
                treeNode1.right = treeNode3;

                return treeNode1;
            }
        }

        public TreeNode TreeNode2
        {
            get
            {
                var treeNode1 = new TreeNode(1);
                var treeNode2 = new TreeNode(2);

                treeNode1.left = treeNode2;

                return treeNode1;
            }
        }

        public TreeNode TreeNode3
        {
            get
            {
                var treeNode1 = new TreeNode(1);
                var treeNode2 = new TreeNode(2);

                treeNode1.right = treeNode2;

                return treeNode1;
            }
        }

        [Test]
        public void Test_Identical()
        {
            Assert.AreEqual(true, ProgSameTree.IsSameTree(TreeNode1, TreeNode1));
        }

        [Test]
        public void Test_NonSymmetrical()
        {
            Assert.AreEqual(true, ProgSameTree.IsSameTree(TreeNode2, TreeNode3));
        }
    }
}
