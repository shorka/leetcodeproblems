﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace _100.SameTree
{
    //Given two binary trees, write a function to check 
    //if they are the same or not.
    //Two binary trees are considered the same if they are structurally
    //identical and the nodes have the same value.

    class ProgSameTree
    {
        static void Main(string[] args)
        {
        }

        public static bool IsSameTree(TreeNode p, TreeNode q)
        {
            if (p == null && q == null)
                return true;

            if (p == null || q == null)
                return false;

            if (p.val == q.val)
                return IsSameTree(p.left, q.left) && IsSameTree(p.right, q.right);

            return false;
        }
    }
}
