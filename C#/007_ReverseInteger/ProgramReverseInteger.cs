﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_ReverseInteger
{
    //Reverse digits of an integer.

    //Example1: x = 123, return 321
    //Example2: x = -123, return -321

    class ProgramReverseInteger
    {
        static void Main(string[] args)
        {
            int result = 999999999;
            Console.WriteLine("Result: " + result * 10);
            //Console.WriteLine("Result: " + Reverse(result * 10));
        }

        public static int Reverse(int x)
        {
            int result = 0;

            while (x != 0)
            {
                int tail = x % 10;
                int newResult = result * 10 + tail;
                int checkRes = (newResult - tail) / 10;
                if (checkRes != result)
                    return 0;


                result = newResult;
                x /= 10;
            }

            return result;
        }
    }
}
