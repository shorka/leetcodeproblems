﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _007_ReverseInteger
{
    [TestFixture]
    class TestReverseInteger
    {
        [Test]
        public void Test_Reverse_Return148()
        {
            Assert.AreEqual(148, ProgramReverseInteger.Reverse(841));
        }

        [Test]
        public void Test_Reverse_Return1()
        {
            Assert.AreEqual(1, ProgramReverseInteger.Reverse(1));
        }

        [Test]
        public void Test_Reverse_Return484183()
        {
            Assert.AreEqual(483184, ProgramReverseInteger.Reverse(481384));
        }

        [Test]
        public void Test_Reverse_ReturnMinus148()
        {
            Assert.AreEqual(-148, ProgramReverseInteger.Reverse(-841));
        }

        [Test]
        public void Test_Reverse_OverInteger()
        {
            Assert.AreEqual(0, ProgramReverseInteger.Reverse(1534236469));
        }
    }
}
