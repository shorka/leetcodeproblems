﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools
{
    public class DisplayTools
    {
        static void Main(string[] args)
        {
        }

        public static void DisplayList(ListNode current)
        {
            while (current != null)
            {
                Console.Write(" " + current.val);
                current = current.next;
            }
            Console.WriteLine();
        }

        public static void DisplayIntArray(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + " ");
            }
        }
    }
}
