﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _070_ClimbingStairs
{
    [TestFixture]
    class TestClimbStairs
    {
        [Test]
        public void TestBrute_Return8()
        {
            Assert.AreEqual(8, ProgramClimbStairs.ClimbStairs(5));
        }

        [Test]
        public void TestMemoRecursion_Return8()
        {
            Assert.AreEqual(8, ProgramClimbStairs.ClimbStairs_Recur(5));
        }

        [Test]
        public void TestClimbStarsDynamic()
        {
            Assert.AreEqual(8, ProgramClimbStairs.ClimbStairs_Dynamic(5));
        }

        [Test]
        public void TestClimbStarsFibanacci()
        {
            Assert.AreEqual(8, ProgramClimbStairs.ClimbStairs_Fibanacci(5));
        }
    }
}
