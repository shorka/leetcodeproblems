﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _070_ClimbingStairs
{
    //You are climbing a stair case. It takes n steps to reach to the top.
    //Each time you can either climb 1 or 2 steps.In how many distinct ways can you climb to the top?
    //Note: Given n will be a positive integer.
    class ProgramClimbStairs
    {
        static void Main(string[] args)
        {
            //int qty = ClimbStairs(5);
            //int qty = ClimbStairs_Recur(5);
            int qty = ClimbStairs_Dynamic(5);
            Console.WriteLine("QTY of steps: " + qty);
        }

        /// <summary>
        // Time complexity : O(2^n). Size of recursion tree will be 2^n
        // https://imgur.com/a/oZbPk
        /// </summary>
        public static int ClimbStairs(int n)
        {
            return Climb_Stairs(0, n);
        }
        private static int Climb_Stairs(int i, int n)
        {
            if (i > n)
                return 0;

            if (i == n)
                return 1;

            int sum = Climb_Stairs(i + 1, n) + Climb_Stairs(i + 2, n);

            return sum;
            //return Climb_Stairs(i + 1, n) + Climb_Stairs(i + 2, n);
        }

        /// <summary>
        /// Time complexity : O(n). Size of recursion tree can go upto n.
        // Space complexity : O(n). The depth of recursion tree can go upto n.
        /// </summary>
        public static int ClimbStairs_Recur(int n)
        {
            int[] memo = new int[n + 1];
            return Climb_Stairs_Recur(0, n, memo);
        }

        private static int Climb_Stairs_Recur(int i, int n, int[] memo)
        {
            if (i > n)
                return 0;

            if (i == n)
                return 1;

            if (memo[i] > 0)
                return memo[i];

            memo[i] = Climb_Stairs_Recur(i + 1, n, memo) + Climb_Stairs_Recur(i + 2, n, memo);
            return memo[i];
        }

        /// <summary>
        /// Time complexity : O(n) Single loop upto nn.
        /// Space complexity : O(n). dpdp array of size nn is used.
        /// </summary>
        public static int ClimbStairs_Dynamic(int n)
        {
            if (n == 1)
                return 1;

            int[] dp = new int[n + 1];
            dp[1] = 1;
            dp[2] = 2;
            for (int i = 3; i <= n; i++)
            {
                dp[i] = dp[i - 1] + dp[i - 2];
            }
            return dp[n];
        }

        /// <summary>
        /// Time complexity : O(n). Single loop upto nn is required to calculate n^{th} fibonacci number.
        ///Space complexity : O(1) Constant space is used.
        /// </summary>
        public static int ClimbStairs_Fibanacci(int n)
        {
            if (n == 1)
                return 1;

            int first = 1;
            int second = 2;
            for (int i = 3; i <= n; i++)
            {
                int third = first + second;
                first = second;
                second = third;
            }
            return second;
        }
    }
}
