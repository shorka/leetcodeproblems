﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _053_MaximumSubArray
{
    //    Find the contiguous subarray within an array(containing at least one number) which has the largest sum.

    //For example, given the array[-2, 1, -3, 4, -1, 2, 1, -5, 4],
    //the contiguous subarray[4, -1, 2, 1] has the largest sum = 6.

    class ProgramMaxSubArray
    {
        static void Main(string[] args)
        {
        }

        public static int MaxSubArray(int[] nums)
        {
            int lenNum = nums.Length;

            int sumMaxSubArr = int.MinValue;
            for (int i = 0; i < lenNum; i++)
            {
                int isumMax = int.MinValue;
                for (int j = i; j < lenNum; j++)
                {
                    int jSum = CalculateSum(nums, i, j);
                    if (isumMax < jSum)
                        isumMax = jSum;
                }

                if (sumMaxSubArr < isumMax)
                    sumMaxSubArr = isumMax;
            }

            return sumMaxSubArr;
        }

        private static int CalculateSum(int[] nums, int startIndex, int endIndex)
        {
            if (startIndex > endIndex)
            {
                Console.WriteLine("Error: startIndex >= endIndex");
                return int.MinValue;
            }

            else if (startIndex == endIndex)
                return nums[startIndex];

            int sum = 0;
            for (int i = startIndex; i <= endIndex; i++)
                sum += nums[i];

            return sum;
        }

        public static int MaxSubArrayDiscuss(int[] nums)
        {
            int maxSoFar = nums[0];
            int maxEndingHere = nums[0];
            for (int i = 1; i < nums.Length; i++)
            {
                maxEndingHere = Math.Max(maxEndingHere + nums[i], nums[i]);
                maxSoFar = Math.Max(maxSoFar, maxEndingHere);
            }
            return maxSoFar;
        }
    }
}
