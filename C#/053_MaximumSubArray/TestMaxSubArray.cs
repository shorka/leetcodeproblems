﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _053_MaximumSubArray
{
    [TestFixture]
    class TestMaxSubArray
    {
        [Test]
        public void Test_Return6()
        {
            int[] array = new int[] { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
            Assert.AreEqual(6, ProgramMaxSubArray.MaxSubArray(array));
        }

        [Test]
        public void Test_Return3()
        {
            int[] array = new int[] { 1, 2 };
            Assert.AreEqual(3, ProgramMaxSubArray.MaxSubArray(array));
        }
    }
}
