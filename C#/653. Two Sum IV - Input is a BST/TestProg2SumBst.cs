﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Tools;

namespace _653.Two_Sum_IV___Input_is_a_BST
{
    [TestFixture]
    class TestProg2SumBst
    {
        public TreeNode TreeNode1
        {
            get
            {
                var treeNode5 = new TreeNode(5);
                var treeNode3 = new TreeNode(3);
                var treeNode6 = new TreeNode(6);
                var treeNode2 = new TreeNode(2);
                var treeNode4 = new TreeNode(4);
                var treeNode7 = new TreeNode(7);

                treeNode5.left = treeNode3;
                treeNode5.right = treeNode6;

                treeNode3.left = treeNode2;
                treeNode3.right = treeNode4;

                treeNode6.right = treeNode7;

                return treeNode5;
            }
        }

        public TreeNode TreeNode2
        {
            get
            {
                var treeNode2 = new TreeNode(2);
                var treeNode3 = new TreeNode(3);

                treeNode2.right = treeNode3;
                return treeNode2;
            }
        }


        [Test]
        public void Test_9True()
        {
            Assert.AreEqual(true, Prog2SumBST.FindTarget(TreeNode1, 9));
        }

        [Test]
        public void Test_4False()
        {
            Assert.AreEqual(false, Prog2SumBST.FindTarget(TreeNode1, 4));
        }

        [Test]
        public void Test_6False()
        {
            Assert.AreEqual(false, Prog2SumBST.FindTarget(TreeNode2, 6));
        }
    }
}
