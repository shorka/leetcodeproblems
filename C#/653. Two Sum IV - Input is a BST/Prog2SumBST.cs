﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace _653.Two_Sum_IV___Input_is_a_BST
{
    //Given a Binary Search Tree and a target number, 
    //return true if there exist two elements in the BST such that their sum is equal to the given target.

    class Prog2SumBST
    {
        static void Main(string[] args)
        {
        }

        static HashSet<int> hSet = new HashSet<int>();
        public static bool FindTarget(TreeNode root, int k)
        {
            InOrderTraversal(root);

            //keep the nodes in the path that are waiting to be visited
            var stack = new Stack<TreeNode>();
            TreeNode node = root;

            //first node to be visited will be the left one
            while (node != null)
            {
                stack.Push(node);
                node = node.left;
            }

            // traverse the tree
            while (stack.Any())
            {
                // visit the top node
                node = stack.Pop();
                int complement = k - node.val;
                if (hSet.Contains(complement) && complement != node.val)
                {
                    Console.WriteLine(complement + " + " + k + " = " + node.val);
                    return true;
                }

                if (node.right != null)
                {
                    node = node.right;

                    // the next node to be visited is the leftmost
                    while (node != null)
                    {
                        stack.Push(node);
                        node = node.left;
                    }
                }
            }

            return false;
        }




    }
}
