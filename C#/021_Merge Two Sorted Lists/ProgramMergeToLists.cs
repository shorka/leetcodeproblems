﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _021_Merge_Two_Sorted_Lists
{
    //Merge two sorted linked lists and return it as a new list.
    //The new list should be made by splicing together the nodes of the first two lists.
    class ProgramMergeToLists
    {
        static void Main(string[] args)
        {
            ListNode listNode1_3 = new ListNode(11);

            ListNode listNode1_2 = new ListNode(5);
            listNode1_2.next = listNode1_3;

            ListNode listNode1_1 = new ListNode(3);
            listNode1_1.next = listNode1_2;


            ListNode listNode2_3 = new ListNode(13);

            ListNode listNode2_2 = new ListNode(4);
            listNode2_2.next = listNode2_3;

            ListNode listNode2_1 = new ListNode(2);
            listNode2_1.next = listNode2_2;

            Console.WriteLine("Node: " + MergeTwoLists(listNode1_1, listNode2_1).next.val);
        }

        public static ListNode MergeTwoLists(ListNode l1, ListNode l2)
        {
            if (l1 == null)
                return l2;

            if (l2 == null)
                return l1;

            ListNode mergeHead;
            if (l1.val < l2.val)
            {
                mergeHead = l1;
                mergeHead.next = MergeTwoLists(l1.next, l2);
            }
            else
            {
                mergeHead = l2;
                mergeHead.next = MergeTwoLists(l1, l2.next);
            }

            return mergeHead;
        }

        //public static ListNode MergeTwoLists_Loop(ListNode l1, ListNode l2)
        //{

        //    if (l1 == null)
        //        return l2;

        //    if (l2 == null)
        //        return l1;

        //    ListNode mergeHead;
        //    if (l1.val < l2.val)
        //    {
        //        mergeHead = l1;
        //        mergeHead.next = MergeTwoLists(l1.next, l2);
        //    }
        //    else
        //    {
        //        mergeHead = l2;
        //        mergeHead.next = MergeTwoLists(l1, l2.next);
        //    }

        //    return mergeHead;
        //}
    }

    public class ListNode
    {
        public int val;
        public ListNode next;

        public ListNode(int x)
        {
            val = x;
        }
    }
}


