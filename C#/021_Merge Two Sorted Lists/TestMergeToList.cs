﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _021_Merge_Two_Sorted_Lists
{
    [TestFixture]
    class TestMergeToList
    {
        [Test]
        public void Test_Merge()
        {
            ListNode listNode1_3 = new ListNode(11);

            ListNode listNode1_2 = new ListNode(5);
            listNode1_2 = listNode1_3;

            ListNode listNode1_1 = new ListNode(3);
            listNode1_1.next = listNode1_2;


            ListNode listNode2_3 = new ListNode(13);

            ListNode listNode2_2 = new ListNode(4);
            listNode2_2 = listNode2_3;

            ListNode listNode2_1 = new ListNode(2);
            listNode2_1.next = listNode2_2;

            Assert.AreEqual(90, ProgramMergeToLists.MergeTwoLists(listNode2_1, listNode1_1));
        }
    }
}
