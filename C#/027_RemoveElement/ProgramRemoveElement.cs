﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
namespace _027_RemoveElement
{
    //    Given an array and a value, remove all instances of that value in place 
    //and return the new length.

    //Do not allocate extra space for another array, you must do this in place with constant memory.

    //The order of elements can be changed.It doesn't matter what you leave beyond the new length.

    //Example:
    //Given input array nums = [3, 2, 2, 3], val = 3

    //Your function should return length = 2, with the first two elements of nums being 2.
    class ProgramRemoveElement
    {
        static void Main(string[] args)
        {

            //var nums = new int[] { 3, 2, 2, 3 };
            //var nums = new int[] { 3, 2, 2, 2, 2, 3 };
            //var nums = new int[] { 2, 2, 3, 3, 4 };
            var nums = new int[] { 1, 2, 3, 4, 5, 0 };
            Console.WriteLine("N: " + RemoveElement_TwoPointersRare(nums, 3));

            //foreach (int item in nums)
            //{
            //    Console.Write(item + " ");
            //}
        }

        public static int RemoveElement(int[] nums, int val)
        {
            if (nums == null || nums.Length == 0)
                return 0;

            int lenRes = 0;
            for (int i = 0; i < nums.Length; i++)
            {
                int iNum = nums[i];
                if (iNum != val)
                {
                    lenRes++;
                    continue;
                }


                //So if iNum == val
                for (int j = i + 1; j < nums.Length; j++)
                {
                    //prevents index overflow
                    if (j >= nums.Length)
                        break;

                    if (nums[j] == val)
                        continue;

                    //If nums[j] != val
                    //Helpers.Swap<int>(nums, i, j);
                    int temp = nums[i];
                    nums[i] = nums[j];
                    nums[j] = temp;
                    lenRes++;
                    break;
                }
            }
            return lenRes;
        }

        public static int RemoveElement_TwoPointers(int[] nums, int val)
        {
            if (nums == null)
                return -1;

            int i = 0;
            for (int j = 0; j < nums.Length; j++)
            {
                if (nums[j] != val)
                {
                    nums[i] = nums[j];
                    i++;
                }
            }
            return i;
        }

        public static int RemoveElement_TwoPointersRare(int[] nums, int val)
        {
            if (nums == null)
                return -1;

            int i = 0;
            int n = nums.Length;
            while (i < n)
            {
                if (nums[i] == val)
                {
                    nums[i] = nums[n - 1];
                    // reduce array size by one
                    n--;
                }
                else
                {
                    i++;
                }
            }
            return n;
        }
    }
}
