﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
namespace _027_RemoveElement
{
    [TestFixture]
    class TestRemoveElement
    {
        [Test]
        public void Test_Return3()
        {
            var nums = new int[] { 2, 2, 3, 3, 4 };
            Assert.AreEqual(3, ProgramRemoveElement.RemoveElement(nums, 3));
        }

        [Test]
        public void Test_Return2()
        {
            var nums = new int[] { 3, 2, 2, 3 };
            Assert.AreEqual(2, ProgramRemoveElement.RemoveElement(nums, 3));
        }

        [Test]
        public void Test_Return4()
        {
            var nums = new int[] { 17, 3, 17, 2, 4, 6, 17 };
            Assert.AreEqual(4, ProgramRemoveElement.RemoveElement(nums, 17));
        }
    }
}
