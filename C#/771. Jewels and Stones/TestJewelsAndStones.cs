﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _771.Jewels_and_Stones
{
    [TestFixture]
    class TestJewelsAndStones
    {
        [Test]
        public void Test_3aA()
        {
            string j = "aA";
            string s = "aAAbbbb";

            Assert.AreEqual(3,
                ProgrJewelsAndStones.NumJewelsInStones(j, s));
        }

        [Test]
        public void Test_0Z()
        {
            string j = "z";
            string s = "ZZ";

            Assert.AreEqual(0,
                ProgrJewelsAndStones.NumJewelsInStones(j, s));
        }

        [Test]
        public void Test_3aA_2Approach()
        {
            string j = "aA";
            string s = "aAAbbbb";

            Assert.AreEqual(3,
                ProgrJewelsAndStones.NumJewelsInStones(j, s));
        }

        [Test]
        public void Test_0_2ApproachZ()
        {
            string j = "z";
            string s = "ZZ";

            Assert.AreEqual(0,
                ProgrJewelsAndStones.NumJewelsInStones(j, s));
        }
    }
}
