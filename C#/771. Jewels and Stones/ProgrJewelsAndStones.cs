﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace _771.Jewels_and_Stones
{
    class ProgrJewelsAndStones
    {
        static void Main(string[] args)
        {
        }

        /// <summary>
        /// Brute force.
        /// Time Complexity: O(J\text{.length} * S\text{.length}))O(J.length∗S.length)).
        /// </summary>
        public static int NumJewelsInStones(string J, string S)
        {
            int lenJ = J.Length;
            int lenS = S.Length;

            int stonesJewQTY = 0;
            for (int i = 0; i < lenJ; i++)
            {
                char iJ = J[i];
                for (int j = 0; j < lenS; j++)
                {
                    if (S[j] == iJ)
                    {
                        stonesJewQTY++;
                    }
                }
            }
            return stonesJewQTY;
        }

        public static int NumJewelsInStonesHash(string J, string S)
        {
            if (string.IsNullOrEmpty(J))
                return 0;

            var hash = new HashSet<char>(J.ToCharArray());

            int ans = 0;
            int lenS = S.Length;
            for (int i = 0; i < lenS; i++)
            {
                if (hash.Contains(S[i]))
                    ans++;
            }

            return ans;
        }
    }
}
