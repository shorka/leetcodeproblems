﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _058_Length_Of_LastWorld
{
    [TestFixture]
    class TestLastWord
    {
        [Test]
        public void Test_Return1_ASpace()
        {
            Assert.AreEqual(1, ProgramLastWord.LastWord("a "));
        }

        [Test]
        public void Test_Return1_SpaceASpace()
        {
            Assert.AreEqual(1, ProgramLastWord.LastWord(" a "));
        }

        [Test]
        public void Test_HelloWorld_Return5()
        {
            Assert.AreEqual(5, ProgramLastWord.LastWord("Hello world"));
        }

        [Test]
        public void Test_Space_Return0()
        {
            Assert.AreEqual(0, ProgramLastWord.LastWord(" "));
        }

        [Test]
        public void Test_BA_Return1()
        {
            Assert.AreEqual(1, ProgramLastWord.LastWord("b   a   "));
        }
    }
}
