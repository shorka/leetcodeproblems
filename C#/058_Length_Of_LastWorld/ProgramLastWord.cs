﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _058_Length_Of_LastWorld
{
    //    Given a string s consists of upper/lower-case alphabets and empty space characters ' ', 
    //return the length of last word in the string.

    //If the last word does not exist, return 0.
    //Note: A word is defined as a character sequence consists of non-space characters only.

    //For example,
    //Given s = "Hello World",
    //return 5.

    //TODO: check this
    class ProgramLastWord
    {
        static void Main(string[] args)
        {
            string str = "Hello world";
            Console.WriteLine(str);
            Console.WriteLine("Last world: " + LastWordDiscussed("Im good at coding "));

            //Console.WriteLine("Trim:\n" + "   a sds sd ".Trim());
        }

        public static int LastWord(string s)
        {
            char[] chArr = s.ToCharArray();
            int lastWordCounter = 0;
            int len = chArr.Length;
            for (int i = 0; i < len; i++)
            {
                if (chArr[i] != ' ')
                {
                    lastWordCounter++;
                }

                else
                {
                    if ((i + 1) >= len)
                        break;

                    bool isCharacterAfter = false;
                    for (int j = i + 1; j < len; j++)
                    {
                        if (chArr[j] != ' ')
                        {
                            isCharacterAfter = true;
                            break;
                        }
                    }

                    if (isCharacterAfter)
                        lastWordCounter = 0;
                }
            }

            return lastWordCounter;
        }

        public static int LastWordDiscussed(string s)
        {
            s = s.Trim();
            int lastIndex = s.LastIndexOf(' ') + 1;
            return s.Length - lastIndex;
        }
    }
}
