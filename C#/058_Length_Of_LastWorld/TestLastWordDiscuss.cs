﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _058_Length_Of_LastWorld
{
    [TestFixture]
    class TestLastWordDiscuss
    {
        [Test]
        public void Test_Return1_ASpace()
        {
            Assert.AreEqual(1, ProgramLastWord.LastWordDiscussed("a "));
        }

        [Test]
        public void Test_Return1_SpaceASpace()
        {
            Assert.AreEqual(1, ProgramLastWord.LastWordDiscussed(" a "));
        }

        [Test]
        public void Test_HelloWorld_Return5()
        {
            Assert.AreEqual(5, ProgramLastWord.LastWordDiscussed("Hello world"));
        }

        [Test]
        public void Test_Space_Return0()
        {
            Assert.AreEqual(0, ProgramLastWord.LastWordDiscussed(" "));
        }

        [Test]
        public void Test_BA_Return1()
        {
            Assert.AreEqual(1, ProgramLastWord.LastWordDiscussed("b   a   "));
        }
    }
}
