﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _459.Repeated_Substring_Pattern
{
    //Given a non-empty string check if it can be constructed by taking
    //a substring of it and appending multiple copies of the substring together.
    //You may assume the given string consists of lowercase English letters
    //only and its length will not exceed 10000.
    class ProgRepearSubStr
    {
        static void Main(string[] args)
        {
        }

        public static bool MyRepeatedSubstringPattern(string s)
        {
            //bool isPatternBeg = false;
            string pattern = string.Empty;
            int patterSize = 0;
            for (int i = 1; i < s.Length; i++)
            {
                for (int j = 0; j < s.Length; j++)
                {
                    if (s[i + j] == s[j])
                    {
                        if (patterSize == 0)
                            patterSize = i;


                        if (!pattern.Contains(s[j]))
                            pattern += s[j];
                    }


                    if (s[i + j] != s[j] && (i + j) > patterSize)

                        if (i + j == s.Length)
                        {
                            if (patterSize != pattern.Length)
                                return true;

                            else
                                return false;
                        }

                }

            }

            return false;
        }

        public static bool RepeatedSubStrPattern(string str)
        {
            int len = str.Length;
            for (int i = len / 2; i >= 1; i--)
            {
                if (len % i != 0)
                    continue;

                int m = len / i;
                String subS = str.Substring(0, i);
                StringBuilder sb = new StringBuilder();
                for (int j = 0; j < m; j++)
                    sb.Append(subS);

                if (sb.ToString().Equals(str))
                    return true;
            }
            return false;
        }
    }
}
