﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _459.Repeated_Substring_Pattern
{
    [TestFixture]
    class TestRepeatedSubStr
    {
        [Test]
        public void Test_Abc()
        {
            Assert.AreEqual(true, ProgRepearSubStr.RepeatedSubStrPattern("abcabcabcabc"));
        }

        [Test]
        public void Test_Abc3()
        {
            Assert.AreEqual(true, ProgRepearSubStr.RepeatedSubStrPattern("abcabcabc"));
        }

        [Test]
        public void Test_Klor()
        {
            Assert.AreEqual(false, ProgRepearSubStr.RepeatedSubStrPattern("klorklorkr"));
        }

        [Test]
        public void Test_Empty()
        {
            Assert.AreEqual(false, ProgRepearSubStr.RepeatedSubStrPattern(string.Empty));
        }

        [Test]
        public void Test_Aba()
        {
            Assert.AreEqual(false, ProgRepearSubStr.RepeatedSubStrPattern("aba"));
        }
        [Test]
        public void Test_Abab()
        {
            Assert.AreEqual(true, ProgRepearSubStr.RepeatedSubStrPattern("abab"));
        }
    }


}
