﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft_SqrtRoot
{
    //Calculate square root of int number. Was asked on Microsoft's interview (Sep 22, 2017)
    //SQRT(37) = 6.0827F
    class ProgramSqrRoot
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Result: " + SquareRoot(37).ToString());
        }

        /// <summary>
        /// My brute method. Was discussed on white boards with interviewer.
        /// I finish implementation of it
        /// </summary>
        public static float SquareRoot(int num)
        {
            if (num < 0)
                return -1F;

            if (num == 0)
                return 0;

            int left = 1;
            int right = num;

            float result = 0;
            int guess = left + (right - left) / 2;

            while (guess != num)
            {
                if (guess > num / guess)
                {
                    right = guess - 1;
                }
                else
                {
                    if (guess + 1 > num / (guess + 1))
                    {
                        return FindAfterDecimalPoint(num, guess);
                    }
                    left = guess + 1;
                }
            }



            return result;
        }

        private static float FindAfterDecimalPoint(int target, int numInt)
        {
            float val = 1F;
            int multiplier = 10;

            float guess = numInt + val / multiplier;

            while (multiplier < 100000)
            {
                if (guess * guess > target)
                {
                    val = 1;
                    guess -= (val / multiplier);
                    multiplier *= 10;
                    if (multiplier >= 100000)
                        return guess;
                }
                // guess is Less
                else
                {
                    guess -= (val / multiplier);
                    val++;
                }
                guess += val / multiplier;
            }

            return guess;

        }
    }
}
