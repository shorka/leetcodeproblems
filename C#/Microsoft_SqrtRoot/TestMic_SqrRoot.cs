﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
namespace Microsoft_SqrtRoot
{
    [TestFixture]
    class TestMic_SqrRoot
    {

        [Test]
        public void Test_Return6()
        {
            Assert.AreEqual(6F, ProgramSqrRoot.SquareRoot(36));
        }

        [Test]
        public void Test_Return60827()
        {
            Assert.AreEqual(6.0827F, ProgramSqrRoot.SquareRoot(37));
        }

        [Test]
        public void Test_Return228473()
        {
            Assert.AreEqual(22.8473F, ProgramSqrRoot.SquareRoot(522));
        }

        [Test]
        public void Test_Return0()
        {
            Assert.AreEqual(0F, ProgramSqrRoot.SquareRoot(0));
        }

        [Test]
        public void Test_ReturnMinus()
        {
            Assert.AreEqual(-1F, ProgramSqrRoot.SquareRoot(-3432));
        }
    }
}
