﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace _501.Find_Mode_in_Binary_Search_Tree
{
    //https://leetcode.com/problems/find-mode-in-binary-search-tree/description/

    class ProgFindModeInBST
    {
        public TreeNode TreeNode1
        {
            get
            {
                var tnRoot = new TreeNode(15);
                var tn1 = new TreeNode(9);
                var tn2 = new TreeNode(9);
                var tn3 = new TreeNode(9);
                var tn4 = new TreeNode(9);
                var tn5 = new TreeNode(11);
                var tn6 = new TreeNode(23);
                var tn7 = new TreeNode(23);
                var tn8 = new TreeNode(21);

                tnRoot.left = tn1;
                tnRoot.right = tn6;

                tn1.left = tn2;
                tn1.right = tn5;

                tn2.left = tn3;
                tn2.right = tn4;

                tn6.left = tn7;
                tn6.right = tn8;

                return tnRoot;
            }
        }

        public TreeNode TreeNode2
        {
            get
            {
                var tnRoot = new TreeNode(1);
                var tn1 = new TreeNode(2);
                var tn2 = new TreeNode(2);

                tnRoot.right = tn1;
                tn1.left = tn2;

                return tnRoot;
            }
        }

        public TreeNode TreeNode3
        {
            get
            {
                var tnRoot = new TreeNode(1);
                var tn3 = new TreeNode(1);
                var tn1 = new TreeNode(2);
                var tn2 = new TreeNode(2);
                var tn4 = new TreeNode(2);

                tnRoot.right = tn1;
                tnRoot.left = tn3;

                tn1.left = tn4;
                tn1.right = tn2;
                return tnRoot;
            }
        }

        static void Main(string[] args)
        {
            var obj = new ProgFindModeInBST();
            obj.TreeNode3.Print();
            Console.WriteLine();
            DisplayTools.DisplayIntArray(obj.FindMode(obj.TreeNode3));
        }

        private int currVal;
        private int currCount = 0;
        private int maxCount = 0;
        private int modeCount = 0;

        public int[] FindMode(TreeNode root)
        {
            //find the highest number of occurrences of any value
            Inorder(root);
            modes = new int[modeCount];

            //pass to collect all values occurring that often
            modeCount = 0;
            currCount = 0;
            Inorder(root);

            return modes;
        }

        private int[] modes;

        private void HandleValue(int val)
        {
            if (val != currVal)
            {
                currVal = val;
                currCount = 0;
            }
            currCount++;
            if (currCount > maxCount)
            {
                maxCount = currCount;
                modeCount = 1;
            }
            else if (currCount == maxCount)
            {
                if (modes != null)
                    modes[modeCount] = currVal;

                modeCount++;
            }
        }

        private void Inorder(TreeNode root)
        {
            if (root == null)
                return;

            Inorder(root.left);
            HandleValue(root.val);
            Inorder(root.right);
        }

        //private void Inorder(TreeNode root)
        //{
        //    TreeNode node = root;
        //    while (node != null)
        //    {
        //        if (node.left == null)
        //        {
        //            HandleValue(node.val);
        //            node = node.right;
        //        }
        //        else
        //        {
        //            TreeNode prev = node.left;
        //            while (prev.right != null && prev.right != node)
        //                prev = prev.right;

        //            if (prev.right == null)
        //            {
        //                prev.right = node;
        //                node = node.left;
        //            }
        //            else
        //            {
        //                prev.right = null;
        //                HandleValue(node.val);
        //                node = node.right;
        //            }
        //        }
        //    }
        //}
    }
}
