﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _020_ValidParentheses
{
    class ProgramValidParentheses
    {
        static void Main(string[] args)
        {
            string parentheses = "([])";
            Console.WriteLine("iSvalid: " + IsValid(parentheses));
        }

        public static bool IsValid(string s)
        {
            if (string.IsNullOrEmpty(s))
                return false;


            var stack = new Stack<char>();
            foreach (char item in s.ToCharArray())
            {
                if (item == '(')
                    stack.Push(')');

                else if (item == '{')
                    stack.Push('}');

                else if (item == '[')
                    stack.Push(']');

                else if (stack.Count == 0 || stack.Pop() != item)
                    return false;
            }
            return stack.Count == 0;
        }
    }
}
