﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _022_ImplementstrStr
{
    [TestFixture]
    class TestStrStr
    {
        [Test]
        public void Test_Hello()
        {
            Assert.AreEqual(2, ProgstrStr.StrRightAnswer("Hello", "ll"));
        }

        [Test]
        public void Test_BBA()
        {
            Assert.AreEqual(-1, ProgstrStr.StrRightAnswer("aaaaa", "bba"));
        }

        [Test]
        public void Test_WORD()
        {
            Assert.AreEqual(-1, ProgstrStr.StrRightAnswer("someWord", "rrr"));
        }

        [Test]
        public void Test_Empty()
        {
            Assert.AreEqual(0, ProgstrStr.StrRightAnswer("", ""));
        }
    }
}
