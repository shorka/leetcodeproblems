﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _022_ImplementstrStr
{
    //Return the index of the first occurrence of 
    //needle in haystack, or -1 if needle is not part of haystack.
    class ProgstrStr
    {
        static void Main(string[] args)
        {
            int answ = StrRightAnswer("a", "");
            Console.WriteLine("Answer: " + answ);
        }

        public static int StrStr(string haystack, string needle)
        {
            char[] arrHays = haystack.ToCharArray();
            char[] arrNeedl = needle.ToCharArray();

            if (arrNeedl.Length == 0)
                return -1;

            bool isEqual = true;
            for (int i = 0; i < arrHays.Length; i++)
            {
                char iH = arrHays[i];

                if (iH == arrNeedl[0])
                {
                    isEqual = true;

                    for (int j = 1; j < arrNeedl.Length; j++)
                    {
                        if (i + j >= arrHays.Length)
                            return -1;

                        if (arrHays[i + j] != needle[j])
                        {
                            isEqual = false;
                            break;
                        }
                    }
                }
                else
                    isEqual = false;


                if (isEqual)
                    return i;
            }
            return -1;
        }

        public static int StrRightAnswer(string haystack, string needle)
        {
            for (int i = 0; ; i++)
            {
                for (int j = 0; ; j++)
                {
                    if (j == needle.Length)
                        return i;

                    if (i + j == haystack.Length)
                        return -1;

                    if (needle[j] != haystack[i + j])
                        break;
                }
            }
        }
    }
}
