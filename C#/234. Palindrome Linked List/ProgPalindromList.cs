﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _234.Palindrome_Linked_List
{
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int x) { val = x; }


        public string Printed
        {
            get
            {
                string res = "val : " + val + " next: ";
                res += next == null ? " NULL" : next.val.ToString();

                return res;
            }
        }
    }

    /// <summary>
    /// Given a singly linked list, determine if it is a palindrome.
    /// Follow up:
    /// Could you do it in O(n) time and O(1) space?
    /// </summary>
    class ProgPalindromList
    {
        /// <summary>
        /// 1 -> 4 -> 8 -> 12
        /// </summary>
        private static ListNode ListNode1
        {
            get
            {
                ListNode node1 = new ListNode(1);
                ListNode node2 = new ListNode(4);
                ListNode node3 = new ListNode(8);
                ListNode node4 = new ListNode(12);

                node1.next = node2;
                node2.next = node3;
                node3.next = node4;

                return node1;
            }
        }

        /// <summary>
        /// 1 -> 4 -> 8 -> 4 -> 1
        /// </summary>
        private static ListNode ListNodePali
        {
            get
            {
                ListNode node1 = new ListNode(1);
                ListNode node2 = new ListNode(4);
                ListNode node3 = new ListNode(8);
                ListNode node4 = new ListNode(4);
                ListNode node5 = new ListNode(1);

                node1.next = node2;
                node2.next = node3;
                node3.next = node4;
                node4.next = node5;

                return node1;
            }
        }

        /// <summary>
        /// 1 -> 4 -> 8 -> 4 -> 1
        /// </summary>
        private static ListNode ListNodePali2
        {
            get
            {
                ListNode node1 = new ListNode(10);
                ListNode node2 = new ListNode(40);
                ListNode node3 = new ListNode(80);
                ListNode node4 = new ListNode(90);
                ListNode node5 = new ListNode(110);
                ListNode node6 = new ListNode(90);
                ListNode node7 = new ListNode(80);
                ListNode node8 = new ListNode(40);
                ListNode node9 = new ListNode(10);

                node1.next = node2;
                node2.next = node3;
                node3.next = node4;
                node4.next = node5;
                node5.next = node6;
                node6.next = node7;
                node7.next = node8;
                node8.next = node9;

                return node1;
            }
        }

        static void Main(string[] args)
        {
            //Console.WriteLine("YES: IsPolind: " + IsPalindrome(ListNodePali));
            //Console.WriteLine("NO: IsPolind: " + IsPalindrome(ListNode1));
            Console.WriteLine("YES: IsPolind2: " + IsPalindrome(ListNodePali2));
        }

        private static bool IsPalindrome(ListNode head)
        {
            string listInit = PrintList(head);

            ListNode fast = head;
            ListNode slow = head;
            while (fast != null && fast.next != null)
            {
                fast = fast.next.next;
                slow = slow.next;
            }
            if (fast != null)
            { // odd nodes: let right half smaller
                slow = slow.next;
            }
            slow = Reverse(slow);
            string listSlow = PrintList(slow);
            string listFastBef = PrintList(fast);
            string listHead = PrintList(head);
            fast = head;
            string listFastAfter = PrintList(fast);

            while (slow != null)
            {
                if (fast.val != slow.val)
                    return false;

                fast = fast.next;
                slow = slow.next;
            }
            return true;
        }

        public static ListNode Reverse(ListNode head)
        {
            ListNode prev = null;
            ListNode curr = head;
            while (curr != null)
            {
                ListNode nextTemp = curr.next;
                curr.next = prev;
                prev = curr;
                curr = nextTemp;
                string list = PrintList(prev);
            }
            return prev;
        }

        public static string PrintList(ListNode current)
        {
            string res = string.Empty;
            while (current != null)
            {
                res += " " + current.val;
                //Console.Write(" " + current.val);
                current = current.next;
            }

            //res += "\n";
            return res;
            //Console.WriteLine();
        }
    }
}
