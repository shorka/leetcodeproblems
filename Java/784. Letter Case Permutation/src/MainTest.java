import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/17/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void letterCasePermutation_a1b2() {

        List<String> list = Arrays.asList("a1b2", "a1B2", "A1b2", "A1B2");

        assertEquals(list, new Main().letterCasePermutation("a1b2"));
    }

    @org.junit.jupiter.api.Test
    void letterCasePermutation_3Z4() {

        List<String> list = Arrays.asList("3z4", "3Z4");

        assertEquals(list, new Main().letterCasePermutation("3z4"));
    }

    @org.junit.jupiter.api.Test
    void letterCasePermutation_a1b3() {

        List<String> list = Arrays.asList("a1b2", "a1B2", "A1b2", "A1B2");

        assertEquals(list, new Main().letterCasePermutation2("a1b2"));
    }

    @org.junit.jupiter.api.Test
    void letterCasePermutation_abc() {

        List<String> list = Arrays.asList("abc", "Abc", "aBc", "ABc", "abC", "AbC", "aBC", "ABC");

        assertEquals(list, new Main().letterCasePermutation2("abc"));
    }

    @org.junit.jupiter.api.Test
    void letterCasePermutation_a1b2_DFS() {

        List<String> list = Arrays.asList("a1b2", "a1B2", "A1b2", "A1B2");

        assertEquals(list, new Main().letterCasePermutationDFS("a1b2"));
    }
}