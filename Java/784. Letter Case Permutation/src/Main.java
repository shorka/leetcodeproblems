import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.StreamSupport;

public class Main {

    public static void main(String[] args) {
	// write your code here
    }
    List<String> resList;
    public List<String> letterCasePermutation(String S) {

        resList = new ArrayList<String>();
        resList.add(S);
        int len = S.length();
        String s1 = "";
        for (int i = 0; i <len-1; i++) {

           s1 += permute(S,i, i+1);
        }
        return  resList;
    }

    private String permute(String S, int start, int end){

        for (int i = start; i < end; i++) {
            char iChar = S.charAt(i);
            if(!Character.isLetter(iChar))
                continue;

            if(Character.isUpperCase(iChar))
                iChar = Character.toLowerCase(iChar);

            else
                iChar = Character.toUpperCase(iChar);

            String strRet =  (S.substring(0,i) + iChar + S.substring(i+1,end));
           return strRet;
        }

        return "";
    }

    public List<String> letterCasePermutation2(String S) {
        List<StringBuilder> listAns = new ArrayList();
        listAns.add(new StringBuilder());

        for (char c: S.toCharArray()) {
            int n = listAns.size();
            if (Character.isLetter(c)) {
                for (int i = 0; i < n; ++i) {
                    listAns.add(new StringBuilder(listAns.get(i)));
                    listAns.get(i).append(Character.toLowerCase(c));
                    listAns.get(n+i).append(Character.toUpperCase(c));
                }
            } else {
                for (int i = 0; i < n; ++i)
                    listAns.get(i).append(c);
            }
        }

        List<String> finalans = new ArrayList();
        for (StringBuilder sb: listAns)
            finalans.add(sb.toString());

        return finalans;
    }

    public List<String> letterCasePermutationDFS(String S) {
        if (S == null) {
            return new LinkedList<>();
        }
        Queue<String> queue = new LinkedList<>();
        queue.offer(S);

        for (int i = 0; i < S.length(); i++) {
            if (Character.isDigit(S.charAt(i)))
                continue;

            int size = queue.size();
            for (int j = 0; j < size; j++) {
                String cur = queue.poll();
                char[] chs = cur.toCharArray();

                chs[i] = Character.toUpperCase(chs[i]);
                queue.offer(String.valueOf(chs));

                chs[i] = Character.toLowerCase(chs[i]);
                queue.offer(String.valueOf(chs));
            }
        }

        return new LinkedList<>(queue);
    }
}
