import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
	// write your code here
    }

    public int addDigits(int num) {

        Set<Integer> inLoop = new HashSet<Integer>();
        int sum,remain;
        while (inLoop.add(num)) {
            sum = 0;
            while (num >0) {
                remain = num%10;
                sum += remain;
                num /= 10;
            }
            if (sum < 10)
                return sum;

            else
                num = sum;

        }
        return 0;
    }

    public int addDigitsShort(int num) {
        if(num==0)return 0;

        if(num %9 == 0)
            return 0;
//        else {
//            int mod = num %9;
//            int res = 9:mod;
//        }
        return num%9==0?9:num%9;
    }
}
