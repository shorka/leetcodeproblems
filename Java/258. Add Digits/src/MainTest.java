import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/22/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void addDigits() {
        assertEquals(7, new Main().addDigits(88));
    }
    @org.junit.jupiter.api.Test
    void addDigitsShort() {
        assertEquals(7, new Main().addDigitsShort(88));
    }
}