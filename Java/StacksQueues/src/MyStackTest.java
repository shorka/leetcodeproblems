import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 12/12/2018.
 */
class MyStackTest {

    private MyStack getMyStack(){
        //1 4 8 3
        MyStack obj = new MyStack();
        obj.push(1);
        obj.push(4);
        obj.push(8);
        obj.push(3);

        return obj;
    }

    private MyStack2 getMyStack2(){
        //1 4 8 3
        MyStack2 obj = new MyStack2();
        obj.push(1);
        obj.push(4);
        obj.push(8);
        obj.push(3);

        return obj;
    }

    private MyStack3 getMyStack3(){
        //1 4 8 3
        MyStack3 obj = new MyStack3();
        obj.push(1);
        obj.push(4);
        obj.push(8);
        obj.push(3);

        return obj;
    }
    @org.junit.jupiter.api.Test
    void pop() {

        MyStack myStack2 = getMyStack();
        String res = ""+ myStack2.pop();
        res += " " + myStack2.pop();
        res += " " + myStack2.pop();
        res += " " + myStack2.pop();
        assertEquals("3 8 4 1" , res);
    }

    @org.junit.jupiter.api.Test
    void pop2() {

        MyStack2 myStack = getMyStack2();
        String res = ""+ myStack.pop();
        res += " " + myStack.pop();
        res += " " + myStack.pop();
        res += " " + myStack.pop();
        assertEquals("3 8 4 1" , res);
    }

    @org.junit.jupiter.api.Test
    void pop3() {

        MyStack3 myStack = getMyStack3();
        String res = ""+ myStack.pop();
        res += " " + myStack.pop();
        res += " " + myStack.pop();
        res += " " + myStack.pop();
        assertEquals("3 8 4 1" , res);
    }


}