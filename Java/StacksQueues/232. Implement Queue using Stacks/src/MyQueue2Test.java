import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 12/12/2018.
 */
class MyQueue2Test {

    private MyQueue2 getMyQueue2(){
        //1 4 8 3
        MyQueue2 obj = new MyQueue2();
        obj.push(1);
        obj.push(4);
        obj.push(8);
        obj.push(3);

        return obj;
    }

    @org.junit.jupiter.api.Test
    void pop() {

        MyQueue2 myQueue2 = getMyQueue2();
        String res = ""+ myQueue2.pop();
        res += " " + myQueue2.pop();
        res += " " + myQueue2.pop();
        res += " " + myQueue2.pop();
        assertEquals("1 4 8 3" , res);
    }


}