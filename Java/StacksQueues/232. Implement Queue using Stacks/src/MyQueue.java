import java.util.LinkedList;

/**
 * Created by Kirill on 12/12/2018.
 */
public class MyQueue {

    private LinkedList<Integer> q1 = new LinkedList<>();

    public MyQueue() {

    }

    /**
     * Push element x to the back of queue.
     */
    public void push(int x) {
        q1.add(x);
    }

    /**
     * Removes the element from in front of queue and returns that element.
     */
    public int pop() {
        return q1.remove();
    }

    /**
     * Get the front element.
     */
    public int peek() {
        return q1.peek();
    }

    /**
     * Returns whether the queue is empty.
     */
    public boolean empty() {
        return q1.isEmpty();
    }

}
