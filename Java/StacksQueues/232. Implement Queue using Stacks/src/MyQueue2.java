import java.util.LinkedList;
import java.util.Stack;

/**
 * Created by Kirill on 12/12/2018.
 */
public class MyQueue2 {

    private int front;
    private Stack<Integer> s1 = new Stack<>();
    private Stack<Integer> s2 = new Stack<>();

    public MyQueue2() {

    }

    /**
     * Push element x to the back of queue.
     */
    public void push(int x) {

        if (s1.empty())
            front = x;

        while (!s1.isEmpty())
            s2.push(s1.pop());

        s2.push(x);
        while (!s2.isEmpty())
            s1.push(s2.pop());

    }

    /**
     * Removes the element from in front of queue and returns that element.
     */
    public int pop() {

        front = s1.pop();


        return front;
    }

    /**
     * Get the front element.
     */
    public int peek() {
        return front;
    }

    /**
     * Returns whether the queue is empty.
     */
    public boolean empty() {
        return s1.isEmpty();
    }

}
