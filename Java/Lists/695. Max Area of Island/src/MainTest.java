import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/18/2018.
 */
class MainTest {

    public int[][] Arr(){
        int[][] arr = new int[][]{
                {0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0}
        };

        return  arr;
    }

    @org.junit.jupiter.api.Test
    void maxAreaOfIsland() {
        assertEquals(6, new Main().maxAreaOfIsland(Arr()));
    }

    @org.junit.jupiter.api.Test
    void maxAreaOfIsland2() {
        assertEquals(6, new Main().maxAreaOfIsland2(Arr()));
    }
}