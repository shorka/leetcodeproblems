public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public boolean checkPerfectNumber(int num) {

        int sum = 0;
        for (int i = 1; i <= num/2; i++) {
            if (num % i == 0)
                sum += i;
        }

        return num == sum;
    }

    public boolean checkPerfectNumber2(int num) {
        if (num <= 0) {
            return false;
        }
        int sum = 0;
        for (int i = 1; i * i <= num; i++) {
            if (num % i == 0) {
                sum += i;
                if (i * i != num) {
                    sum += num / i;
                }

            }
        }
        return sum - num == num;
    }

    public boolean checkPerfectNumber3(int num) {
        if (num <= 1)
            return false;

        int sum = 1;

        int start = 2, end = num/2;

        while (start <= end) {
            if (num % start == 0) {
                sum += start;
                sum += num/start;
                end = num/start -1;
            }
            start ++;
        }

        return sum == num;
    }
}
