import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/22/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void checkPerfectNumber() {
        assertEquals(true, new Main().checkPerfectNumber(28));
    }
    @org.junit.jupiter.api.Test
    void checkPerfectNumber2() {
        assertEquals(true, new Main().checkPerfectNumber2(144));
    }

    @org.junit.jupiter.api.Test
    void checkPerfectNumber3() {
        assertEquals(true, new Main().checkPerfectNumber3(144));
    }
}