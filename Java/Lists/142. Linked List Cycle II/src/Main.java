import java.util.HashSet;

public class Main {

    public static void main(String[] args) {
	// write your code here
    }

    public ListNode detectCycle(ListNode head) {
        if(head==null)
            return null;

//        HashSet<ListNode> hashSet = new HashSet<>();
//        ListNode currHead = head;
//        hashSet.add(currHead);
//        while (currHead != null && currHead.next != null) {
//
//            if (!hashSet.contains(currHead.next))
//                hashSet.add(currHead.next);
//
//            else
//                return currHead.next;
//
//            currHead = currHead.next;
//        }
        ListNode walker = head;
        ListNode runner = head;
        while(runner.next!=null && runner.next.next!=null) {
            walker = walker.next;
            runner = runner.next.next;
            if(walker==runner) return runner.next;
        }

        return null;
    }

    public ListNode detectCycleNOSpace(ListNode head) {
        ListNode slow = head;
        ListNode fast = head;

        while (fast!=null && fast.next!=null){
            fast = fast.next.next;
            slow = slow.next;

            if (fast == slow){
                ListNode slow2 = head;
                while (slow2 != slow){
                    slow = slow.next;
                    slow2 = slow2.next;
                }
                return slow;
            }
        }
        return null;
    }
}
