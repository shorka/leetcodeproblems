import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 3/1/2018.
 */
class MainTest {

    public ListNode nodeCycled(){
        ListNode l8 = new ListNode(8);
        ListNode ll00 = new ListNode(100);
        ListNode l12 = new ListNode(12);
        ListNode l1 = new ListNode(1);
        ListNode l2 = new ListNode(2);
        ListNode l3 = new ListNode(3);
        ListNode l4 = new ListNode(4);

        l8.next = ll00;
        ll00.next = l2;
        l12.next = l1;
        l1.next = l2;
        l2.next = l3;
        l3.next = l4;
        l4.next = l1;
        return ll00;
    }

    @org.junit.jupiter.api.Test
    void detectCycleNOSpace() {
        assertEquals(nodeCycled().next, new Main().detectCycleNOSpace(nodeCycled()));
    }

}