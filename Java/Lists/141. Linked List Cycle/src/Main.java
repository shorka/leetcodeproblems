import java.util.HashSet;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public boolean hasCycle(ListNode head) {
        if (head == null)
            return false;

        HashSet<ListNode> hashSet = new HashSet<>();
        ListNode currHead = head;
        hashSet.add(currHead);
        while (currHead != null && currHead.next != null) {

            if (!hashSet.contains(currHead.next))
                hashSet.add(currHead.next);

            else
                return true;

            currHead = currHead.next;
        }
        return false;
    }

    public boolean hasCycleO1(ListNode head) {
        if(head==null)
            return false;

        ListNode walker = head;
        ListNode runner = head;
        while(runner.next!=null && runner.next.next!=null) {
            walker = walker.next;
            runner = runner.next.next;
            if(walker==runner) return true;
        }
        return false;
    }
}
