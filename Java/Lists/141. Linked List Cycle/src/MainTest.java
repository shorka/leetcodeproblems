import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 3/1/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void hasCycle() {
        ListNode l1 = new ListNode(1);
        ListNode l2 = new ListNode(2);
        ListNode l3 = new ListNode(3);
        ListNode l4 = new ListNode(4);

        l1.next = l2;
        l2.next = l3;
        l3.next = l4;

        assertEquals(false, new Main().hasCycle(l1));
    }

    public ListNode nodeCycled(){
        ListNode l1 = new ListNode(1);
        ListNode l2 = new ListNode(2);
        ListNode l3 = new ListNode(3);
        ListNode l4 = new ListNode(4);

        l1.next = l2;
        l2.next = l3;
        l3.next = l4;
        l4.next = l1;
        return l1;
    }

    @org.junit.jupiter.api.Test
    void hasCycle2() {
        assertEquals(true, new Main().hasCycle(nodeCycled()));
    }

    @org.junit.jupiter.api.Test
    void hasCycle2_Leet() {
        assertEquals(true, new Main().hasCycleO1(nodeCycled()));
    }
}