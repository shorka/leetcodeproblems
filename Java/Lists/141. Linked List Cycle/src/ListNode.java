/**
 * Created by Kirill on 2/13/2018.
 */

// Definition for singly-linked list.
  public class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
  }
