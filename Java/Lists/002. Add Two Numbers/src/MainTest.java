import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/27/2018.
 */
class MainTest {

    private ListNode Node1() {
        ListNode n1 = new ListNode(2);
        ListNode n2 = new ListNode(4);
        ListNode n3 = new ListNode(3);
        ListNode n4 = new ListNode(7);
        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        return n1;
    }

    private ListNode Node2() {
        ListNode n1 = new ListNode(5);
        ListNode n2 = new ListNode(6);
        ListNode n3 = new ListNode(4);

        n1.next = n2;
        n2.next = n3;
        return n1;
    }

    private ListNode Node3() {
        ListNode n1 = new ListNode(7);
        ListNode n2 = new ListNode(0);
        ListNode n3 = new ListNode(8);

        n1.next = n2;
        n2.next = n3;
        return n1;
    }

    @org.junit.jupiter.api.Test
    void addTwoNumbers708() {

        assertEquals(Node3(), new Main().addTwoNumbers(Node1(), Node2()));
    }

    @org.junit.jupiter.api.Test
    void addTwoNumbers0() {

        ListNode n0 = new ListNode(0);
        assertEquals(n0, new Main().addTwoNumbers(n0, n0));
    }

    public ListNode Node199999() {
        ListNode n9_0 = new ListNode(1);
        ListNode n9_1 = new ListNode(9);
        ListNode n9_2 = new ListNode(9);
        ListNode n9_3 = new ListNode(9);
        ListNode n9_4 = new ListNode(9);
        ListNode n9_5 = new ListNode(9);
        ListNode n9_6 = new ListNode(9);
        ListNode n9_7 = new ListNode(9);
        ListNode n9_8 = new ListNode(9);
        ListNode n9_9 = new ListNode(9);

        n9_0.next = n9_1;
        n9_1.next = n9_2;
        n9_2.next = n9_3;
        n9_3.next = n9_4;
        n9_4.next = n9_5;
        n9_5.next = n9_6;
        n9_6.next = n9_7;
        n9_7.next = n9_8;
        n9_8.next = n9_9;
        return n9_0;
    }

    @org.junit.jupiter.api.Test
    void addTwoNumbers100000() {
        ListNode n9 = new ListNode(9);
        assertEquals(null, new Main().addTwoNumbers(n9, Node199999()));
    }
}