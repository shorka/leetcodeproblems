import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 12/26/2018.
 */
class MainTest {

    public ListNode getNode1(){
        ListNode node1 = new ListNode(1);
        ListNode node2= new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(5);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;

        return node1;
    }

    public ListNode getNode2(){
        ListNode node1 = new ListNode(1);
        ListNode node2= new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node5 = new ListNode(5);

        node1.next = node2;
        node2.next = node3;
        node3.next = node5;

        return node1;
    }

    public ListNode getNode_only1(){
        return new ListNode(1);
    }

    public ListNode getNode_1_2(){
        ListNode node1 = new ListNode(1);
        ListNode node2= new ListNode(2);
        node1.next = node2;
        return node1;
    }

    @org.junit.jupiter.api.Test
    void removeNthFromEnd() {
        assertEquals(getNode2(), new Main().removeNthFromEnd(getNode1(),2));
    }

    @org.junit.jupiter.api.Test
    void removeNthFromEnd_Leetcode1() {
        assertEquals(getNode2(), new Main().removeNthFromEnd_Leetcode1(getNode1(),2));
    }
    @org.junit.jupiter.api.Test
    void removeNthFromEnd_Leetcode2() {
        assertEquals(getNode2(), new Main().removeNthFromEnd_Leetcode2(getNode1(),2));
    }





    @org.junit.jupiter.api.Test
    void removeNthFromEnd_one() {
        assertEquals(null, new Main().removeNthFromEnd(getNode_only1(),1));
    }

    @org.junit.jupiter.api.Test
    void removeNthFromEnd_one_Leetcode1() {
        assertEquals(null, new Main().removeNthFromEnd_Leetcode1(getNode_only1(),1));
    }

    @org.junit.jupiter.api.Test
    void removeNthFromEnd_one_Leetcode2() {
        assertEquals(null, new Main().removeNthFromEnd_Leetcode2(getNode_only1(),1));
    }



    @org.junit.jupiter.api.Test
    void removeNthFromEnd_1_2() {
        assertEquals(new ListNode(2), new Main().removeNthFromEnd(getNode_1_2(),2));
    }
}