/**
 * Created by Kirill on 12/26/2018.
 */
public class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }
}
