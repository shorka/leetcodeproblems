import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public ListNode removeNthFromEnd(ListNode head, int n) {

        Stack<ListNode> stack = new Stack<>();

        ListNode currNode = head;
        while (currNode != null) {
            stack.add(currNode);
            currNode = currNode.next;
        }

        int sizeOfStack = stack.size();
        if (n > sizeOfStack)
            return head;

        ListNode resHead = head;
        ListNode prev = null;
        for (int i = 0; i < n; i++) {

            ListNode iNode = stack.pop();
            if (i == n - 1) {
                iNode.next = null;
                if (!stack.isEmpty())
                    stack.pop().next = prev;
                else
                    resHead = prev;
            } else
                prev = iNode;
        }

        return resHead;
    }

    public ListNode removeNthFromEnd_Leetcode1(ListNode head, int n) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        int length  = 0;
        ListNode first = head;
        while (first != null) {
            length++;
            first = first.next;
        }
        length -= n;
        first = dummy;
        while (length > 0) {
            length--;
            first = first.next;
        }
        first.next = first.next.next;
        return dummy.next;
    }

    public ListNode removeNthFromEnd_Leetcode2(ListNode head, int n) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode first = dummy;
        ListNode second = dummy;
        // Advances first pointer so that the gap between first and second is n nodes apart
        for (int i = 1; i <= n + 1; i++) {
            first = first.next;
        }
        // Move first to the end, maintaining the gap
        while (first != null) {
            first = first.next;
            second = second.next;
        }
        second.next = second.next.next;
        return dummy.next;
    }
}
