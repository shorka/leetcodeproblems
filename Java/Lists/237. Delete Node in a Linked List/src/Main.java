public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public ListNode deleteNode(ListNode node) {

        node.val = node.next.val;
        node.next = node.next.next;

        return  node;
    }
}
