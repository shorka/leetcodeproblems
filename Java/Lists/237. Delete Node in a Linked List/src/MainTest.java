import java.util.ListIterator;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/13/2018.
 */
class MainTest {

    public ListNode Node1() {

        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;

        return node1;
    }

    public ListNode Node2() {

        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node4 = new ListNode(4);

        node1.next = node2;
        node2.next = node4;

        return node1;
    }

    @org.junit.jupiter.api.Test
    void deleteNode() {
        assertEquals(Node2(), new Main().deleteNode(Node1()));
    }

}