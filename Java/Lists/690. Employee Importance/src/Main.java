import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    Map<Integer, Employee> emap;

    public int getImportance(List<Employee> employees, int id) {
        emap = new HashMap();
        for (Employee e : employees)
            emap.put(e.id, e);

        return dfs(id);
    }

    public int dfs(int id) {
        Employee employee = emap.get(id);
        int ans = employee.importance;
        for (Integer subid : employee.subordinates)
            ans += dfs(subid);

        return ans;
    }
}
