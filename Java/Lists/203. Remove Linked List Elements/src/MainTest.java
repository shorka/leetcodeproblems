import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/18/2018.
 */
class MainTest {

    public ListNode L1() {
        ListNode l1 = new ListNode(1);
        ListNode l2 = new ListNode(2);
        ListNode l3 = new ListNode(6);
        ListNode l4 = new ListNode(3);
        ListNode l5 = new ListNode(4);
        ListNode l6 = new ListNode(5);
        ListNode l7 = new ListNode(6);

        l1.next = l2;
        l2.next = l3;
        l3.next = l4;
        l4.next = l5;
        l5.next = l6;
        l6.next = l7;

        return l1;
    }

    public ListNode L2() {
        ListNode l1 = new ListNode(1);
        ListNode l2 = new ListNode(2);
        ListNode l4 = new ListNode(3);
        ListNode l5 = new ListNode(4);
        ListNode l6 = new ListNode(5);

        l1.next = l2;
        l2.next = l4;
        l4.next = l5;
        l5.next = l6;

        return l1;
    }

    @org.junit.jupiter.api.Test
    void removeElements() {

        assertEquals(L2(), new Main().removeElements(L1(), 6));
    }

    @org.junit.jupiter.api.Test
    void removeElements1() {

        assertEquals(null, new Main().removeElements(new ListNode(1), 1));
    }

    @org.junit.jupiter.api.Test
    void removeElementsEmpty() {

        assertEquals(null, new Main().removeElements(null, 6));
    }

}