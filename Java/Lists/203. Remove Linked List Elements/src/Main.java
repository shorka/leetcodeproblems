public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public ListNode removeElements_brute(ListNode head, int val) {

        if (head == null)
            return null;

        ListNode currHead = head;
        while (currHead != null) {

            if (currHead.next != null) {
                if (currHead.val == val) {
                    currHead.val = currHead.next.val;
                    currHead.next = currHead.next.next;
                } else if (currHead.next.next == null && currHead.next.val == val) {
                    currHead.next = null;
                    break;
                }
            } else {
                if (currHead.val == val)
                    head = null;
                break;
            }

            currHead = currHead.next;
        }

        return head;
    }

    public ListNode removeElements(ListNode head, int val) {
        while (head != null && head.val == val) head = head.next;
        ListNode curr = head;
        while (curr != null && curr.next != null) {
            if (curr.next.val == val)
                curr.next = curr.next.next;
            else
                curr = curr.next;
        }
        return head;
    }
}
