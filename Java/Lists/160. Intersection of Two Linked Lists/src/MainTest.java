import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/21/2018.
 */
class MainTest {

    public ListNode L1(){
        ListNode l5 = new ListNode(5);
        ListNode l7 = new ListNode(7);

        l5.next = l7;
        l7.next = LCommon();

        return l5;
    }

    public ListNode L2(){
        ListNode l12 = new ListNode(12);
        ListNode l11 = new ListNode(11);
        ListNode l6 = new ListNode(6);

        l12.next = l11;
        l11.next = l6;
        l6.next = LCommon();

        return l12;
    }

    public ListNode LCommon(){
        ListNode l8 = new ListNode(8);
        ListNode l3 = new ListNode(3);
        ListNode l1 = new ListNode(1);
        l8.next = l3;
        l3.next = l1;

        return l8;
    }


    public ListNode L3(){
        ListNode l0 = new ListNode(0);
        ListNode l5 = new ListNode(5);
        ListNode l7 = new ListNode(7);

        l0.next = l5;
        l5.next = l7;
        l7.next = LCommon();

        return l0;
    }


    public ListNode L_NoCommon(){
        ListNode l0 = new ListNode(0);
        ListNode l5 = new ListNode(5);
        ListNode l7 = new ListNode(7);

        l0.next = l5;
        l5.next = l7;

        return l0;
    }

    @org.junit.jupiter.api.Test
    void getIntersectionNode() {
        assertEquals(LCommon(), new Main().getIntersectionNode_Leet(L1(),L2()));
    }
    @org.junit.jupiter.api.Test
    void getIntersectionNode_Null() {
        assertEquals(null, new Main().getIntersectionNode_Leet(L2(),L_NoCommon()));
    }
}