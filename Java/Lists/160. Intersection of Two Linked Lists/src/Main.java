public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {

        ListNode nodeStartA = headA;
        ListNode nodeStartB = headB;

        while (nodeStartA != null) {

            while (nodeStartB != null){

                if (nodeStartA == nodeStartB)
                    return nodeStartA;

                nodeStartB = nodeStartB.next;
            }

            nodeStartB = headB;
            nodeStartA = nodeStartA.next;
        }

        return null;
    }

    public ListNode getIntersectionNode_Leet(ListNode headA, ListNode headB) {
        //boundary check
        if(headA == null || headB == null)
            return null;

        ListNode a = headA;
        ListNode b = headB;

        //if a & b have different len, then we will stop the loop after second iteration
        while( a != b){
            //for the end of first iteration, we just reset the pointer to the head of another linkedlist
            a = a == null? headB : a.next;
            b = b == null? headA : b.next;
        }

        return a;
    }
}
