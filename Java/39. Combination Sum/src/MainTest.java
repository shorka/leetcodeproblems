import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/12/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void combinationSum() {

        List<List<Integer>> res = new ArrayList<>();
        List<Integer> list1 = Arrays.asList(7);
        List<Integer> list2 = Arrays.asList(2,2,3);

        res.add(list1);
        res.add(list2);

        int[] arr = new int[]{2,3,6,7};
        assertEquals(res, new Main().combinationSum(arr,7));

    }

    @org.junit.jupiter.api.Test
    void combinationSum1() {

        List<List<Integer>> res = new ArrayList<>();
        List<Integer> list1 = Arrays.asList(7);
        List<Integer> list2 = Arrays.asList(2,2,3);

        res.add(list1);
        res.add(list2);

        int[] arr = new int[]{10,1,2,7,6,1,6};
        assertEquals(res, new Main().combinationSum(arr,8));
    }
}