/**
 * Created by Kirill on 12/18/2018.
 */
fun main(args: Array<String>) {

    println("Hello world")
}

fun hammingDistance(x: Int, y: Int): Int {

    var xorRes = x xor y
    var res: Int = 0
    while (xorRes>0){

        res += xorRes and 1
        xorRes = xorRes shr 1
    }

    return res
}