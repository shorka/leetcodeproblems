import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/14/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void findComplement5() {
        assertEquals(2, new Main().findComplement(5));
    }

    @org.junit.jupiter.api.Test
    void findComplement2() {
        assertEquals(1, new Main().findComplement(2));
    }

    @org.junit.jupiter.api.Test
    void findComplement5_L() {
        assertEquals(2, new Main().findComplement_L1(5));
    }

    @org.junit.jupiter.api.Test
    void findComplement2_L() {
        assertEquals(1, new Main().findComplement_L1(2));
    }

    @org.junit.jupiter.api.Test
    void findComplement5_L2() {
        assertEquals(2, new Main().findComplement_L2(5));
    }

    @org.junit.jupiter.api.Test
    void findComplement2_L2() {
        assertEquals(1, new Main().findComplement_L2(2));
    }
    @org.junit.jupiter.api.Test
    void findComplement2_L4() {
        assertEquals(1, new Main().findComplement_L3(16));
    }

    @org.junit.jupiter.api.Test
    void findComplement2_L4_2() {
        assertEquals(2, new Main().findComplement_L4(5));
    }

    @org.junit.jupiter.api.Test
    void findComplement2_L4_3() {
        assertEquals(2, new Main().findComplement_L4(16));
    }
}