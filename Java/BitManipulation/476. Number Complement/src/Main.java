public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public int findComplement(int num) {
        String bNnum = Integer.toBinaryString(num);
        String bNnum2 = Integer.toBinaryString(~num);

        String strRes = "";
        int len = bNnum.length();
        for (int i = 0; i < len; i++) {
            strRes = String.valueOf(bNnum2.charAt(bNnum2.length() - 1 - i)) + strRes;
        }

        return Integer.parseInt(strRes, 2);
    }

    public int findComplement_L1(int num) {

        int highestBit = Integer.highestOneBit(num);
        int hShift = highestBit << 1;

        int mask = hShift - 1;
        num = ~num;
        int res = num & mask;
        return res;
    }

    int findComplement_L2(int num) {
        int mask = num;
        mask |= mask >> 1;
        int numb2 = mask >>1;

        mask |= mask >> 2;
        int numb3 = mask >>1;
        String Snumb3 = Integer.toBinaryString(numb3);

        mask |= mask >> 4;
        int numb4 = mask >>1;
        String Snumb4 = Integer.toBinaryString(numb4);

        mask |= mask >> 8;
        int numb5 = mask >>1;
        String Snumb5 = Integer.toBinaryString(numb5);
        mask |= mask >> 16;

        return num ^ mask;
    }

    public int findComplement_L3(int num) {

        int highestBit = highestOneBit(num);

        int mask = highestBit - 1;
        num = ~num;

        String bNnum = Integer.toBinaryString(num);
        String bNnum2 = Integer.toBinaryString(mask);

        int res = num & mask;

        return res;
    }

    private int highestOneBit(int i) {
        // HD, Figure 3-1
        i |= (i >>  1);
        i |= (i >>  2);
        i |= (i >>  4);
        i |= (i >>  8);
        i |= (i >> 16);
        return i - (i >>> 1);
    }

    public int findComplement_L4(int num)
    {
        int i = 0;
        int j = 0;

        while (i < num)
        {
            i += Math.pow(2, j);
            j++;
        }

        return i - num;
    }
}
