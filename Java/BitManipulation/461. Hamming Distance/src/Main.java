public class Main {

    public static void main(String[] args) {
        Main m = new Main();
        System.out.println("dist = " + m.hammingDistance_Brute(3, 123131) + "]");
    }

    public int hammingDistance_Brute(int x, int y) {
        String bx = Integer.toBinaryString(x);
        String by = Integer.toBinaryString(y);
        System.out.println("bx = [" + bx + "], by = [" + by + "]");

        int res = 0;

        int max = Math.max(bx.length(), by.length());
        String bMin = "";
        String bMax = "";

        if (bx.length() == max) {
            bMax = bx;
            bMin = by;
        } else if (by.length() == max) {
            bMax = by;
            bMin = bx;
        }

        for (int i = 0; i < max; i++) {

            char chBMax = bMax.charAt(bMax.length() - 1 - i);
            if (bMin.length() <= i) {
                if (chBMax == '1')
                    res++;

                continue;
            }

            if (chBMax != bMin.charAt(bMin.length() - 1 - i))
                res++;
        }

        return res;
    }

    public int hammingDistance_OneLine(int x, int y) {
        return Integer.bitCount(x ^ y);
    }

    public int hammingDistance_2(int x, int y) {
        int xor = x ^ y;
        int res = 0;
        while (xor != 0) {
            res += xor & 1;
            xor >>= 1;
        }
        return res;
    }
}
