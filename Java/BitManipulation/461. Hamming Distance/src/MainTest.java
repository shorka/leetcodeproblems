import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/12/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void hammingDistance2() {
        Main m = new Main();
        assertEquals(2,m.hammingDistance_Brute(4,14));
    }

    @org.junit.jupiter.api.Test
    void hammingDistance2_4() {
        Main m = new Main();
        assertEquals(2,m.hammingDistance_Brute(1,4));
    }

    @org.junit.jupiter.api.Test
    void hammingDistance2_OneLine() {
        Main m = new Main();
        assertEquals(2,m.hammingDistance_OneLine(4,14));
    }

    @org.junit.jupiter.api.Test
    void hammingDistance2_4_OneLine() {
        Main m = new Main();
        assertEquals(2,m.hammingDistance_OneLine(1,4));
    }

    @org.junit.jupiter.api.Test
    void hammingDistance2_2nSolution() {
        Main m = new Main();
        assertEquals(2,m.hammingDistance_2(4,14));
    }

    @org.junit.jupiter.api.Test
    void hammingDistance2_2_2nSolution() {
        Main m = new Main();
        assertEquals(2,m.hammingDistance_2(1,4));
    }
}