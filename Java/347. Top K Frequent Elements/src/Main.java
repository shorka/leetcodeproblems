import java.util.*;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public List<Integer> topKFrequent(int[] nums, int k) {

        HashMap<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {

            if (map.containsKey(num))
                map.put(num, map.get(num) + 1);

            else
                map.put(num, 1);

            map.put(num,
                    map.containsKey(num) ? map.get(num) + 1 : 1);
        }


        ArrayList<Integer> listRes = new ArrayList<>(k);
        int count = k;
        while (count > 0) {

            int maxKey = 0;
            int maxVal = Integer.MIN_VALUE;
            for (int key : map.keySet()) {

                int iVal = map.get(key);
                if (maxVal < iVal) {
                    maxKey = key;
                    maxVal = iVal;
                }
            }

            map.remove(maxKey);
            listRes.add(maxKey);
            count--;
        }


        return listRes;
    }

    public List<Integer> topKFrequent_Leet(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        for(int n: nums){
            map.put(n, map.getOrDefault(n,0)+1);
        }

        // corner case: if there is only one number in nums, we need the bucket has index 1.
        List<Integer>[] bucket = new List[nums.length+1];
        for(int n:map.keySet()){
            int freq = map.get(n);
            if(bucket[freq]==null)
                bucket[freq] = new LinkedList<>();
            bucket[freq].add(n);
        }

        List<Integer> res = new LinkedList<>();
        for(int i=bucket.length-1; i>0 && k>0; --i){
            if(bucket[i]!=null){
                List<Integer> list = bucket[i];
                res.addAll(list);
                k-= list.size();
            }
        }

        return res;
    }
}
