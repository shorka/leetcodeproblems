import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/29/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void topKFrequent_2() {

        int[] nums = new int[]{1,1,1,2,2,3};
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);

        assertEquals(list, new Main().topKFrequent(nums,2));
    }

    @org.junit.jupiter.api.Test
    void topKFrequent_Leet_2() {

        int[] nums = new int[]{1,1,1,2,2,3};
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);

        assertEquals(list, new Main().topKFrequent_Leet(nums,2));
    }

}