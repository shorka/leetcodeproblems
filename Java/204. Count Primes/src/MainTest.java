import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/22/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void countPrimes() {
        assertEquals(4, new Main().countPrimes(28));
    }
    @org.junit.jupiter.api.Test
    void countPrimes2() {
        assertEquals(4, new Main().countPrimes2(28));
    }
}