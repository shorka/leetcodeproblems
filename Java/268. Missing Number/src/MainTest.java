import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/27/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void missingNumber8() {
        int[] arr = new int[]{9, 6, 4, 2, 3, 5, 7, 0, 1};
        assertEquals(8, new Main().missingNumber(arr));
    }

    @org.junit.jupiter.api.Test
    void missingNumber2() {
        int[] arr = new int[]{3,0,1};
        assertEquals(2, new Main().missingNumber(arr));
    }

}