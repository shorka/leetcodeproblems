import java.util.HashSet;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public int missingNumber(int[] nums) {

        HashSet<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            set.add(nums[i]);
        }

        int max = 0;
        for (int i = 0; i < nums.length; i++) {

            if (!set.contains(i))
                return i;

            if(max < i)
                max = i;
        }

        return max+1;
    }
}
