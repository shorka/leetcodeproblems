import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/13/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void selfDividingNumbers() {

        List<Integer> list = Arrays.asList(1, 2, 3, 4,5,6,7,8,9,11,12,15,22);


        assertEquals(list, new Main().selfDividingNumbers(1, 22));
    }

}