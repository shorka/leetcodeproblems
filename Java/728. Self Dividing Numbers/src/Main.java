import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public List<Integer> selfDividingNumbers(int left, int right) {

        List<Integer> list = new LinkedList<>();

        for (int i = left; i <= right; i++) {

            int numb = i;
            boolean isSelfDiv = true;
            while (isSelfDiv && numb >0) {
                int digit = numb % 10;
                if (digit == 0){
                    isSelfDiv = false;
                    break;
                }

                if(i%digit != 0 ){
                    isSelfDiv = false;
                    break;
                }
                numb /= 10;
                if (numb == 0)
                    break;
            }

            if(isSelfDiv)
                list.add(i);
        }

        return list;
    }
}
