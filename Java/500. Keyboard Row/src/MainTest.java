import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/7/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void findWords() {

        String[] arr = new String[]{"Hello", "Alaska", "Dad", "Peace"};
        String[] res = new String[]{"Alaska", "Dad"};

        assertArrayEquals(res, new Main().findWords(arr));
    }

    @org.junit.jupiter.api.Test
    void findWordsLeet() {

        String[] arr = new String[]{"Hello", "Alaska", "Dad", "Peace"};
        String[] res = new String[]{"Alaska", "Dad"};

        assertArrayEquals(res, new Main().findWordsLeet(arr));
    }
}