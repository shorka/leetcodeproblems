import java.util.*;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public String[] findWords(String[] words) {

        HashSet<String> hashSet = new HashSet<>();

//        hashSet.add(new char[]{'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'});
//        hashSet.add(new char[]{'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'});
//        hashSet.add(new char[]{'z', 'x', 'c', 'v', 'b', 'n', 'm'});

        hashSet.add("qwertyuiop");
        hashSet.add("asdfghjkl");
        hashSet.add("zxcvbnm");

        List<String> listRes = new ArrayList<String>();
        for (String word : words) {

            char[] chars = word.toCharArray();
            if (isWordOnLine(hashSet, word))
                listRes.add(word);

        }

        return listRes.toArray(new String[0]);
    }

    public String[] findWordsLeet(String[] words) {
        String[] strs = {"QWERTYUIOP","ASDFGHJKL","ZXCVBNM"};
        Map<Character, Integer> map = new HashMap<>();
        for(int i = 0; i<strs.length; i++){
            for(char c: strs[i].toCharArray()){
                map.put(c, i);//put <char, rowIndex> pair into the map
            }
        }
        List<String> res = new LinkedList<>();
        for(String w: words){
            if(w.equals(""))
                continue;

            int index = map.get(w.toUpperCase().charAt(0));

            for(char c: w.toUpperCase().toCharArray()){
                if(map.get(c)!=index){
                    index = -1; //don't need a boolean flag.
                    break;
                }
            }
            if(index!=-1) res.add(w);//if index != -1, this is a valid string
        }
        return res.toArray(new String[0]);
    }

    private boolean isWordOnLine(HashSet<String> hashSet, String word) {
        char[] chars = word.toCharArray();

        Iterator<String> i = hashSet.iterator();
        while (i.hasNext()) {
            boolean isWordMatch = true;

            String strKey = i.next();
            for (char ch : chars) {
                if (!strKey.contains(String.valueOf(ch).toLowerCase())) {
                    isWordMatch = false;
                    break;
                }
            }

            if (isWordMatch)
                return true;
        }
        return false;
    }

}
