import java.util.Dictionary;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public boolean judgeCircle(String moves) {

        int[]coor = new int[]{0, 0};

        for (int i = 0; i < moves.length(); i++) {

            char item = moves.charAt(i);

            if(item == 'D')
                coor[1]--;

            else if(item == 'U')
                coor[1]++;

            else if(item == 'L')
                coor[0]--;

            else if(item == 'R')
                coor[0]++;
        }

        return (coor[0] == 0 && coor[1] == 0);
    }
}
