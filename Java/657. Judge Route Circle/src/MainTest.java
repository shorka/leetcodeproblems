import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/15/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void judgeCircleUD() {
        assertEquals(true, new Main().judgeCircle("UD"));
    }

    @org.junit.jupiter.api.Test
    void judgeCircleLL() {
        assertEquals(false, new Main().judgeCircle("LL"));
    }

    @org.junit.jupiter.api.Test
    void judgeCircleDRUL() {
        assertEquals(true, new Main().judgeCircle("DRUL"));
    }
}