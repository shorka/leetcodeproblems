public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public boolean escapeGhosts(int[][] ghosts, int[] target) {
        int[] source = new int[]{0, 0};
        for (int[] ghost: ghosts)
            if (taxi(ghost, target) <= taxi(source, target))
                return false;
        return true;
    }

    public int taxi(int[] P, int[] Q) {
        //dist(A, B) = abs(A.x - B.x) + abs(A.y - B.y).
        int dist =Math.abs(P[0] - Q[0]) + Math.abs(P[1] - Q[1]);
        return  dist;
    }
}
