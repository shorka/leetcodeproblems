import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/26/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void escapeGhosts1() {
        int[][] ghost = new int[][]{{1, 0}, {0, 3}};
        assertEquals(true, new Main().
                escapeGhosts(ghost, new int[]{0, 1})
        );
    }

    @org.junit.jupiter.api.Test
    void escapeGhosts2() {
        int[][] ghost = new int[][]{{1, 0}, {1, 0}};
        assertEquals(false, new Main().
                escapeGhosts(ghost, new int[]{2, 0})
        );
    }

    @org.junit.jupiter.api.Test
    void escapeGhosts3() {
        int[][] ghost = new int[][]{{2, 0}, {2, 0}};
        assertEquals(false, new Main().
                escapeGhosts(ghost, new int[]{1, 0})
        );
    }
}