import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/23/2018.
 */
class MainTest {

    public List<List<String>> listGroup(){
        List<List<String>> listGroup = new ArrayList<List<String>>();
        List<String> list1 = new ArrayList<String>();
        list1.add("ate");
        list1.add("eat");
        list1.add("tea");

        List<String> list2 = new ArrayList<String>();
        list2.add("nat");
        list2.add("tan");

        List<String> list3 = new ArrayList<String>();
        list3.add("bat");

        listGroup.add(list1);
        listGroup.add(list2);
        listGroup.add(list3);

        return listGroup;
    }

    @org.junit.jupiter.api.Test
    void groupAnagrams() {

        String[] tmpStrings = new String[]{"eat", "tea", "tan", "ate", "nat", "bat"};
        assertEquals(listGroup(),new Main().groupAnagrams(tmpStrings));
    }

    @org.junit.jupiter.api.Test
    void groupAnagrams_L() {

        String[] tmpStrings = new String[]{"eat", "tea", "tan", "ate", "nat", "bat"};
        assertEquals(listGroup(),new Main().groupAnagrams_L(tmpStrings));
    }
}