import java.util.*;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> listGroup = new ArrayList<List<String>>();
        String[] tmpStrings = new String[strs.length];
        System.arraycopy( strs, 0, tmpStrings, 0, strs.length );


        for (int i = 0; i < tmpStrings.length; i++) {

            char[] ch = tmpStrings[i].toCharArray();
            Arrays.sort(ch);
            tmpStrings[i] = String.valueOf(ch);
        }

        HashSet<String> set = new HashSet<String>();
        for (int i = 0; i < tmpStrings.length; i++) {

            if(set.contains(tmpStrings[i]))
                continue;

            set.add(tmpStrings[i]);
            String itmpStr = tmpStrings[i];
            List<String> iList = new ArrayList<String>();
            iList.add(strs[i]);

            for (int j = 0; j < tmpStrings.length; j++) {
                if(i==j)
                    continue;

                if(itmpStr.equals(tmpStrings[j]))
                    iList.add(strs[j]);
            }

            listGroup.add(iList);
        }

        return listGroup;
    }

    public List<List<String>> groupAnagrams_L(String[] strs) {
        if (strs.length == 0)
            return new ArrayList();

        Map<String, List> ans = new HashMap<String, List>();
        for (String s : strs) {
            char[] ca = s.toCharArray();
            Arrays.sort(ca);
            String key = String.valueOf(ca);
            if (!ans.containsKey(key))
                ans.put(key, new ArrayList());

            ans.get(key).add(s);
        }
        return new ArrayList(ans.values());
    }
}
