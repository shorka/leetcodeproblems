import java.util.List;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public int rotatedDigits(int N) {

        int ans = 0;
        for (int i = 1; i <= N; i++) {
            int numb = i;
            Stack<Integer> digits = new Stack<Integer>();
            boolean dontSkipDigit = true;
            while (numb != 0) {
                int digit = numb % 10;

                if (digit == 2) digit = 5;
                else if (digit == 5) digit = 2;
                else if (digit == 6) digit = 9;
                else if (digit == 9) digit = 6;

                if (digit != 3 && digit != 4 && digit != 7){
                    digits.add(digit);
                }

                else{
                    dontSkipDigit = false;
                    break;
                }

                numb /= 10;
            }

            if(!dontSkipDigit)
                continue;

            int numbNew = 0;
            int size = digits.size();
            boolean wasChange = size > 0;
            while (!digits.isEmpty()){
                int digit = digits.pop();
                numbNew += digit* Math.pow(10, size-1);
                size--;
            }

            if (i != numbNew && wasChange)
                ans++;

        }

        return ans;
    }

    public int rotatedDigits_Dyn(int N){
        char[] A = String.valueOf(N).toCharArray();
        int K = A.length;

        int[][][] memo = new int[K+1][2][2];
        memo[K][0][1] = memo[K][1][1] = 1;
        for (int i = K - 1; i >= 0; --i) {
            for (int eqf = 0; eqf <= 1; ++eqf)
                for (int invf = 0; invf <= 1; ++invf) {
                    // We will compute ans = memo[i][eqf][invf],
                    // the number of good numbers with respect to N = A[i:].
                    // If eqf is true, we must stay below N, otherwise
                    // we can use any digits.
                    // Invf becomes true when we write a 2569, and it
                    // must be true by the end of our writing as all
                    // good numbers have a digit in 2569.
                    int ans = 0;
                    for (char d = '0'; d <= (eqf == 1 ? A[i] : '9'); ++d) {
                        if (d == '3' || d == '4' || d == '7') continue;
                        boolean invo = (d == '2' || d == '5' || d == '6' || d == '9');
                        ans += memo[i+1][d == A[i] ? eqf : 0][invo ? 1 : invf];
                    }
                    memo[i][eqf][invf] = ans;
                }
        }

        return memo[0][1][0];
    }

    public int rotatedDigits2(int N){
        // Count how many n in [1, N] are good.
        int ans = 0;
        for (int n = 1; n <= N; ++n)
            if (good(n, false)) ans++;
        return ans;
    }

    // Return true if n is good.
    // The flag is true iff we have an occurrence of 2, 5, 6, 9.
    public boolean good(int n, boolean flag) {
        if (n == 0) return flag;

        int d = n % 10;
        if (d == 3 || d == 4 || d == 7) return false;
        if (d == 0 || d == 1 || d == 8) return good(n / 10, flag);
        return good(n / 10, true);
    }
}
