import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/24/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void rotatedDigits4() {
        assertEquals(4, new Main().rotatedDigits(10));
    }

    @org.junit.jupiter.api.Test
    void rotatedDigits857() {
        assertEquals(247, new Main().rotatedDigits(857));
    }

    @org.junit.jupiter.api.Test
    void rotatedDigits857_D() {
        assertEquals(247, new Main().rotatedDigits_Dyn(857));
    }

    @org.junit.jupiter.api.Test
    void rotatedDigits2() {
        assertEquals(247, new Main().rotatedDigits2(857));
    }

    @org.junit.jupiter.api.Test
    void rotatedDigits28() {
        assertEquals(5, new Main().rotatedDigits2(28));
    }
}