import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/22/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void isHappy() {
        assertEquals(true, new Main().isHappy(19));
    }
    @org.junit.jupiter.api.Test
    void isHappy2() {
        assertEquals(false, new Main().isHappy(2));
    }
}