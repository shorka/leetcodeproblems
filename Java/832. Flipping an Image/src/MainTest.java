import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/5/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void flipAndInvertImage() {

        int[][] array = {{1, 1, 0}, {1, 0, 1}, {0, 0, 0}};
        int[][] arrayRes = {{1, 0, 0}, {0, 1, 0}, {1, 1, 1}};

        assertArrayEquals(arrayRes, new Main().flipAndInvertImage(array));

    }

    @org.junit.jupiter.api.Test
    void flipAndInvertImageLeet() {

        int[][] array = {{1, 1, 0}, {1, 0, 1}, {0, 0, 0}};
        int[][] arrayRes = {{1, 0, 0}, {0, 1, 0}, {1, 1, 1}};

        assertArrayEquals(arrayRes, new Main().flipAndInvertImageLeet(array));

    }

}