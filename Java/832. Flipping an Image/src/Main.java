/**
 * Created by Kirill on 6/5/2018.
 */
public class Main {

    public static void main(String[] args) {

    }

    public int[][] flipAndInvertImage(int[][] A) {
        int len = A[0].length;
        for (int i = 0; i < len; i++) {

            for (int j = 0; j < len / 2; j++) {
                int numBuf = A[i][j];
                A[i][j] = A[i][len - j - 1];
                A[i][len - j - 1] =numBuf;
            }

            for (int j = 0; j < len; j++) {
                A[i][j] ^= 1;
            }
        }
        return A;
    }

    public int[][] flipAndInvertImageLeet(int[][] A) {
        int C = A[0].length;
        for (int[] row: A)
            for (int i = 0; i < (C + 1) / 2; ++i) {
                int tmp = row[i];
                row[i] = row[C - 1 - i] ^ 1;
                row[C - 1 - i] = tmp ^ 1;
            }

        return A;
    }


}
