import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

    }

    public int[] twoSum(int[] numbers, int target) {

        Map<Integer, Integer> map = new HashMap();

        int len = numbers.length;
        for (int i = 0; i < len; i++)
            map.put(numbers[i], i);


        for (int i = 0; i < len; i++) {
            int complement = target - numbers[i];
            if(map.containsKey(complement)&& map.get(complement)!= i)
                return new int[]{i+1,map.get(complement)+1};
        }

        return null;
    }
}
