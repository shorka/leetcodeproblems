import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/25/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void twoSum() {
        int[] res = new int[]{1, 2};
        int[] numbers = new int[]{2, 7, 11, 15};
        assertEquals(res, new Main().twoSum(numbers, 9));
    }

}