import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/14/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void subsets() {

        List<List<Integer>> list = new ArrayList<>();
        list.add(Arrays.asList(3));
        list.add(Arrays.asList(1));
        list.add(Arrays.asList(2));
        list.add(Arrays.asList(1,2,3));
        list.add(Arrays.asList(1,3));
        list.add(Arrays.asList(2,3));
        list.add(Arrays.asList(1,2));
        list.add(null);

        assertEquals(list, new Main().subsets(new int[]{1,2,3}));
    }

}