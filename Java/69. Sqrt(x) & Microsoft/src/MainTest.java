import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/10/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void mySqrt6() {
        assertEquals(6, new Main().MySqrt(36));
    }

    @org.junit.jupiter.api.Test
    void mySqrt6_Binary() {
        assertEquals(6, new Main().MySqrt_Binary(36));
    }

    @org.junit.jupiter.api.Test
    void mySqrt0() {
        assertEquals(0, new Main().MySqrt(0));
    }
}