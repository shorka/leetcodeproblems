import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/13/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void combinationSum2() {
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> list1 = Arrays.asList(1,1,5);
        List<Integer> list2 = Arrays.asList(1,2,5);
        List<Integer> list3 = Arrays.asList(1,7);
        List<Integer> list4 = Arrays.asList(2,6);

        res.add(list1);
        res.add(list2);

        int[] arr = new int[]{10, 1,2,7,6,1,5};
        assertEquals(res, new Main().combinationSum2(arr,8));
    }

    @org.junit.jupiter.api.Test
    void combinationSum2_5() {
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> list1 = Arrays.asList(1,2,2);
        List<Integer> list2 = Arrays.asList(5);

        res.add(list1);
        res.add(list2);

        int[] arr = new int[]{2,5,2,1,2};
        assertEquals(res, new Main().combinationSum2(arr,5));
    }
}