public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public int findDuplicate(int[] nums) {

        int len = nums.length;
        for (int i = 0; i < len; i++) {
            int numbI = nums[i];
            for (int j = i+1; j < len; j++) {
                if(numbI == nums[j])
                    return numbI;
            }
        }

        return -1;
    }

    public int findDuplicateCycle(int[] nums) {
        // Find the intersection point of the two runners.
        int tortoise = nums[0];
        int hare = nums[0];
        do {
            tortoise = nums[tortoise];
            hare = nums[nums[hare]];
        } while (tortoise != hare);

        // Find the "entrance" to the cycle.
        int ptr1 = nums[0];
        int ptr2 = tortoise;
        while (ptr1 != ptr2) {
            ptr1 = nums[ptr1];
            ptr2 = nums[ptr2];
        }

        return ptr1;
    }
}
