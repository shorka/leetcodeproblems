import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 3/1/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void findDuplicate2() {
        int[] nums = new int[]{1,2,3,5,6,2,7,8};
        assertEquals(2,new Main().findDuplicate(nums));
    }

    @org.junit.jupiter.api.Test
    void findDuplicate2Cycle() {
        int[] nums = new int[]{1,2,3,5,6,2,7,8};
        assertEquals(2,new Main().findDuplicateCycle(nums));
    }

    @org.junit.jupiter.api.Test
    void findDuplicate1() {
        int[] nums = new int[]{1,1};
        assertEquals(1,new Main().findDuplicate(nums));
    }
}