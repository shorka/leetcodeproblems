import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/22/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void isUgly() {
        assertEquals(true, new Main().isUgly(8));
    }

    @org.junit.jupiter.api.Test
    void isUgly14() {
        assertEquals(false, new Main().isUgly(14));
    }
}