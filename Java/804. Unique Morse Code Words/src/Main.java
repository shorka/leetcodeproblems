import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Kirill on 6/4/2018.
 */
public class Main {

    public static  String[] morseCodes(){
        String[] morseCodes = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.",
                "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
        return morseCodes;
    }

    public static void main(String[] args) {
        String[] words = {"gin", "zen", "gig", "msg"};
        new Main().uniqueMorseRepresentations(words);
    }

    public int uniqueMorseRepresentations(String[] words) {


        Map<Character, String> mapAlpha = new HashMap<>();

        int count = 0;
        for (char alphabet = 'a'; alphabet <= 'z'; alphabet++) {

            System.out.println("alphabet = " + alphabet);
            if (count >= morseCodes().length) {
                System.out.println("count>= morseCodes.length");
                break;
            }
            mapAlpha.put(alphabet,morseCodes()[count]);
            count++;
        }

        System.out.println();
        HashSet<String> uniqueMorse = new HashSet<>();
        for (String word : words) {
            String morseWord = "";
            for (char ch: word.toCharArray()) {
                morseWord+= String.valueOf(mapAlpha.get(ch)) ;
            }
            uniqueMorse.add(morseWord);
            System.out.println("morseWord = " + morseWord);
        }

        return uniqueMorse.size();
    }

    public int uniqueMorseRepresentationsLeetcode(String[] words) {
        String[] MORSE = new String[]{".-","-...","-.-.","-..",".","..-.","--.",
                "....","..",".---","-.-",".-..","--","-.",
                "---",".--.","--.-",".-.","...","-","..-",
                "...-",".--","-..-","-.--","--.."};

        Set<String> seen = new HashSet();
        for (String word: words) {
            StringBuilder code = new StringBuilder();
            for (char c: word.toCharArray())
                code.append(MORSE[c - 'a']);
            seen.add(code.toString());
        }

        return seen.size();
    }

}
