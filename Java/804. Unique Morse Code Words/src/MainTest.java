import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/4/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void uniqueMorseRepresentations() {
        String[] words = {"gin", "zen", "gig", "msg"};

        assertEquals(2, new Main().uniqueMorseRepresentations(words));
    }

}