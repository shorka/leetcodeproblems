import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/6/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void spiralOrder() {

        int[][] arr = {{1, 2, 3},{4, 5, 6 },{7, 8, 9} };

        List<Integer> res = Arrays.asList(1,2,3,4,8,12,11,10,9,5,6,7);

        assertEquals(res, new Main().spiralOrder(arr));

    }

}