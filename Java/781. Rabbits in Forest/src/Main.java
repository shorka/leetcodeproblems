public class Main {

    public static void main(String[] args) {
        System.out.println("(-5%3) = " + (-5%3));

        System.out.println("(5%3) = " + (5%3));

        System.out.println("(100%8) = " + (100%8));
        System.out.println("(-100%8) = " + (-100%8));

        System.out.println("floorMod 100,8= " + Math.floorMod(100,8));
        System.out.println("floorMod -100,8= " + Math.floorMod(-100,8));
    }

    public int numRabbits(int[] answers) {
        int[] count = new int[1000];
        for (int x: answers) count[x]++;

        int ans = 0;
        for (int k = 0; k < 1000; ++k)
            ans += Math.floorMod(-count[k], k+1) + count[k];
        return ans;
    }
}
