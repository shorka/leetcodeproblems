import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

//    public List<List<Integer>> combine(int n, int k) {
//        int max = Math.max(n, k);
//        int min = Math.min(n, k);
//
//        List<List<Integer>> listRes = new ArrayList<>();
//        for (int i = 1; i <= max; i++) {
//            int j;
//            for (j = i + 1; j <= max; j++) {
//                if (i == j)
//                    continue;
//
//                if (i == min)
//                    listRes.add(Arrays.asList(i));
//
//                else
//                    listRes.add(Arrays.asList(i, j));
//            }
//
//            if (i == max && j == max + 1)
//                listRes.add(Arrays.asList(i));
//
//        }
//
//
//        return listRes;
//    }

    public static List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> combs = new ArrayList<List<Integer>>();
        combine(combs, new ArrayList<Integer>(), 1, n, k);
        return combs;
    }
    public static void combine(List<List<Integer>> combs, List<Integer> comb, int start, int n, int k) {
        if(k==0) {
            combs.add(new ArrayList<Integer>(comb));
            return;
        }
        for(int i=start;i<=n;i++) {
            comb.add(i);
            combine(combs, comb, i+1, n, k-1);
            comb.remove(comb.size()-1);
        }
    }

}
