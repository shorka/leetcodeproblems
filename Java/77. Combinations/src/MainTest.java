import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/26/2018.
 */
class MainTest {

    public List<List<Integer>> list(){
        List<List<Integer>> list = new ArrayList<>();
        list.add(Arrays.asList(1, 2));
        list.add(Arrays.asList(1, 3));
        list.add(Arrays.asList(1, 4));
        list.add(Arrays.asList(2, 3));
        list.add(Arrays.asList(2, 4));
        list.add(Arrays.asList(3, 4));
        return list;
    }

    public List<List<Integer>> list1(){
        List<List<Integer>> list = new ArrayList<>();
        list.add(Arrays.asList(1));
        return list;
    }

    public List<List<Integer>> list12(){
        List<List<Integer>> list = new ArrayList<>();
        list.add(Arrays.asList(1));
        list.add(Arrays.asList(2));
        return list;
    }

    @org.junit.jupiter.api.Test
    void combine() {
        assertEquals(list(),new Main().combine(4,2));
    }
    @org.junit.jupiter.api.Test
    void combine1_1() {
        assertEquals(list1(),new Main().combine(1,1));
    }

    @org.junit.jupiter.api.Test
    void combine1_2() {
        assertEquals(list12(),new Main().combine(1,2));
    }
}