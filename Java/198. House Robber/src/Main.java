public class Main {

    public static void main(String[] args) {
	// write your code here
    }

    public int robMyWrong(int[] nums) {

        int res = 0;
        for (int i = 0; i < nums.length; i++) {

            if(i+1 <= nums.length -1)
            {
                int max = Math.max(nums[i],nums[i+1]);
                if(nums[i+1] > nums[i]){
                    i ++;
                }
                i++;
                 res += max;
            }
            else {
                res+=nums[i];
            }
        }

        return res;
    }

    public int robLeetCode(int[] num){
        int a = 0;
        int b = 0;

        for (int i=0; i<num.length; i++)
        {
            if (i%2==0)
            {
                a = Math.max(a+num[i], b);
            }
            else
            {
                b = Math.max(a, b+num[i]);
            }
        }

        return Math.max(a, b);
    }

    public int robDynamic(int[] num) {
        int prevNo = 0;
        int prevYes = 0;
        for (int n : num) {
            int temp = prevNo;
            prevNo = Math.max(prevNo, prevYes);
            prevYes = n + temp;
        }
        return Math.max(prevNo, prevYes);
    }
}
