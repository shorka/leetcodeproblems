import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/10/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void rob17() {
        int[] intArray = new int[]{ 1,5,2,7,3,5 };
        assertEquals(17,new Main().robLeetCode(intArray));
    }

    @org.junit.jupiter.api.Test
    void rob14() {
        int[] intArray = new int[]{ 5,7,1,5,3,2 };
        assertEquals(14,new Main().robLeetCode(intArray));
    }

    @org.junit.jupiter.api.Test
    void rob1() {
        int[] intArray = new int[]{ 1 };
        assertEquals(1,new Main().robLeetCode(intArray));
    }

    @org.junit.jupiter.api.Test
    void rob0() {
        int[] intArray = new int[]{ 0 };
        assertEquals(0,new Main().robLeetCode(intArray));
    }

    @org.junit.jupiter.api.Test
    void rob4() {
        int[] intArray = new int[]{ 2,3,2 };
        assertEquals(4,new Main().robLeetCode(intArray));
    }

    @org.junit.jupiter.api.Test
    void rob11() {
        int[] intArray = new int[]{ 8,6,0,3,2 };
        assertEquals(11,new Main().robLeetCode(intArray));
    }

    @org.junit.jupiter.api.Test
    void rob11_Dynamic() {
        int[] intArray = new int[]{ 8,6,0,3,2 };
        assertEquals(11,new Main().robDynamic(intArray));
    }
}