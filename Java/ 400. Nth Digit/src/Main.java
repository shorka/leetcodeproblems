public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public int findNthDigit_my(int n) {

        int allDigitsCount = 0;
        for (int i = 1; i <= n; i++) {

            int digits = (int) Math.log10(i) + 1;

            if (i == n & digits == 1)
                return i;


            if (allDigitsCount + digits >= n) {
                int nDigit = 0;
                while (allDigitsCount + nDigit != n)
                    nDigit++;

                return digitOfNumb(i, nDigit - 1, digits);
            }

            allDigitsCount += digits;
        }

        return 0;
    }

    private int digitOfNumb(int numb, int nDigit, int digitsCount) {

        int currNumb = numb;
        int count = digitsCount - 1;
        while (currNumb > 0) {

            if (count == nDigit)
                return currNumb % 10;

            currNumb = currNumb / 10;
            count--;
        }
        return -1;
    }

    public  static int findNthDigit(int n) {
        int digitLvl = 1;//digit level, we start at one digit.
        long counts = 9;//the number counts of current digit level,we start at one digit,there are 9 numbers(1-9)which is at one digit.
        /**
         * number [1-9] (there are 9 numbers)is of one digit,number[10-99](there are 90 numbers) is
         * of two digits,number[100-999](there are 900 numbers) is of three digits,so first we should
         * find what level(i mean which digits(one digit,two digit or so on)  by level) the nth digit locate,
         * once we find the digit level, we achieve half the process,
         */
        /**
         *if n - digit * counts > 0,it means the nth digit is not at the current digit level,we should
         * increase digit level to pass more number
         */

        while (n - digitLvl * counts > 0) {
            //every time we pass the number at current digit level
            n -= digitLvl * counts;

            digitLvl++;
            //counts are grow as follows,9,90,900,9000.....since the counts maybe overflow so i use long type
            counts *= 10;
        }
        //after loop,the n means nth digits from the current baseNumber

        //the base number is 1，10，100，1000，10000 and so on.
        int baseNumber = (int)Math.pow(10, digitLvl - 1);
        //find the number where nth digit locate
        int number  = (n -1) / digitLvl + baseNumber;
        //find the digit where nth digit locate at the number above
        int mod = (n - 1 ) % digitLvl;
        return String.valueOf(number).charAt(mod) - '0';
    }

    public   int findNthDigit_Leet(int n) {
        int len = 1;
        long count = 9;
        int start = 1;

        while (n > len * count) {
            n -= len * count;
            len += 1;
            count *= 10;
            start *= 10;
        }

        start += (n - 1) / len;
        String s = Integer.toString(start);
        return Character.getNumericValue(s.charAt((n - 1) % len));
    }
}
