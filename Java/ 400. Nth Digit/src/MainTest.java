import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/27/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void findNthDigit_11() {
        assertEquals(0, new Main().findNthDigit(11));
    }

    @org.junit.jupiter.api.Test
    void findNthDigit_10() {
        assertEquals(1, new Main().findNthDigit(10));
    }

    @org.junit.jupiter.api.Test
    void findNthDigit_3() {
        assertEquals(3, new Main().findNthDigit(3));
    }

    @org.junit.jupiter.api.Test
    void findNthDigit_17() {
        assertEquals(3, new Main().findNthDigit(17));
    }

    @org.junit.jupiter.api.Test
    void findNthDigit_17_Leet() {
        assertEquals(3, new Main().findNthDigit_Leet(17));
    }
}