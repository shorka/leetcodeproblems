public class Main {

    public static void main(String[] args) {
        System.out.println("args = [" + Math.pow(91, 0.3) + "]");
    }

    public boolean judgeSquareSum(int c) {

        int cPow = (int) Math.pow(c,0.5);
        for (int i = 0; i <= cPow; i++) {
            for (int j = i ; j <= cPow; j++) {
                if (i * i + j * j == c)
                    return true;
            }
        }
        return false;
    }

    public boolean judgeSquareSum2(int c) {
        for (long a = 0; a * a <= c; a++) {
            double b = Math.sqrt(c - a * a);
            if (b == (int) b)
                return true;
        }
        return false;
    }

    public boolean judgeSquareSum_binary(int c) {
        for (long a = 0; a * a <= c; a++) {
            int b = c - (int)(a * a);
            if (binary_search(0, b, b))
                return true;
        }
        return false;
    }
    public boolean binary_search(long s, long e, int n) {
        if (s > e)
            return false;
        long mid = s + (e - s) / 2;
        if (mid * mid == n)
            return true;
        if (mid * mid > n)
            return binary_search(s, mid - 1, n);
        return binary_search(mid + 1, e, n);
    }
}
