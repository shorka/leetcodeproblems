import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/22/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void judgeSquareSum3() {
        assertEquals(false, new Main().judgeSquareSum(3));
    }

    @org.junit.jupiter.api.Test
    void judgeSquareSum5() {
        assertEquals(true, new Main().judgeSquareSum(5));
    }

    @org.junit.jupiter.api.Test
    void judgeSquareSum4() {
        assertEquals(true, new Main().judgeSquareSum(4));
    }

    @org.junit.jupiter.api.Test
    void judgeSquareSum1() {
        assertEquals(true, new Main().judgeSquareSum(1));
    }

    @org.junit.jupiter.api.Test
    void judgeSquareSum2() {
        assertEquals(true, new Main().judgeSquareSum(2));
    }

    @org.junit.jupiter.api.Test
    void judgeSquareSum5_Sqrt() {
        assertEquals(true, new Main().judgeSquareSum2(5));
    }

    @org.junit.jupiter.api.Test
    void judgeSquareSum5_Binary() {
        assertEquals(true, new Main().judgeSquareSum_binary(5));
    }
}