import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/28/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void lengthOfLongestSubstring_abc() {

        assertEquals(3, new Main().lengthOfLongestSubstring("abcabcbb"));
    }

    @org.junit.jupiter.api.Test
    void lengthOfLongestSubstring_b() {

        assertEquals(1, new Main().lengthOfLongestSubstring("bbbbb"));
    }

    @org.junit.jupiter.api.Test
    void lengthOfLongestSubstring_pwwkew() {

        assertEquals(3, new Main().lengthOfLongestSubstring("pwwkew"));
    }

    @org.junit.jupiter.api.Test
    void lengthOfLongestSubstring_aab() {

        assertEquals(2, new Main().lengthOfLongestSubstring("aab"));
    }

    @org.junit.jupiter.api.Test
    void lengthOfLongestSubstring_dvdf() {

        assertEquals(3, new Main().lengthOfLongestSubstring("dvdf"));
    }

}