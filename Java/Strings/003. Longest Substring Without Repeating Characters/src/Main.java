import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
	// write your code here
    }

    public int lengthOfLongestSubstring_my(String s) {

        String currStr = "";
        int indexStart = 0;
        String maxSubStr = "";
        for (int i = 0; i < s.length(); i++) {

            String iStr = String.valueOf(s.charAt(i));
            if(!currStr.contains(iStr )){
                currStr += iStr;
            }
            else {
                if(maxSubStr.length() < currStr.length())
                    maxSubStr = currStr;

                currStr = "";
                indexStart++;
                i = indexStart-1;
            }
        }

        if(maxSubStr.length() < currStr.length())
            maxSubStr = currStr;


        return maxSubStr.length();
    }

    public int lengthOfLongestSubstring(String s) {

        if (s.length()==0)
            return 0;

        HashMap<Character, Integer> map = new HashMap<>();
        int max=0;
        for (int i=0, j=0; i<s.length(); ++i){

            if (map.containsKey(s.charAt(i))){
                j = Math.max(j,map.get(s.charAt(i))+1);
            }

            map.put(s.charAt(i),i);
            max = Math.max(max,i-j+1);
        }

        return max;
    }
}
