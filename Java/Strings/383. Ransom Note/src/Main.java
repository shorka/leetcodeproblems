public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public boolean canConstruct(String ransomNote, String magazine) {

        int[] freqMag = new int[26];

        for (int i = 0; i < magazine.length(); i++) {

            freqMag[magazine.charAt(i) - 'a']++;
        }

        for (int i = 0; i < ransomNote.length(); i++) {

            int magFreq = freqMag[ransomNote.charAt(i) - 'a'];
            if (magFreq == 0)
                return false;

            else
                freqMag[ransomNote.charAt(i) - 'a']--;
        }


        return true;
    }
}
