import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/15/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void canConstruct_ab() {

        assertEquals(false, new Main().canConstruct("a","b"));
    }

    @org.junit.jupiter.api.Test
    void canConstruct_aa() {

        assertEquals(false, new Main().canConstruct("aa","ab"));
    }

    @org.junit.jupiter.api.Test
    void canConstruct_aab() {

        assertEquals(true, new Main().canConstruct("aa","aab"));
    }


}