import java.util.*;

public class Main {

    public static void main(String[] args) {

    }

    public int firstUniqChar(String s) {

        HashSet<Character> hashSet = new HashSet<>();
        int uniqueIndex = -1;
        List<Character> listUnique = new ArrayList<>();

        for (int i = s.length() - 1; i >= 0; i--) {

            Character iCh = s.charAt(i);
            if (hashSet.contains(iCh)) {

                hashSet.remove(iCh);
                if(listUnique.contains(iCh)){
                    listUnique.remove(iCh);
                }
                continue;
            } else {
                listUnique.add(iCh);
                hashSet.add(iCh);
            }
        }

        if(listUnique.size() == 0)
            return -1;

        for (int i = 0; i<s.length(); i++){
            char iChar = s.charAt(i);
            for (int j = 0; j < listUnique.size(); j++) {
                if(iChar == listUnique.get(j)){
                    return i;
                }
            }
        }

        return -1;
    }

    public int firstUniqChar1(String s) {
        int freq [] = new int[26];
        for(int i = 0; i < s.length(); i ++)
            freq [s.charAt(i) - 'a'] ++;
        for(int i = 0; i < s.length(); i ++)
            if(freq [s.charAt(i) - 'a'] == 1)
                return i;
        return -1;
    }
}
