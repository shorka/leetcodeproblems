import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/15/2018.
 */
class MainTest {

//    @org.junit.jupiter.api.Test
//    void firstUniqChar_2() {
//
//        assertEquals(2, new Main().firstUniqChar("loveleetcode"));
//
//    }
//
//    @org.junit.jupiter.api.Test
//    void firstUniqChar_0() {
//
//        assertEquals(0, new Main().firstUniqChar("leetcode"));
//
//    }
//
//    @org.junit.jupiter.api.Test
//    void firstUniqChar_minus() {
//
//        assertEquals(-1, new Main().firstUniqChar("vvtt"));
//
//    }
//
//    @org.junit.jupiter.api.Test
//    void firstUniqChar_ttvrv() {
//
//        assertEquals(3, new Main().firstUniqChar("ttvrv"));
//
//    }

    @org.junit.jupiter.api.Test
    void firstUniqChar_2_L1() {

        assertEquals(2, new Main().firstUniqChar1("loveleetcode"));

    }

    @org.junit.jupiter.api.Test
    void firstUniqChar_0_L1() {

        assertEquals(0, new Main().firstUniqChar1("leetcode"));

    }

    @org.junit.jupiter.api.Test
    void firstUniqChar_minus_L1() {

        assertEquals(-1, new Main().firstUniqChar1("vvtt"));

    }

    @org.junit.jupiter.api.Test
    void firstUniqChar_ttvrv_L1() {

        assertEquals(3, new Main().firstUniqChar1("ttvrv"));

    }
}