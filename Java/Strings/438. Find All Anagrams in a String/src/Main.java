import java.util.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
    }

    public List<Integer> findAnagrams(String s, String p) {

        List<Integer> list = new  ArrayList<Integer>();

        int lenP = p.length();

        for (int i = 0; i < s.length()-(lenP-1); i++) {
            String subStr = s.substring(i,i+lenP);
            if(isValidAnagram(subStr,p)){
                list.add(i);
            }
        }

        return list;
    }

    private boolean isValidAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        int[] counter = new int[26];
        for (int i = 0; i < s.length(); i++) {

            int numI = (int) s.charAt(i);
            counter[numI - 'a']++;
            counter[t.charAt(i) - 'a']--;
        }
        for (int count : counter) {
            if (count != 0) {
                return false;
            }
        }
        return true;
    }

    public List<Integer> slidingWindowTemplateByHarryChaoyangHe(String s, String t) {
        //init a collection or int value to save the result according the question.
        List<Integer> result = new LinkedList<>();
        if(t.length()> s.length())
            return result;

        //create a hashmap to save the Characters of the target substring.
        //(K, V) = (Character, Frequence of the Characters)
        Map<Character, Integer> mapStillNeed = new HashMap<>();
        for(char c : t.toCharArray()){
            mapStillNeed.put(c, mapStillNeed.getOrDefault(c, 0) + 1);
        }

        //maintain a counter to check whether match the target string.
        //must be the map size, NOT the string size because the char may be duplicate.
        int counter = mapStillNeed.size();

        //Two Pointers: begin - left pointer of the window; end - right pointer of the window
        int begin = 0, end = 0;

        //loop at the begining of the source string
        while(end < s.length()){
            char c = s.charAt(end);//get a character
            if( mapStillNeed.containsKey(c) ){
                mapStillNeed.put(c, mapStillNeed.get(c)-1);// plus or minus one
                if(mapStillNeed.get(c) == 0) counter--;//modify the counter according the requirement(different condition).
            }
            end++;

            //increase begin pointer to make it invalid/valid again
            while(counter == 0){
                char tempc = s.charAt(begin);
                if(mapStillNeed.containsKey(tempc)){
                    mapStillNeed.put(tempc, mapStillNeed.get(tempc) + 1);
                    if(mapStillNeed.get(tempc) > 0){
                        counter++;
                    }
                }
                if(end-begin == t.length()){
                    result.add(begin);
                }
                begin++;
            }

        }
        return result;
    }
}
