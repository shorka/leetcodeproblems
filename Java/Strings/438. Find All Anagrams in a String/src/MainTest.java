import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/18/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void findAnagrams() {
        List<Integer> list = Arrays.asList(0, 1, 2);
        assertEquals(list, new Main().findAnagrams("abab", "ab"));
    }

    @org.junit.jupiter.api.Test
    void findAnagrams2() {
        List<Integer> list = Arrays.asList(0, 1, 2);
        assertEquals(list, new Main().slidingWindowTemplateByHarryChaoyangHe("abab", "ab"));
    }

    @org.junit.jupiter.api.Test
    void findAnagrams_abc() {
        List<Integer> list = Arrays.asList(0, 6);
        assertEquals(list, new Main().slidingWindowTemplateByHarryChaoyangHe("cbaebabacd", "abc"));
    }

}