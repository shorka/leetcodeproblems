import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/28/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void reverseVowels_Hello() {
        assertEquals("holle", new Main().reverseVowels("hello"));
    }

    @org.junit.jupiter.api.Test
    void reverseVowels_Leetcode() {
        assertEquals("leotcede", new Main().reverseVowels("leetcode"));
    }

    @org.junit.jupiter.api.Test
    void reverseVowels_empty() {
        assertEquals("", new Main().reverseVowels(""));
    }
}