public class Main {

    public static void main(String[] args) {
        // write your code here
    }


    public String reverseVowels(String s) {

        StringBuilder res = new StringBuilder(s);

        int i = 0;
        int j = s.length() - 1;

        while (i < j) {
            if (!isVowel(s.charAt(i))) {
                i++;
                continue;
            }

            char chJ = s.charAt(j);
            while (!isVowel(chJ)) {
                j--;
                chJ = s.charAt(j);
            }

            if(i>j)
                break;

            char chI = s.charAt(i);
            res.setCharAt(i, chJ);
            res.setCharAt(j, chI);

            i++;
            j--;
        }

        return res.toString();
    }

    public String reverseVowels_Leetcode(String s) {
        if(s == null || s.length()==0) return s;
        String vowels = "aeiouAEIOU";
        char[] chars = s.toCharArray();
        int start = 0;
        int end = s.length()-1;
        while(start<end){

            while(start<end && !vowels.contains(chars[start]+"")){
                start++;
            }

            while(start<end && !vowels.contains(chars[end]+"")){
                end--;
            }

            char temp = chars[start];
            chars[start] = chars[end];
            chars[end] = temp;

            start++;
            end--;
        }
        return new String(chars);
    }

    private boolean isVowel(char c) {
        c = Character.toLowerCase(c);
        return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u');
    }
}
