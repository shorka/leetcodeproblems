public class Main {

    public static void main(String[] args) {

    }

    public boolean validPalindrome(String s) {
        char[] sc = s.toCharArray();
        int i = 0, j = s.length() - 1;
        while(i < j){
            if(sc[i] != sc[j]){
                return helper(sc, i + 1, j) || helper(sc, i, j - 1);
            }
            i++;
            j--;
        }
        return true;
    }

    boolean helper(char[] sc, int i, int j){
        while(i < j){
            if(sc[i] != sc[j])
                return false;
            i++; j--;
        }
        return true;
    }
}
