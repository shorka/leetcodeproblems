import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/27/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void validPalindrome_aba() {
        assertEquals(true, new Main().validPalindrome("aba"));
    }

    @org.junit.jupiter.api.Test
    void validPalindrome_abca() {
        assertEquals(true, new Main().validPalindrome("abca"));
    }

    @org.junit.jupiter.api.Test
    void validPalindrome_klro() {
        assertEquals(true, new Main().validPalindrome("klrorlck"));
    }

}