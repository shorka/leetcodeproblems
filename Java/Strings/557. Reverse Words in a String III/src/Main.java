public class Main {

    public static void main(String[] args) {
	// write your code here
    }

    public String reverseWords(String s) {

        String ans = "";
        String word = "";
        int len = s.length();
        for (int i = 0; i < len; i++) {

            Character iChar = s.charAt(i);
            if (iChar != ' ') {
                word = iChar.toString() + word;
            }
            else {
                ans += word + " ";
                word = "";
            }

            if(i == len-1){
                ans += word;
            }

        }
        return ans;
    }


}
