import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/12/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void reverseWords() {

        assertEquals("s'teL ekat edoCteeL tsetnoc", new Main().reverseWords("Let's take LeetCode contest"));

    }

}