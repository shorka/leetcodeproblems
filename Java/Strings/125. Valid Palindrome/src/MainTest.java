import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/12/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void isPalindromePanama() {
        assertEquals(true, new Main().isPalindrome("A man, a plan, a canal: Panama"));
    }
    @org.junit.jupiter.api.Test
    void isPalindromePanama3() {
        assertEquals(true, new Main().isPalindromeLine3("A man, a plan, a canal: Panama"));
    }

    @org.junit.jupiter.api.Test
    void isPalindromeRace() {
        assertEquals(false, new Main().isPalindrome("race a car"));
    }


    @org.junit.jupiter.api.Test
    void isPalindromeRace3() {
        assertEquals(false, new Main().isPalindromeLine3("race a car"));
    }
}