public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public boolean isPalindrome(String s) {

        int i = 0, j = s.length() - 1;
        while (i < j) {
            char chI = s.charAt(i);
            char chJ = s.charAt(j);

            boolean isAlphaI = Character.isLetterOrDigit(chI);
            boolean isAlphaJ = Character.isLetterOrDigit(chJ);
            if (!isAlphaI)
                i++;

            if (!isAlphaJ)
                j--;

            if (!(isAlphaJ && isAlphaI))
                continue;

            if (Character.toLowerCase(chI) != Character.toLowerCase(chJ))
                return false;

            i++;
            j--;
        }

        return true;
    }

    public boolean isPalindromeLine3(String s) {
        String actual = s.replaceAll("[^A-Za-z0-9]", "").toLowerCase();
        String rev = new StringBuffer(actual).reverse().toString();
        return actual.equals(rev);
    }
}
