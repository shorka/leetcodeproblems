import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/16/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void backspaceCompare_ac() {
        assertEquals(true, new Main().backspaceCompare("ab#c","ad#c"));
    }

    @org.junit.jupiter.api.Test
    void backspaceCompare_empty1() {
        assertEquals(true, new Main().backspaceCompare("ab##","c#d#"));
    }

    @org.junit.jupiter.api.Test
    void backspaceCompare_empty0() {
        assertEquals(true, new Main().backspaceCompare("",""));
    }

    @org.junit.jupiter.api.Test
    void backspaceCompare_c() {
        assertEquals(true, new Main().backspaceCompare("a##c","#a#c"));
    }

    @org.junit.jupiter.api.Test
    void backspaceCompare_b_false() {
        assertEquals(false, new Main().backspaceCompare("a#c","b"));
    }

    @org.junit.jupiter.api.Test
    void backspaceCompare_bxj() {
        assertEquals(true, new Main().backspaceCompare("bxj##tw","bxo#j##tw"));
    }

    @org.junit.jupiter.api.Test
    void backspaceCompare_bxj_L1() {
        assertEquals(true, new Main().backspaceCompare_L1("bxj##tw","bxo#j##tw"));
    }

    @org.junit.jupiter.api.Test
    void backspaceCompare_bxj_L2() {
        assertEquals(true, new Main().backspaceCompare_L2("bxj##tw","bxo#j##tw"));
    }
}