import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public boolean backspaceCompare(String S, String T) {


        return formatString(S).equals(formatString(T));
    }

    private String formatString(String str) {

        String res = "";
        int countBackSpaces = 0;
        for (int i = str.length() - 1; i >= 0; i--) {
            char iChar = str.charAt(i);
            if (iChar == '#' && i > 0) {

                if (str.charAt(i - 1) != '#') {
                    i = i - countBackSpaces - 1;
                    countBackSpaces = 0;
                } else if (str.charAt(i - 1) == '#')
                    countBackSpaces++;

                continue;
            }

            if (iChar != '#')
                res = String.valueOf(iChar) + res;

        }

        return res;
    }

    public boolean backspaceCompare_L1(String S, String T) {
        return build_BuildStrin(S).equals(build_BuildStrin(T));
    }

    //Solution from Leetcode
    private String build_BuildStrin(String S) {
        Stack<Character> ans = new Stack();
        for (char c : S.toCharArray()) {
            if (c != '#')
                ans.push(c);
            else if (!ans.empty())
                ans.pop();
        }
        return String.valueOf(ans);
    }

    public boolean backspaceCompare_L2(String S, String T) {
        int i = S.length() - 1, j = T.length() - 1;
        int skipS = 0, skipT = 0;

        while (i >= 0 || j >= 0) { // While there may be chars in build(S) or build (T)
            while (i >= 0) { // Find position of next possible char in build(S)
                if (S.charAt(i) == '#') {
                    skipS++;
                    i--;
                } else if (skipS > 0) {
                    skipS--;
                    i--;
                } else
                    break;
            }
            while (j >= 0) { // Find position of next possible char in build(T)
                if (T.charAt(j) == '#') {
                    skipT++;
                    j--;
                } else if (skipT > 0) {
                    skipT--;
                    j--;
                } else break;
            }
            // If two actual characters are different
            if (i >= 0 && j >= 0 && S.charAt(i) != T.charAt(j))
                return false;
            // If expecting to compare char vs nothing
            if ((i >= 0) != (j >= 0))
                return false;
            i--;
            j--;
        }
        return true;
    }
}
