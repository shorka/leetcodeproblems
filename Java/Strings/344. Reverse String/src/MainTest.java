import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/15/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void reverseString_hello() {
        assertEquals("olleh", new Main().reverseStringOpt("hello"));
    }

    @org.junit.jupiter.api.Test
    void reverseString_empty() {
        assertEquals("", new Main().reverseStringOpt(""));
    }

    @org.junit.jupiter.api.Test
    void reverseString_snow() {
        assertEquals("wons", new Main().reverseStringOpt("snow"));
    }

    @org.junit.jupiter.api.Test
    void reverseString2_hello() {
        assertEquals("olleh", new Main().reverseString2("hello"));
    }
}