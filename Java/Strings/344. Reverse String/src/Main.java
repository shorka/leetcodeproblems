public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public String reverseString(String s) {
        String reversed = "";
        for (int i = 0; i < s.length(); i++)
            reversed = s.charAt(i) + reversed;

        return reversed;
    }

    public String reverseStringOpt(String s) {
        if(s == null || s.isEmpty())
            return s;

        return reverseStringOpt(s, 0, s.length() - 1);
    }

    public String reverseStringOpt(String s, int low, int high) {
        if (low < high) {
            int middle = low + (high - low) / 2;
            String left = reverseStringOpt(s, low, middle);
            String right = reverseStringOpt(s, middle + 1, high);

            return right + left;
        }

        return String.valueOf(s.charAt(low));
    }

    public String reverseString2(String s) {
        char[] word = s.toCharArray();
        int i = 0;
        int j = s.length() - 1;
        while (i < j) {
            char temp = word[i];
            word[i] = word[j];
            word[j] = temp;
            i++;
            j--;
        }
        return new String(word);
    }
}
