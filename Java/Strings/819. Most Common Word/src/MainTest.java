import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/14/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void mostCommonWord() {

        assertEquals("ball", new Main().mostCommonWord("Bob hit a ball, the hit BALL flew far after it was hit.",
                new String[]{"hit"}));

    }

}