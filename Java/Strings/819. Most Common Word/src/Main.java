import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public String mostCommonWord(String paragraph, String[] banned) {

        String[] words = paragraph.trim().split(" ");

        HashMap<String, Integer> map = new HashMap<String, Integer>();

        for (int i = 0; i < words.length; i++) {

            String iWord = words[i].toLowerCase();
            char lastCh= iWord.charAt(iWord.length()-1);
            if(!Character.isLetter(lastCh)){
                iWord = iWord.substring(0, iWord.length()-1);
            }

            if (doArrayHasElement(banned, iWord))
                continue;

            map.put(iWord, map.containsKey(iWord) ? map.get(iWord) + 1 : 1);
        }

        int max = Integer.MIN_VALUE;
        String maxWord = "";

        for (String key : map.keySet()) {
            if (map.get(key) > max) {
                maxWord = key;
                max = map.get(key);
            }
        }

        return maxWord;
    }

    private boolean doArrayHasElement(String[] arr, String str) {

        for (int i = 0; i < arr.length; i++) {
            if (arr[i].toLowerCase().equals(str))
                return true;
        }
        return false;
    }
}
