import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/12/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void countSubstrings3() {
        assertEquals(3, new Main().countSubstrings("abc"));
    }

    @org.junit.jupiter.api.Test
    void countSubstrings6() {
        assertEquals(6, new Main().countSubstrings("aaa"));
    }

    @org.junit.jupiter.api.Test
    void countSubstrings13() {
        assertEquals(6, new Main().countSubstrings("abba"));
    }

    @org.junit.jupiter.api.Test
    void countSubstrings13_M() {
        assertEquals(6, new Main().countSubstrings_Manachers("abba"));
    }

    @org.junit.jupiter.api.Test
    void countSubstrings13_Brute() {
        assertEquals(6, new Main().countSubstrings_Brute("abba"));
    }
}