public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public String longestPalindrome(String s) {

        if(s.isEmpty())
            return "";

        int N = s.length();
        String maxPolind = "";

        for (int center = 0; center <= 2 * N - 1; ++center) {
            int left = center / 2;
            int right = left + center % 2;
            while (left >= 0 && right < N && s.charAt(left) == s.charAt(right)) {
                if (maxPolind.length() < (right - left)+1)
                    maxPolind = s.substring(left, right + 1);

                left--;
                right++;
            }
        }
        return  maxPolind;
    }


    public boolean isPalindrome(String s) {

        if (s.isEmpty()) {
            return true;
        }
        int head = 0, tail = s.length() - 1;
        char cHead, cTail;
        while (head <= tail) {
            cHead = s.charAt(head);
            cTail = s.charAt(tail);
            if (!Character.isLetterOrDigit(cHead)) {
                head++;
            } else if (!Character.isLetterOrDigit(cTail)) {
                tail--;
            } else {
                if (Character.toLowerCase(cHead) != Character.toLowerCase(cTail)) {
                    return false;
                }
                head++;
                tail--;
            }
        }
        return true;
    }
}
