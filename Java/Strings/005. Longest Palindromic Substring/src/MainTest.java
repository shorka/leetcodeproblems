import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/27/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void longestPalindrome_babad() {
        assertEquals("bab", new Main().longestPalindrome("babad"));
    }
    @org.junit.jupiter.api.Test
    void longestPalindrome_cbbd() {
        assertEquals("bb", new Main().longestPalindrome("cbbd"));
    }

    @org.junit.jupiter.api.Test
    void longestPalindrome_a() {
        assertEquals("a", new Main().longestPalindrome("a"));
    }
}