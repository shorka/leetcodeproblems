public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public char nextGreatestLetter(char[] letters, char target) {

        char smallest = '~';
        for (int i = 0; i < letters.length; i++) {

            if (letters[i] <= target)
                continue;

            if (smallest > letters[i])
                smallest = letters[i];
        }

        if(smallest == '~')
            return letters[0];

        return smallest;
    }

    public char nextGreatestLetter_Search(char[] letters, char target) {
        int lo = 0, hi = letters.length;
        while (lo < hi) {
            int mi = lo + (hi - lo) / 2;
            if (letters[mi] <= target)
                lo = mi + 1;

            else
                hi = mi;
        }
        return letters[lo % letters.length];
    }
}
