import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/26/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void nextGreatestLetter_a() {
        char[] arr = new char[]{'c', 'f', 'j'};
        assertEquals('c', new Main().nextGreatestLetter(arr, 'a'));
    }

    @org.junit.jupiter.api.Test
    void nextGreatestLetter_a_BS() {
        char[] arr = new char[]{'c', 'f', 'j'};
        assertEquals('c', new Main().nextGreatestLetter_Search(arr, 'a'));
    }

    @org.junit.jupiter.api.Test
    void nextGreatestLetter_c() {
        char[] arr = new char[]{'c', 'f', 'j'};
        assertEquals('f', new Main().nextGreatestLetter(arr, 'c'));
    }

    @org.junit.jupiter.api.Test
    void nextGreatestLetter_c_BS() {
        char[] arr = new char[]{'c', 'f', 'j'};
        assertEquals('f', new Main().nextGreatestLetter_Search(arr, 'c'));
    }

    @org.junit.jupiter.api.Test
    void nextGreatestLetter_j() {
        char[] arr = new char[]{'c', 'f', 'j'};
        assertEquals('c', new Main().nextGreatestLetter_Search(arr, 'j'));
    }

    @org.junit.jupiter.api.Test
    void nextGreatestLetter_j_BS() {
        char[] arr = new char[]{'a','b',  'c', 'f', 'j','k','l','m'};
        assertEquals('l', new Main().nextGreatestLetter_Search(arr, 'k'));
    }
}