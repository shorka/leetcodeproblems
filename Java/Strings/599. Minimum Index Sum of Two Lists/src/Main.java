import java.util.*;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public String[] findRestaurant_my(String[] list1, String[] list2) {

        HashMap<Integer, ArrayList<String>> commonRest = new HashMap<>();

        int minSum = Integer.MAX_VALUE;
        for (int i = 0; i < list1.length; i++) {

            String iStr = list1[i];
            for (int j = 0; j < list2.length; j++) {

                if (!iStr.equals(list2[j]))
                    continue;

                int indexSum = i + j;

                if (minSum >= indexSum) {

                    if (!commonRest.containsKey(indexSum)) {
                        commonRest.put(indexSum,
                                new ArrayList<>(Arrays.asList(iStr)));
                    } else {

                        ArrayList<String> list = commonRest.get(indexSum);
                        list.add(iStr);
                        commonRest.put(indexSum, list);
                    }

                    minSum = indexSum;
                }
            }
        }

        int minKey = Integer.MAX_VALUE;
        for (Map.Entry<Integer, ArrayList<String>> entry : commonRest.entrySet()) {
            Integer key = entry.getKey();
            ArrayList<String> value = entry.getValue();
            minKey = Math.min(minKey, key);
        }

        Object[] objectList = commonRest.get(minKey).toArray();
        return Arrays.copyOf(objectList, objectList.length, String[].class);
    }

    public String[] findRestaurant(String[] list1, String[] list2) {
        Map<String, Integer> map = new HashMap<>();
        List<String> res = new LinkedList<>();
        int minSum = Integer.MAX_VALUE;
        for (int i = 0; i < list1.length; i++)
            map.put(list1[i], i);

        for (int i = 0; i < list2.length; i++) {
            Integer j = map.get(list2[i]);
            if (j != null && i + j <= minSum) {
                if (i + j < minSum) {
                    res.clear();
                    minSum = i + j;
                }
                res.add(list2[i]);
            }
        }
        return res.toArray(new String[res.size()]);
    }
}
