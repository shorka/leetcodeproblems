import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/28/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void findRestaurant_4() {

        String[] res = new String[]{"KFC", "Burger King", "Tapioca Express", "Shogun"};

        String[] list1 = new String[]{"Shogun", "Tapioca Express", "Burger King", "KFC"};
        String[] list2 = new String[]{"KFC", "Burger King", "Tapioca Express", "Shogun"};

        assertArrayEquals (res, new Main().findRestaurant(list1, list2));

    }

}