import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        System.out.println(new Main().customSortString("cba", "abcd"));
    }

    public String customSortString(String S, String T) {
        char[] tChars = T.toCharArray();
        char[] tChars2 = new char[tChars.length];
        System.arraycopy( tChars, 0, tChars2, 0, tChars.length );
        char[] sChars = S.toCharArray();

        HashSet<Character> seen = new HashSet<Character>();
        for (int i = 0; i < tChars.length; i++) {

            for (int j = 0; j < sChars.length; j++) {
                if (tChars2[i] == sChars[j] && i != j) {
                    char tmp = tChars[j];
                    tChars[j] = tChars[i];
                    tChars[i] = tmp;
                }
            }
//
//            int j = 0;
//            while (true) {
//                if (seen.contains(tChars[j]))
//            }
        }

        return String.valueOf(tChars2);
    }

    public String customSortString2(String S, String T) {
        // count[char] = the number of occurrences of 'char' in T.
        // This is offset so that count[0] = occurrences of 'a', etc.
        // 'count' represents the current state of characters
        // (with multiplicity) we need to write to our answer.
        int aNumb = 'a';
        int[] count = new int[26];
        for (char c: T.toCharArray())
            count[c - 'a']++;

        // ans will be our final answer.  We use StringBuilder to join
        // the answer so that we more efficiently calculate a
        // concatenation of strings.
        StringBuilder ans = new StringBuilder();

        // Write all characters that occur in S, in the order of S.
        for (char c: S.toCharArray()) {
            for (int i = 0; i < count[c - 'a']; ++i)
                ans.append(c);
            // Setting count[char] to zero to denote that we do
            // not need to write 'char' into our answer anymore.
            count[c - 'a'] = 0;
        }

        // Write all remaining characters that don't occur in S.
        // That information is specified by 'count'.
        for (char c = 'a'; c <= 'z'; ++c)
            for (int i = 0; i < count[c - 'a']; ++i)
                ans.append(c);

        return ans.toString();
    }
}
