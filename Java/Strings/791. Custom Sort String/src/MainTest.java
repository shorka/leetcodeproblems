import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/24/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void customSortString_cba() {
        assertEquals("cbad", new Main().customSortString2("cba", "abcd"));
    }

    @org.junit.jupiter.api.Test
    void customSortString_kqeep() {
        assertEquals("kqeep", new Main().customSortString2("kqep", "pekeq"));
    }

}