import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

    }

    public boolean isAnagram_Brute(String s, String t) {

        if (s.length() != t.length())
            return false;

        for (int i = 0; i < s.length(); i++) {
            char iChar = s.charAt(i);
            boolean findWord = false;
            for (int j = 0; j < t.length(); j++) {

                if (iChar == t.charAt(j)) {
                    findWord = true;
                    t = t.substring(0,j) + "$"+t.substring(j+1,t.length());
                    break;
                }
            }
            if(!findWord)
                return false;
        }

        return true;
    }

    public boolean isAnagram_Sort(String s, String t) {

        if (s.length() != t.length())
            return false;

        char[] chS = s.toCharArray();
        Arrays.sort(chS);

        char[] chT = t.toCharArray();
        Arrays.sort(chT);

        return Arrays.equals(chS, chT);
    }

    public boolean isAnagram2(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        int[] counter = new int[26];
        for (int i = 0; i < s.length(); i++) {

            int numI = (int) s.charAt(i);
            counter[numI - 'a']++;
            counter[t.charAt(i) - 'a']--;
        }
        for (int count : counter) {
            if (count != 0) {
                return false;
            }
        }
        return true;
    }
}
