import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/17/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void isAnagram() {
        assertEquals(true, new Main().isAnagram_Sort("anagram", "nagaram"));
    }

    @org.junit.jupiter.api.Test
    void isAnagram2() {
        assertEquals(true, new Main().isAnagram2("anagram", "nagaram"));
    }

    @org.junit.jupiter.api.Test
    void isAnagram3() {
        assertEquals(false, new Main().isAnagram2("car", "tar"));
    }
}