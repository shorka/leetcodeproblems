import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    int maxDepth;
    public int maxDepth(TreeNode root) {

        maxDepth = 0;
        Traverse(root,1);
        return  maxDepth;
    }

    void Traverse(TreeNode node, int lvl){
        if (node == null)
            return;

        Traverse(node.left,lvl+1);
        Traverse(node.right,lvl+1);

        if(lvl> maxDepth)
            maxDepth = lvl;
    }
}
