import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/16/2018.
 */
class MainTest {

1

    public TreeNode GetT2() {
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode0 = new TreeNode(0);
        TreeNode treeNode4 = new TreeNode(4);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode1 = new TreeNode(1);

        treeNode3.left = treeNode0;
        treeNode3.right = treeNode4;

        treeNode0.right = treeNode2;

        treeNode2.left = treeNode1;
        return treeNode3;
    }

    public TreeNode GetT3() {
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode9 = new TreeNode(9);
        TreeNode treeNode20 = new TreeNode(20);
        TreeNode treeNode7 = new TreeNode(7);
        TreeNode treeNode15 = new TreeNode(15);

        treeNode3.left = treeNode9;
        treeNode3.right = treeNode20;

        treeNode20.right = treeNode7;
        treeNode20.left = treeNode15;

        return treeNode3;
    }

    @org.junit.jupiter.api.Test
    void maxDepth_3() {
        BTreePrinter.printNode(GetT1());
        assertEquals(3, new Main().maxDepth(GetT1()));
    }

    @org.junit.jupiter.api.Test
    void maxDepth_2() {
        BTreePrinter.printNode(GetT2());
        assertEquals(4, new Main().maxDepth(GetT2()));
    }

    @org.junit.jupiter.api.Test
    void maxDepth__3_2() {
        BTreePrinter.printNode(GetT3());
        assertEquals(3, new Main().maxDepth(GetT3()));
    }
}