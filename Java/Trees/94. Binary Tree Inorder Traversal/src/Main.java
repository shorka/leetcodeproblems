import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    List<Integer> list;
    public List<Integer> inorderTraversal(TreeNode root) {
        list = new ArrayList<>();
        traverse(root);
        return list;
    }

    public void traverse(TreeNode root){
        if(root == null)
            return;

        if(root.left != null)
            traverse(root.left);

        list.add(root.val);

        if(root.right != null)
            traverse(root.right);
    }
}
