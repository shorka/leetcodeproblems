import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/15/2018.
 */
class MainTest {

    public TreeNode GetT1() {
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode5 = new TreeNode(5);
        TreeNode treeNode3 = new TreeNode(3);

        treeNode1.right = treeNode5;
        treeNode5.left = treeNode3;

        return treeNode1;
    }

    public TreeNode GetT2() {
        TreeNode treeNode236 = new TreeNode(236);
        TreeNode treeNode104 = new TreeNode(104);
        TreeNode treeNode701 = new TreeNode(701);
        TreeNode treeNode227 = new TreeNode(227);
        TreeNode treeNode911 = new TreeNode(911);

        treeNode236.left = treeNode104;
        treeNode236.right = treeNode701;

        treeNode701.right = treeNode911;

        treeNode104.right = treeNode227;


        return treeNode236;
    }

    @org.junit.jupiter.api.Test
    void getMinimumDifference() {

        BTreePrinter.printNode(GetT1());

        assertEquals(2, new Main().getMinimumDifference(GetT1()));

    }

    @org.junit.jupiter.api.Test
    void getMinimumDifference2() {

        BTreePrinter.printNode(GetT2());

        assertEquals(123, new Main().getMinimumDifference(GetT2()));

    }
}