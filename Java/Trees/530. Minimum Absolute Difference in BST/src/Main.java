public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    int minDiff;
    Integer prev = null;
    public int getMinimumDifference(TreeNode root) {

        minDiff = Integer.MAX_VALUE;
        traverse(root);
        return minDiff;
    }

    private void traverse(TreeNode node) {
        if (node == null)
            return;

        traverse(node.left);
        if(prev != null){
            minDiff = Math.min(minDiff, node.val - prev);
        }

        System.out.println("node = " + node.val);
        prev = node.val;

        traverse(node.right);
    }
}
