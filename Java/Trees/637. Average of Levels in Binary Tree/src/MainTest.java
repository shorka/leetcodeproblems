import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/15/2018.
 */
class MainTest {

    public TreeNode GetT1() {
        TreeNode treeNode6 = new TreeNode(6);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode8 = new TreeNode(8);
        TreeNode treeNode0 = new TreeNode(0);
        TreeNode treeNode4 = new TreeNode(4);
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode5 = new TreeNode(5);
        TreeNode treeNode7 = new TreeNode(7);
        TreeNode treeNode9 = new TreeNode(9);

        treeNode6.left = treeNode2;
        treeNode6.right = treeNode8;

        treeNode2.left = treeNode0;
        treeNode2.right = treeNode4;

        treeNode8.left = treeNode7;
        treeNode8.right = treeNode9;

        treeNode4.left = treeNode3;
        treeNode4.right = treeNode5;

        return treeNode6;
    }

    @org.junit.jupiter.api.Test
    void averageOfLevels() {
//        BTreePrinter.printNode(GetT1());
        List<Double> list = new ArrayList<Double>();
        list.add(6.0);
        list.add(9.0);
        list.add(6.6);
        list.add(4.0);

        assertEquals(list, new Main().averageOfLevels(GetT1()));
    }

    @org.junit.jupiter.api.Test
    void averageOfLevels2() {
//        BTreePrinter.printNode(GetT1());
        List<Double> list = new ArrayList<Double>();
        list.add(6.0);
        list.add(9.0);
        list.add(6.6);
        list.add(4.0);

        assertEquals(list, new Main().averageOfLevelsDFS(GetT1()));
    }

    @org.junit.jupiter.api.Test
    void averageOfLevels3() {
        BTreePrinter.printNode(GetT1());
        List<Double> list = new ArrayList<Double>();
        list.add(6.0);
        list.add(9.0);
        list.add(6.6);
        list.add(4.0);

        assertEquals(list, new Main().averageOfLevels_L2(GetT1()));
    }
}