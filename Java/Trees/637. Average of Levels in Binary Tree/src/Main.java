import java.util.*;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }


    Map<Integer, ArrayList<Double>> dicLvlAvg;

    public List<Double> averageOfLevels(TreeNode root) {
        dicLvlAvg =  new HashMap<Integer, ArrayList<Double>>();

        List<Double> list = new ArrayList<Double>();
        traverse(root, 0);
        for (Map.Entry<Integer,  ArrayList<Double>> entry : dicLvlAvg.entrySet()) {

            int qty = 0;
            double sum = 0.0;
            for (Double val: entry.getValue()){
                qty++;
                sum += val;
            }

            list.add(qty == 0 ? 0 : sum/qty);
        }


        return list;
    }

    void traverse(TreeNode node, int lvl) {
        if (node == null)
            return;

        ArrayList<Double> listVal = null;
        if (dicLvlAvg.containsKey(lvl)) 
            listVal = dicLvlAvg.get(lvl);        
        else
            listVal = new ArrayList<Double>();

        listVal.add((double) node.val);


        dicLvlAvg.put(lvl, listVal);
        traverse(node.left, lvl + 1);
        traverse(node.right, lvl + 1);
    }

    public List < Double > averageOfLevelsDFS(TreeNode root) {
        List < Integer > count = new ArrayList < > ();
        List < Double > res = new ArrayList < > ();
        average(root, 0, res, count);
        for (int i = 0; i < res.size(); i++)
            res.set(i, res.get(i) / count.get(i));
        return res;
    }
    void average(TreeNode t, int i, List < Double > sum, List < Integer > count) {
        if (t == null)
            return;
        if (i < sum.size()) {
            sum.set(i, sum.get(i) + t.val);
            count.set(i, count.get(i) + 1);
        } else {
            sum.add(1.0 * t.val);
            count.add(1);
        }
        average(t.left, i + 1, sum, count);
        average(t.right, i + 1, sum, count);
    }


    public List < Double > averageOfLevels_L2(TreeNode root) {
        List < Double > res = new ArrayList < > ();
        Queue < TreeNode > queue = new LinkedList < > ();
        queue.add(root);
        while (!queue.isEmpty()) {
            long sum = 0, count = 0;
            Queue < TreeNode > temp = new LinkedList < > ();
            while (!queue.isEmpty()) {
                TreeNode n = queue.remove();
                sum += n.val;
                count++;
                if (n.left != null)
                    temp.add(n.left);
                if (n.right != null)
                    temp.add(n.right);
            }
            queue = temp;
            res.add(sum * 1.0 / count);
        }
        return res;
    }
}
