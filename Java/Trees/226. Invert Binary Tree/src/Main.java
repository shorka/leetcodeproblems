import java.util.LinkedList;
import java.util.Queue;

public class Main {

    public TreeNode GetT1() {
        TreeNode treeNode4 = new TreeNode(4);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode7 = new TreeNode(7);
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode6 = new TreeNode(6);
        TreeNode treeNode9 = new TreeNode(9);

        treeNode4.left = treeNode2;
        treeNode4.right = treeNode7;

        treeNode7.left = treeNode6;
        treeNode7.right = treeNode9;

        treeNode2.left = treeNode1;
        treeNode2.right = treeNode3;

        return treeNode4;
    }

    public static void main(String[] args) {
        Main m = new Main();
        BTreePrinter.printNode(m.GetT1());
        System.out.println("Inverted");
        BTreePrinter.printNode(m.invertTreeIter(m.GetT1()));
    }

    public TreeNode invertTree(TreeNode root) {

        if(root == null)
            return  null;

        TreeNode right = invertTree(root.right);
        TreeNode left = invertTree(root.left);

        root.left = right;
        root.right = left;

        BTreePrinter.printNode(root);

        return root;
    }

    public  TreeNode invertTreeIter(TreeNode root){
        System.out.println("invertTreeIter");
        if(root == null)
            return  null;

        Queue<TreeNode> queue = new LinkedList<>();

        queue.add(root);
//        BTreePrinter.printNode(root);

        while (!queue.isEmpty()){
            TreeNode curr = queue.poll();
            if (curr == null)
                continue;

            TreeNode tmp = curr.left;
            curr.left = curr.right;
            curr.right = tmp;

            if (curr.left != null)
                queue.add(curr.left);

            if (curr.right != null)
                queue.add(curr.right);
        }

        return root;

    }

}
