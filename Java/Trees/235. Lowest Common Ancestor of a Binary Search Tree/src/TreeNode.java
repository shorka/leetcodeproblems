import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Kirill on 2/6/2018.
 */
  public class TreeNode {
       int val;
       TreeNode left;
       TreeNode right;

       TreeNode(int x) { val = x; }
 }

