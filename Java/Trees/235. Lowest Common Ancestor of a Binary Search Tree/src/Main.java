import java.util.LinkedList;
import java.util.Queue;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public TreeNode lowestCommonAncestor_Brutte(TreeNode root, TreeNode p, TreeNode q) {
        if(root == null)
            return  null;

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()){
            TreeNode curr = queue.poll();
            if (curr == null)
                continue;

            boolean isEq = false;

            if(curr.left != null){
                if(curr.left.val == q.val || curr.left.val == p.val)
                    isEq = true;

            }
            if(curr.right != null){
                if(curr.right.val == q.val || curr.right.val == p.val)
                    isEq = true;
            }

            if(isEq)
                return  curr;


            if (curr.left != null)
                queue.add(curr.left);

            if (curr.right != null)
                queue.add(curr.right);
        }

        return  root;
    }

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if(root.val > p.val && root.val > q.val){
            return lowestCommonAncestor(root.left, p, q);

        }else if(root.val < p.val && root.val < q.val){
            return lowestCommonAncestor(root.right, p, q);

        }else{
            return root;
        }
    }

    public TreeNode lowestCommonAncestor3(TreeNode root, TreeNode p, TreeNode q) {
        while ((root.val - p.val) * (root.val - q.val) > 0)
            root = p.val < root.val ? root.left : root.right;
        return root;
    }
}
