import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Kirill on 2/12/2018.
 */
public class TreeNodeHelper {

    public  static  TreeNode FindNodeByValue(TreeNode root, int value){
        if(root == null)
            return  null;

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()){
            TreeNode curr = queue.poll();
            if (curr == null)
                continue;

            if(curr.val == value)
                return  curr;

            if (curr.left != null)
                queue.add(curr.left);

            if (curr.right != null)
                queue.add(curr.right);
        }

        return root;
    }
}
