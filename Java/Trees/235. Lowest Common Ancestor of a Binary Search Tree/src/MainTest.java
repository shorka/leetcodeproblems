import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/12/2018.
 */
class MainTest {

    public TreeNode GetT1() {
        TreeNode treeNode6 = new TreeNode(6);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode8 = new TreeNode(8);
        TreeNode treeNode0 = new TreeNode(0);
        TreeNode treeNode4 = new TreeNode(4);
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode5 = new TreeNode(5);
        TreeNode treeNode7 = new TreeNode(7);
        TreeNode treeNode9 = new TreeNode(9);

        treeNode6.left = treeNode2;
        treeNode6.right = treeNode8;

        treeNode2.left = treeNode0;
        treeNode2.right = treeNode4;

        treeNode8.left = treeNode7;
        treeNode8.right = treeNode9;

        treeNode4.left = treeNode3;
        treeNode4.right = treeNode5;

        return treeNode6;
    }

    @org.junit.jupiter.api.Test
    void lowestCommonAncestor_6() {
//        BTreePrinter.printNode(GetT1());

        TreeNode res = new Main().lowestCommonAncestor(GetT1(),
                TreeNodeHelper.FindNodeByValue(GetT1(),2),
                TreeNodeHelper.FindNodeByValue(GetT1(),8) );

        assertEquals(GetT1(), res    );
    }

    @org.junit.jupiter.api.Test
    void lowestCommonAncestor_2() {
//        BTreePrinter.printNode(GetT1());

        TreeNode res = new Main().lowestCommonAncestor(GetT1(),
                TreeNodeHelper.FindNodeByValue(GetT1(),2),
                TreeNodeHelper.FindNodeByValue(GetT1(),4) );

        assertEquals(GetT1(), res    );
    }

    @org.junit.jupiter.api.Test
    void lowestCommonAncestor_3() {
//        BTreePrinter.printNode(GetT1());

        TreeNode res = new Main().lowestCommonAncestor3(GetT1(),
                TreeNodeHelper.FindNodeByValue(GetT1(),2),
                TreeNodeHelper.FindNodeByValue(GetT1(),4) );

        assertEquals(GetT1(), res    );
    }


}