import java.util.LinkedList;
import java.util.Queue;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    private int minDepth = Integer.MAX_VALUE;

    int minDepth(TreeNode root) {
        if (root == null) return 0;

        traverse(root, 1);
        return minDepth;
    }

    private void traverse(TreeNode node, int lvl) {

        if (node == null) return;

        traverse(node.left, lvl + 1);
        traverse(node.right, lvl + 1);

        final boolean isLeaf = node.left == null && node.right == null;
        if (isLeaf && lvl < minDepth) {
            minDepth = lvl;
        }
    }
}
