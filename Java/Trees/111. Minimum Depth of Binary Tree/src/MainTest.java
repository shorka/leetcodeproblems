import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 12/19/2018.
 */
class MainTest {

    private TreeNode GetT1() {
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode9 = new TreeNode(9);
        TreeNode treeNode20 = new TreeNode(20);
        TreeNode treeNode7 = new TreeNode(7);
        TreeNode treeNode15 = new TreeNode(15);

        treeNode3.left = treeNode9;
        treeNode3.right = treeNode20;

        treeNode20.right = treeNode7;
        treeNode20.left = treeNode15;

        return treeNode3;
    }

    @org.junit.jupiter.api.Test
    void minDepth() {
        BTreePrinter.printNode(GetT1());
        assertEquals(2, new Main().minDepth(GetT1()));
    }



}