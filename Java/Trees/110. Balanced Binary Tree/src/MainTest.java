import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/10/2018.
 */
class MainTest {

    public TreeNode GetT1() {
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode9 = new TreeNode(9);
        TreeNode treeNode20 = new TreeNode(20);
        TreeNode treeNode15 = new TreeNode(15);
        TreeNode treeNode7 = new TreeNode(7);

        treeNode3.left = treeNode9;
        treeNode3.right = treeNode20;

        treeNode20.left = treeNode15;
        treeNode20.right = treeNode7;

        return treeNode3;
    }

    @org.junit.jupiter.api.Test
    void isBalanced() {
        BTreePrinter.printNode(GetT1());

        assertEquals(true, new Main().isBalanced(GetT1()));
    }

}