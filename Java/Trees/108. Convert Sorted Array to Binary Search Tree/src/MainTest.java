import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 12/19/2018.
 */
class MainTest {

    public TreeNode GetT1() {
        TreeNode treeNode1 = new TreeNode(0);
        TreeNode treeNode2 = new TreeNode(-3);
        TreeNode treeNode4 = new TreeNode(9);
        TreeNode treeNode3 = new TreeNode(-10);
        TreeNode treeNode5 = new TreeNode(5);

        treeNode1.left = treeNode2;
        treeNode1.right = treeNode4;

        treeNode4.left = treeNode5;
        treeNode2.left = treeNode3;
        return treeNode1;
    }

    private int[] getArray() {
        return new int[]{-10, -3, 0, 5, 9};
    }

    @org.junit.jupiter.api.Test
    void sortedArrayToBST() {
        BTreePrinter.printNode(GetT1());
        TreeNode sortedNode = new Main().sortedArrayToBST(getArray());
        System.out.println("Sorted NODE");
        BTreePrinter.printNode(sortedNode);

        assertEquals(GetT1(), sortedNode);
    }

}