import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/23/2018.
 */
class MainTest {

    public TreeNode GetT1() {
        TreeNode treeNode4 = new TreeNode(4);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode7 = new TreeNode(7);
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode6 = new TreeNode(6);
        TreeNode treeNode9 = new TreeNode(9);

        treeNode4.left = treeNode2;
        treeNode4.right = treeNode7;

        treeNode7.left = treeNode6;
        treeNode7.right = treeNode9;

        treeNode2.left = treeNode1;
        treeNode2.right = treeNode3;

        return treeNode4;
    }

    public TreeNode GetT1_Small() {
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode3 = new TreeNode(3);

        treeNode2.left = treeNode1;
        treeNode2.right = treeNode3;

        return treeNode2;
    }

    public TreeNode GetT1_Small2() {
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode3 = new TreeNode(3);

        treeNode1.left = treeNode3;
        treeNode1.right = treeNode2;

        return treeNode1;
    }

    @org.junit.jupiter.api.Test
    void isValidBST() {
        BTreePrinter.printNode(GetT1());
        assertEquals(true, new Main().isValidBST(GetT1()));
    }

    @org.junit.jupiter.api.Test
    void isValidBST_SmallTru() {
        BTreePrinter.printNode(GetT1_Small());
        assertEquals(true, new Main().isValidBST(GetT1_Small()));
    }

    @org.junit.jupiter.api.Test
    void isValidBST_SmallFalse() {
        BTreePrinter.printNode(GetT1_Small2());
        assertEquals(false, new Main().isValidBST(GetT1_Small2()));
    }

    @org.junit.jupiter.api.Test
    void isValidBST2() {
        BTreePrinter.printNode(GetT1());
        assertEquals(true, new Main().isValidBST2(GetT1()));
    }
}