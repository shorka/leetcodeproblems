import java.util.LinkedList;
import java.util.Queue;

public class Main {

    public static void main(String[] args) {
boolean b = false && false;
        System.out.println("args = [" + b + "]");
    }

    public boolean isValidBST(TreeNode root) {

        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.add(root);

        while (!queue.isEmpty()) {
            TreeNode currNode = queue.poll();
            if (currNode == null)
                continue;

            if (currNode.left != null) {
                if (currNode.val <= currNode.left.val)
                    return false;

                queue.add(currNode.left);
            }


            if (currNode.right != null) {
                if (currNode.val >= currNode.right.val)
                    return false;

                queue.add(currNode.right);

            }

        }

        return true;
    }

    public boolean isValidBST2(TreeNode root) {
        return help(root, null, null);
    }

    public boolean isValidBST(TreeNode root, long minVal, long maxVal) {
        if (root == null)
            return true;

        if (root.val >= maxVal || root.val <= minVal)
            return false;

        return isValidBST(root.left, minVal, root.val) && isValidBST(root.right, root.val, maxVal);
    }

    private boolean help(TreeNode node, Integer low, Integer high) {
        if (node == null) return true;
        return (low == null || node.val > low) && (high == null || node.val < high) && help(node.left, low, node.val) && help(node.right, node.val, high);
    }

}
