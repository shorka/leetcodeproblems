import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }


    public TreeNode flatten(TreeNode root) {
        TreeNode node = flatten(root, null);
        BTreePrinter.printNode(node);
        return node;
    }

    private TreeNode flatten(TreeNode root, TreeNode prev) {
        if (root == null)
            return prev;

        prev = flatten(root.right, prev);
        prev = flatten(root.left, prev);
        root.right = prev;
        root.left = null;
        prev = root;
        return prev;
    }

    public TreeNode flattenDFS(TreeNode root) {
        if (root == null)
            return null;

        Stack<TreeNode> stack = new Stack<TreeNode>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode curr = stack.pop();
            if (curr.right != null)
                stack.push(curr.right);
            if (curr.left != null)
                stack.push(curr.left);
            if (!stack.isEmpty())
                curr.right = stack.peek();
            curr.left = null;  // dont forget this!!
        }
        BTreePrinter.printNode(root);
        return root;
    }
}
