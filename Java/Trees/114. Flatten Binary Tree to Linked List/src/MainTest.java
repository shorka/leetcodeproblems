import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/26/2018.
 */
class MainTest {

    public TreeNode GetT1() {
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode4 = new TreeNode(4);
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode5 = new TreeNode(5);
        TreeNode treeNode6 = new TreeNode(6);

        treeNode1.left = treeNode2;
        treeNode1.right = treeNode5;

        treeNode2.left = treeNode3;
        treeNode2.right = treeNode4;

        treeNode5.right = treeNode6;
        return treeNode1;
    }

    public TreeNode GetT1_right() {
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode4 = new TreeNode(4);
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode5 = new TreeNode(5);
        TreeNode treeNode6 = new TreeNode(6);

        treeNode1.right = treeNode2;
        treeNode2.right = treeNode3;
        treeNode3.right = treeNode4;
        treeNode4.right = treeNode5;
        treeNode5.right = treeNode6;

        return treeNode1;
    }

    @org.junit.jupiter.api.Test
    void flatten() {
//        BTreePrinter.printNode(GetT1());
//        BTreePrinter.printNode(GetT1_right());
        assertEquals(GetT1_right(),new Main().flatten(GetT1()));
    }
    @org.junit.jupiter.api.Test
    void flattenDFS1() {
//        BTreePrinter.printNode(GetT1());
//        BTreePrinter.printNode(GetT1_right());
        assertEquals(GetT1_right(),new Main().flattenDFS(GetT1()));
    }
}