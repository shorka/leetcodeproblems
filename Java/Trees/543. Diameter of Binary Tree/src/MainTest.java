import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/9/2018.
 */
class MainTest {

    public TreeNode GetT1() {
        TreeNode treeNode4 = new TreeNode(4);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode7 = new TreeNode(7);
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode6 = new TreeNode(6);
        TreeNode treeNode9 = new TreeNode(9);

        treeNode4.left = treeNode2;
//        treeNode4.right = treeNode7;

        treeNode7.left = treeNode6;
        treeNode7.right = treeNode9;

        treeNode2.left = treeNode1;
        treeNode2.right = treeNode3;

        return treeNode4;
    }

    @org.junit.jupiter.api.Test
    void diameterOfBinaryTree() {
        BTreePrinter.printNode(GetT1());
        assertEquals(3,new Main().diameterOfBinaryTree(GetT1()));
    }

}