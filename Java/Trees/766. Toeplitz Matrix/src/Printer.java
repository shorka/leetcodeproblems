/**
 * Created by Kirill on 2/16/2018.
 */
public class Printer {

    public static void printArr(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            int col = a[0].length;
//            System.out.println("i: " + i);
            for (int j = 0; j < col; j++) {
                System.out.printf("%5d ", a[i][j]);
            }
            System.out.println();
        }
    }

}
