public class Main {

    public static void main(String[] args) {
        Main m = new Main();
        int[][] matrix = new int[][]{{36,59,71,15,26,82,87},{56,36,59,71,15,26,82},{56,36,59,71,15,26,82}};
        Printer.printArr(matrix);
    }

    public boolean isToeplitzMatrix(int[][] matrix) {
        int rows = matrix.length;
        int columns = 0;

        for (int i = 0; i < rows - 1; i++) {

            if(i == 0)
                columns = matrix[0].length;

            for (int j = 0; j < columns; j++) {
                if(i > 0 && j> 0)
                    break;

                if(!isValidDiag(matrix,i,j,columns, rows))
                    return false;
            }
        }
        return true;
    }

    private boolean isValidDiag(int[][] matrix, int i, int j, int columns, int rows){
        for (int k = j; k < columns-1; k++) {
            if (matrix[i][k] != matrix[i + 1][k + 1])
                return false;

            if(i<rows-2)
                i++;

            else{
                return true;
            }
        }
        return true;
    }

    public boolean isToeplitzMatrix_L(int[][] matrix) {
        for (int i = 0; i < matrix.length - 1; i++) {
            for (int j = 0; j < matrix[i].length - 1; j++) {
                if (matrix[i][j] != matrix[i + 1][j + 1]) return false;
            }
        }
        return true;
    }
}
