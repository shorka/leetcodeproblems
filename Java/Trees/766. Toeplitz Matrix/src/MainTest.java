import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/16/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void isToeplitzMatrix1_true() {

        Main m = new Main();
        int[][] matrix = new int[][]{{1, 2, 3, 4},
                {5, 1, 2, 3},
                {9, 5, 1, 2}
        };

        assertEquals(true, m.isToeplitzMatrix(matrix));
    }

    @org.junit.jupiter.api.Test
    void isToeplitzMatrix1_true_L() {

        Main m = new Main();
        int[][] matrix = new int[][]{{1, 2, 3, 4},
                {5, 1, 2, 3},
                {9, 5, 1, 2}
        };
        Printer.printArr(matrix);
        assertEquals(true, m.isToeplitzMatrix_L(matrix));
    }

    @org.junit.jupiter.api.Test
    void isToeplitzMatrix1_false() {

        Main m = new Main();
        int[][] matrix = new int[][]{{1, 2},
                {1, 2}};

        assertEquals(false, m.isToeplitzMatrix(matrix));
    }

    @org.junit.jupiter.api.Test
    void isToeplitzMatrix3_false() {

        Main m = new Main();
        int[][] matrix = new int[][]{{83}, {64}, {57}
        };

        assertEquals(true, m.isToeplitzMatrix(matrix));
    }

    @org.junit.jupiter.api.Test
    void isToeplitzMatrix4_false() {

        Main m = new Main();
        int[][] matrix = new int[][]{{53,64,90,98,34},{91,53,64,90,98},{17,91,53,64,0}};
        Printer.printArr(matrix);
        assertEquals(false, m.isToeplitzMatrix(matrix));
    }

    @org.junit.jupiter.api.Test
    void isToeplitzMatrix5_false() {

        Main m = new Main();
        int[][] matrix = new int[][]{{36,59,71,15,26,82,87},{56,36,59,71,15,26,82},{15,0,36,59,71,15,26}};
        Printer.printArr(matrix);
        assertEquals(false, m.isToeplitzMatrix(matrix));
    }
}