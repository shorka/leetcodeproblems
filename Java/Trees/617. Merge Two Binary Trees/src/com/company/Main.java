package com.company;

public class Main {

    public TreeNode GetT1() {
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode5 = new TreeNode(5);

        treeNode1.left = treeNode3;
        treeNode1.right = treeNode2;
        treeNode3.left = treeNode5;
        return treeNode1;
    }

    public TreeNode GetT2() {
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode4 = new TreeNode(4);
        TreeNode treeNode7 = new TreeNode(7);

        treeNode2.left = treeNode1;
        treeNode2.right = treeNode3;

        treeNode1.right = treeNode4;
        treeNode3.right = treeNode7;

        return treeNode2;
    }

    public static void main(String[] args) {
        Main main = new Main();
        BTreePrinter.printNode(main.GetT1());
        System.out.println("");
        BTreePrinter.printNode(main.GetT2());

        System.out.println("MERGE");
//        BTreePrinter.printNode(main.mergeTrees2(main.GetT1(), main.GetT2()));
        BTreePrinter.printNode(new LeetcodeSolutions().mergeTrees2(main.GetT1(), main.GetT2()));
    }

    TreeNode mNode;

    public TreeNode mergeTrees(TreeNode t1, TreeNode t2) {

        if (t1 == null && t2 == null)
            return null;

//        mNode = new TreeNode(t1.val + t2.val);
        subMerge(null, t1, t2, true);
        return mNode;
    }

    private void subMerge(TreeNode node, TreeNode t1, TreeNode t2, boolean isLeft) {

        if (t1 == null && t2 == null)
            return;

        TreeNode newNode = new TreeNode((t1 ==null ? 0: t1.val) +  (t2 ==null ? 0: t2.val));

        if(node != null){
            if (isLeft)
                node.left = newNode;
            else
                node.right = newNode;
        }

        else{
            mNode = newNode;
        }


        subMerge(newNode,t1 ==null ? null: t1.left,
                t2 ==null ? null: t2.left, true);

        subMerge(newNode, t1 ==null ? null: t1.right,
                t2 ==null ? null: t2.right, false);
    }
}
