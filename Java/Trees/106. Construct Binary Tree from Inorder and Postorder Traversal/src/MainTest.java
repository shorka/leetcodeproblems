import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 7/2/2018.
 */
class MainTest {

    private TreeNode treeNode1(){

        TreeNode node9 = new TreeNode(9);
        TreeNode node3 = new TreeNode(3);
        TreeNode node15 = new TreeNode(15);
        TreeNode node20 = new TreeNode(20);
        TreeNode node7 = new TreeNode(7);

        node3.left = node9;
        node3.right = node20;
        node20.right = node7;
        node20.left = node15;

        return node3;
    }

    @org.junit.jupiter.api.Test
    void buildTree() {

        int[] post = new int[]{9,15,7,20,3};
        int[] inorder = new int[]{9,3,15,20,7};
        BTreePrinter.printNode(new Main().buildTree(inorder,post));
        assertEquals(treeNode1(), new Main().buildTree(inorder,post));
    }


    @org.junit.jupiter.api.Test
    void buildTree2() {

        int[] post = new int[]{9,15,7,20,3};
        int[] inorder = new int[]{9,3,15,20,7};
//        BTreePrinter.printNode(new Main().buildTree(inorder,post));
        assertEquals(treeNode1(), new Main().buildTree2(inorder,post));
    }
}