import java.util.*;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    int pInorder;   // index of inorder array
    int pPostorder; // index of postorder array

    public TreeNode buildTree(int[] inorder, int[] postorder) {
        pInorder = inorder.length - 1;
        pPostorder = postorder.length - 1;

        return buildTree(inorder, postorder, null);
    }

    private TreeNode buildTree(int[] inorder, int[] postorder, TreeNode end) {
        if (pPostorder < 0) {
            return null;
        }

        // create root node
        TreeNode root = new TreeNode(postorder[pPostorder--]);

        // if right node exist, create right subtree
        if (inorder[pInorder] != root.val) {
            root.right = buildTree(inorder, postorder, root);
        }

        pInorder--;

        // if left node exist, create left subtree
        if ((end == null) || (inorder[pInorder] != end.val)) {
            root.left = buildTree(inorder, postorder, end);
        }

        return root;
    }

    public TreeNode buildTree2(int[] inorder, int[] postorder) {
        if(inorder==null || postorder == null)
            return null;
        Map<Integer, Integer> indexes = new HashMap();
        for(int i =0; i<inorder.length; i++){
            indexes.put(inorder[i],i);
        }
        return helper(inorder, postorder,
                0,inorder.length-1,
                indexes,postorder.length-1 );
    }
    private TreeNode helper(int[] inorder, int[] postorder,
                            int iStart, int iEnd,
                            Map<Integer, Integer> indexes, int postIdx){

        if(postIdx<0 || iStart>iEnd)
            return null;

        int idx = indexes.get(postorder[postIdx]);
        TreeNode node = new TreeNode(postorder[postIdx]);
        node.right = helper(inorder, postorder,idx+1, iEnd,indexes,postIdx-1);
        node.left = helper(inorder, postorder, iStart, idx-1, indexes, postIdx- (iEnd-idx)-1);
        return node;
    }
}
