import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/23/2018.
 */
class MainTest {

    public char[][] Arr1() {
        char[][] arr = new char[][]{
                {'1', '1', '1', '1', '0'},
                {'1', '1', '0', '1', '0'},
                {'1', '1', '0', '0', '0'},
                {'0', '0', '0', '0', '0'}
        };
        return arr;
    }

    public char[][] Arr2() {
        char[][] arr = new char[][]{
                {'1', '1', '0', '0', '0'},
                {'1', '1', '0', '0', '0'},
                {'0', '0', '1', '0', '0'},
                {'0', '0', '0', '1', '1'}
        };
        return arr;
    }

    @org.junit.jupiter.api.Test
    void numIslands() {
        assertEquals(1, new Main().numIslands(Arr1()));
    }

    @org.junit.jupiter.api.Test
    void numIslands2() {
        assertEquals(3, new Main().numIslands(Arr2()));
    }

    @org.junit.jupiter.api.Test
    void numIslandsEmpty() {
        char[][] arr = new char[0][0];
        assertEquals(0, new Main().numIslands(arr));
    }
}