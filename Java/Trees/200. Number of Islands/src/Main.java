public class Main {

    public static void main(String[] args) {
        boolean b = true || false || false;
        System.out.println("args = [" + b + "]");
    }

    char[][] grid;
    boolean[][] seen;

    public int numIslands(char[][] grid) {
        this.grid = grid;
        if(grid.length ==0)
            return 0;

        seen = new boolean[grid.length][grid[0].length];
        int ans = 0;

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if(area(i,j)>0)
                    ans++;
            }
        }

        return ans;
    }

    private int area(int i, int j) {
        if (i < 0 || i >= grid.length || j < 0 || j >= grid[0].length ||
                seen[i][j] || grid[i][j] == '0'){
            return 0;
        }

        seen[i][j] = true;
        return (1 + area(i+1, j) + area(i-1, j)
                + area(i, j-1) + area(i, j+1));
    }
}
