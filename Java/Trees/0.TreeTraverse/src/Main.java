public class Main {

    private TreeNode treeNode1(){

        TreeNode node9 = new TreeNode(9);
        TreeNode node3 = new TreeNode(3);
        TreeNode node15 = new TreeNode(15);
        TreeNode node20 = new TreeNode(20);
        TreeNode node7 = new TreeNode(7);

        node3.left = node9;
        node3.right = node20;
        node20.right = node7;
        node20.left = node15;

        return node3;
    }

    public static void main(String[] args) {
	// write your code here
        Main main = new Main();
        BTreePrinter.printNode(main.treeNode1());
//        main.printInorder(main.treeNode1());
        main.postOrder(main.treeNode1());
    }

    void printInorder(TreeNode node)
    {
        if (node == null)
            return;

        /* first recur on left child */
        printInorder(node.left);

        /* then print the data of node */
        System.out.print(node.val + " ");

        /* now recur on right child */
        printInorder(node.right);
    }

    void postOrder(TreeNode node)
    {
        if (node == null)
            return;

        /* first recur on left child */
        postOrder(node.left);

        /* now recur on right child */
        postOrder(node.right);

                /* then print the data of node */
        System.out.print(node.val + " ");
    }
}
