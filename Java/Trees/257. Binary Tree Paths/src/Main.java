import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    List<String> listRes;

    public List<String> binaryTreePaths(TreeNode root) {

        listRes = new ArrayList<>();

        traverse(root, "");
        return listRes;
    }

    private void traverse(TreeNode node, String leaf) {

        if (node == null) {
            return;
        }

        if(leaf != ""){
            leaf +="->";
        }

        leaf += node.val;
        if(node.left == null && node.right == null){
            listRes.add(leaf);
            return;
        }

        traverse(node.left, leaf);
        traverse(node.right, leaf);
    }
}
