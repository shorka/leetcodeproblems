import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/10/2018.
 */
class MainTest {

    public TreeNode GetT1() {
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode5 = new TreeNode(5);

        treeNode1.left = treeNode2;
        treeNode1.right = treeNode3;

        treeNode2.right = treeNode5;

        treeNode1.right = treeNode3;

        return treeNode1;
    }

    @org.junit.jupiter.api.Test
    void binaryTreePaths() {

        BTreePrinter.printNode(GetT1());

        List<String> places = Arrays.asList("1->2->5", "1->3");
        assertEquals(places, new Main().binaryTreePaths(GetT1()));

    }

}