import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/14/2018.
 */
class MainTest {

    public TreeNode GetT1() {
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode0 = new TreeNode(0);
        TreeNode treeNode2 = new TreeNode(2);

        treeNode1.left = treeNode0;
        treeNode1.right = treeNode2;

        return treeNode1;
    }

    public TreeNode GetT1_Cutted() {
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(2);

        treeNode1.right = treeNode2;
        return treeNode1;
    }

    @org.junit.jupiter.api.Test
    void trimBST_simple() {
        Main m = new Main();
        BTreePrinter.printNode(GetT1());
        assertEquals(GetT1_Cutted(), m.trimBST(GetT1(), 1, 2));
    }

}