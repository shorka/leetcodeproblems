import java.util.LinkedList;
import java.util.Queue;

public class Main {

    public TreeNode GetT1() {
        TreeNode treeNode4 = new TreeNode(4);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode7 = new TreeNode(7);
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode6 = new TreeNode(6);
        TreeNode treeNode9 = new TreeNode(9);

        treeNode4.left = treeNode2;
        treeNode4.right = treeNode7;

        treeNode7.left = treeNode6;
        treeNode7.right = treeNode9;

        treeNode2.left = treeNode1;
        treeNode2.right = treeNode3;

        return treeNode4;
    }

    public TreeNode GetT2() {
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode0 = new TreeNode(0);
        TreeNode treeNode4 = new TreeNode(4);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode1 = new TreeNode(1);

        treeNode3.left = treeNode0;
        treeNode3.right = treeNode4;

        treeNode0.right = treeNode2;

        treeNode2.left = treeNode1;
        return treeNode3;
    }

    public static void main(String[] args) {
        Main m = new Main();
        BTreePrinter.printNode(m.GetT2());
        BTreePrinter.printNode(m.trimBST(m.GetT2(),1,3));
    }

    int L, R = 0;
    public TreeNode trimBST(TreeNode root, int L, int R) {
        this.L = L;
        this.R = R;
        return  trimBSTRec(root);
    }


    private TreeNode trimBSTRec(TreeNode root) {
        BTreePrinter.printNode(root);

        if (root == null)
            return root;

        if (root.val > R) return trimBSTRec(root.left);
        if (root.val < L) return trimBSTRec(root.right);

        root.left = trimBSTRec(root.left);
        root.right = trimBSTRec(root.right);
        return root;
    }
}
