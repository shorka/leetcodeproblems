import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/11/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void letterCombinations() {

        List<String> temp1 = new LinkedList<String>(Arrays.asList("0", "1", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"));

        assertEquals(temp1, new Main().letterCombinations("23"));

    }

}