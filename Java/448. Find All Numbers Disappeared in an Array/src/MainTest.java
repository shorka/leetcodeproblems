import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/15/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void findDisappearedNumbers() {
        List<Integer> list = new ArrayList<Integer>();
        list.add(5);
        list.add(6);

        int[] arr = new int[]{4,3,2,7,8,2,3,1};
        assertEquals(list, new Main().findDisappearedNumbers(arr));
    }

}