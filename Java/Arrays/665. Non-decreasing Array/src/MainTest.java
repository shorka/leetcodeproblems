import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/17/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void checkPossibility1() {

        assertEquals(true, new Main().checkPossibility(new int[]{4,2,3}));
    }

    @org.junit.jupiter.api.Test
    void checkPossibility2() {

        assertEquals(false, new Main().checkPossibility(new int[]{4,2,1}));
    }

    @org.junit.jupiter.api.Test
    void checkPossibility3() {

        assertEquals(false, new Main().checkPossibility(new int[]{3,4,2,3}));
    }

    @org.junit.jupiter.api.Test
    void checkPossibility4() {

        assertEquals(true, new Main().checkPossibility(new int[]{2,3,3,2,4}));
    }

    @org.junit.jupiter.api.Test
    void checkPossibility5() {

        assertEquals(true, new Main().checkPossibility2(new int[]{2,3,3,2,4}));
    }

    @org.junit.jupiter.api.Test
    void checkPossibility6() {

        assertEquals(false, new Main().checkPossibility2(new int[]{3,4,2,3}));
    }

    @org.junit.jupiter.api.Test
    void checkPossibility7() {

        assertEquals(false, new Main().checkPossibility2(new int[]{4,2,1}));
    }
}