public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public boolean checkPossibility(int[] nums) {

        boolean wasChanged = false;
        for (int i = 0; i < nums.length - 1; i++) {

            int diff = nums[i] - nums[i + 1];
            if (diff <= 0)
                continue;

            int num = nums[i] - diff;
            if (num < 1)
                return false;

            if (!wasChanged) {
                nums[i] = num;
                wasChanged = true;
            }
            else
                return false;
        }


        for (int i = 0; i < nums.length-1; i++) {

            if(nums[i] > nums[i+1])
                return false;
        }

        return true;
    }

    public boolean checkPossibility2(int[] nums) {
        int cnt = 0;                                                                    //the number of changes
        for(int i = 1; i < nums.length && cnt<=1 ; i++){
            if(nums[i-1] > nums[i]){
                cnt++;
                if(i-2<0 || nums[i-2] <= nums[i])
                    nums[i-1] = nums[i];
                //modify nums[i-1] of a priority
                else
                    nums[i] = nums[i-1];                                                //have to modify nums[i]
            }
        }
        return cnt<=1;
    }
}
