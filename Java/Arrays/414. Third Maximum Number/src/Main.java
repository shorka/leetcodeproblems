import java.lang.reflect.Array;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
	// write your code here
    }

    public int thirdMax(int[] nums) {

        Arrays.sort(nums);

        int k = 3;
        int max = 0;
        int uniqCount = 0;
        for (int i = nums.length-1; i >= 0 ; i--) {

            if(i == nums.length-1)
                max = nums[i];

            if(i == 0 || nums[i] != nums[i-1])
                uniqCount++;

            if(uniqCount == k)
                return nums[i];
        }


        return max;
    }
}
