import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 7/1/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void thirdMax() {

        int[] arr = new int[]{2, 2, 3, 1};

        assertEquals(1, new Main().thirdMax(arr));

    }

}