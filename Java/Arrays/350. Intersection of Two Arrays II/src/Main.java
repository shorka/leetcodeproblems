import java.util.*;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public int[] intersect(int[] nums1, int[] nums2) {

        Map<Integer, Integer> map1 = new HashMap<>();
        for (int i = 0; i < nums1.length; i++) 
            map1.put(nums1[i], map1.getOrDefault(nums1[i],0) + 1);

        Map<Integer, Integer> map2 = new HashMap<>();
        for (int i = 0; i < nums2.length; i++)
            map2.put(nums2[i], map2.getOrDefault(nums2[i],0) + 1);

        List<Integer> list = new ArrayList<>(nums2.length);
        for (Map.Entry<Integer, Integer> entry : map1.entrySet()){

            int key = entry.getKey();
            if(!map2.containsKey(key))
                continue;

            int times = Math.min(entry.getValue(), map2.get(key));
            for (int i = 0; i < times; i++)
                list.add(key);
        }

        int i = 0;
        int[] result = new int[list.size()];
        for (Integer num : list) result[i++] = num;

        return result;
    }
    public int[] intersect_leet(int[] nums1, int[] nums2) {
        List<Integer> res = new ArrayList<Integer>();
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        for(int i = 0, j = 0; i< nums1.length && j<nums2.length;){
            if(nums1[i]<nums2[j]){
                i++;
            }
            else if(nums1[i] == nums2[j]){
                res.add(nums1[i]);
                i++;
                j++;
            }
            else{
                j++;
            }
        }
        int[] result = new int[res.size()];
        for(int i = 0; i<res.size();i++){
            result[i] = res.get(i);
        }
        return result;
    }

}
