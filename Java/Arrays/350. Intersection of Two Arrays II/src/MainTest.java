import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/28/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void intersect() {
        int[] arr1 = new int[]{1, 2, 2, 1};
        int[] arr2 = new int[]{2,2};
        int[] res = new int[]{2,2};

        assertEquals(res, new Main().intersect(arr1,arr2));
    }

    @org.junit.jupiter.api.Test
    void intersect1() {
        int[] arr1 = new int[]{1, 2};
        int[] arr2 = new int[]{1,1};
        int[] res = new int[]{1};

        assertEquals(res, new Main().intersect(arr1,arr2));
    }

    @org.junit.jupiter.api.Test
    void intersect12() {
        int[] arr1 = new int[]{2, 1};
        int[] arr2 = new int[]{1,2};
        int[] res = new int[]{1,2};

        assertEquals(res, new Main().intersect(arr1,arr2));
    }

}