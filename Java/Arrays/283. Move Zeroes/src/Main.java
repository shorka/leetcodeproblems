import java.util.LinkedList;
import java.util.Queue;

public class Main {

    public static void main(String[] args) {
        // write your code here

//        int[] array = new int[]{0, 1, 0, 3, 12};
        int[] array = new int[]{1, 0, 0, 3, 12};
        new Main().moveZeroesLeet(array);
    }

    public void moveZeroes(int[] nums) {

        Queue<Integer> queue = new LinkedList<Integer>();
        int countZeros = 0;
        for (int i = 0; i < nums.length; i++) {

            int iNumb = nums[i];
            if (iNumb != 0)
                queue.add(iNumb);

            else
                countZeros++;
        }

        for (int i = 0; i < nums.length - countZeros; i++) {
            nums[i] = queue.poll();
        }

        for (int i = nums.length - countZeros; i < nums.length; i++) {
            nums[i] = 0;
        }
    }

    public void moveZeroesLeet(int[] nums) {
        int leftMostZeroIndex = 0; // The index of the leftmost zero
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0)
                continue;

            if (i > leftMostZeroIndex) { // There are zero(s) on the left side of the current non-zero number, swap!
                nums[leftMostZeroIndex] = nums[i];
                nums[i] = 0;
            }

            leftMostZeroIndex++;
        }

    }
}
