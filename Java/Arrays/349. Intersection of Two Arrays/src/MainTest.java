import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/28/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void intersection2() {
        int[] arr1 = new int[]{1, 2, 2, 1};
        int[] arr2 = new int[]{2,2};
        int[] res = new int[]{2};

        assertEquals(res, new Main().intersection(arr1,arr2));
    }

}