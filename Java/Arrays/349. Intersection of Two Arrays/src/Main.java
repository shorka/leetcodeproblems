import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public int[] intersection(int[] nums1, int[] nums2) {

        Arrays.sort(nums1);
        Arrays.sort(nums2);

        int len = Math.max(nums1.length, nums2.length);
        int[] max = len == nums1.length ? nums1 : nums2;
        int[] min = len == nums1.length ? nums2 : nums1;

        int prevNumb = 0;
        List<Integer> list = new ArrayList<>(min.length);
        for (int i = 0; i < len; i++) {

            int numbI = max[i];
            if (i > 0){
                if (numbI == prevNumb)
                    continue;
            }

            for (int j = 0; j < min.length; j++) {
                if (numbI != min[j])
                    continue;

                if (!list.contains(numbI)) {
                    list.add(numbI);
                    break;
                }
            }

            prevNumb = numbI;
        }

        int[] array = new int[list.size()];
        for(int i = 0; i < list.size(); i++) array[i] = list.get(i);

        return array;
    }
}
