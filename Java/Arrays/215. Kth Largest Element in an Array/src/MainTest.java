import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 7/1/2018.
 */
class MainTest {
    @org.junit.jupiter.api.Test
    void findKthLargest() {

        int[] arr = new int[]{3,2,1,5,6,4};

        assertEquals(5, new Main().findKthLargest(arr,2));
    }

    @org.junit.jupiter.api.Test
    void findKthLargest_Leet() {

        int[] arr = new int[]{3,2,1,5,6,4};

        assertEquals(5, new Main().findKthLargest_Leet(arr,2));
    }
}