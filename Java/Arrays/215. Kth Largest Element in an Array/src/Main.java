
//Find the kth largest element in an unsorted array.
//Note that it is the kth largest element in the sorted order, not the kth distinct element.

import java.util.Arrays;
import java.util.PriorityQueue;

public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public int findKthLargest(int[] nums, int k) {

        Arrays.sort(nums);

        int count = 1;
        for (int i = nums.length - 1; i >= 0; i--) {

            if (count == k)
                return nums[i];

            count++;
        }

        return 0;
    }

    public int findKthLargest_Leet(int[] nums, int k) {

        final PriorityQueue<Integer> pq = new PriorityQueue<>();
        for (int val : nums) {
            pq.offer(val);

            if (pq.size() > k) {
                Integer numbPoll = pq.poll();
                int a = 4;
            }
        }
        return pq.peek();
    }
}
