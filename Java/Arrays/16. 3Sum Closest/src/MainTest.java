import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/30/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void threeSumClosest() {

        int[] nums = new int[]{-1, 2, 1, -4};
        assertEquals(2, new Main().threeSumClosest(nums,1));
    }

}