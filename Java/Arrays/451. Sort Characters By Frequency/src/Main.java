import java.util.*;

public class Main {

    public static void main(String[] args) {

    }

    public String frequencySort(String s) {

        HashMap<Character, Integer> map = new HashMap<>();
//        Integer[] arr = new Integer[26];
//        for (int i = 0; i < arr.length; i++) {
//            arr[i] = 0;
//        }

        for (int i = 0; i < s.length(); i++) {
            char iChar = s.charAt(i);
            if(map.containsKey(iChar)){
                map.put(iChar, map.get(iChar)+1);
            }
            else
                map.put(iChar,1);
        }

//        Arrays.sort(arr, Collections.reverseOrder());
//        String strRes = "";
//        for (int i = 0; i < arr.length; i++) {
//
//            int iNum = arr[i];
//            if(iNum == 0)
//                break;
//
//            char iChar = ' ';
//            for (int j = 0; j < s.length(); j++) {
//                if(s.charAt(i) == i)
//            }
//
//            for (int j = 0; j < iNum; j++) {
//                strRes += iChar;
//            }
//        }

        return "";
    }


    public String frequencySortL(String s) {
        Map<Character, Integer> map = new HashMap<>();
        for (char c : s.toCharArray()) {
            if (map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
            } else {
                map.put(c, 1);
            }
        }
        List<Character> [] bucket = new List[s.length() + 1];
        for (char key : map.keySet()) {
            int frequency = map.get(key);
            if (bucket[frequency] == null) {
                bucket[frequency] = new ArrayList<>();
            }
            bucket[frequency].add(key);
        }
        StringBuilder sb = new StringBuilder();
        for (int pos = bucket.length - 1; pos >=0; pos--) {
            if (bucket[pos] == null) {
                continue;
            }
            for (char num : bucket[pos]) {
                for (int i = 0; i < map.get(num); i++) {
                    sb.append(num);
                }
            }
        }
        return sb.toString();
    }
}
