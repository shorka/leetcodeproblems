import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 6/7/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void detectCapitalUse_USA() {

        assertEquals(true, new Main().detectCapitalUse("USA"));
    }

    @org.junit.jupiter.api.Test
    void detectCapitalUse_leetcode() {

        assertEquals(true, new Main().detectCapitalUse("leetcode"));
    }

    @org.junit.jupiter.api.Test
    void detectCapitalUse_FlaG() {

        assertEquals(false, new Main().detectCapitalUse("FlaG"));
    }

    @org.junit.jupiter.api.Test
    void detectCapitalUse_mL() {

        assertEquals(false, new Main().detectCapitalUse("mL"));
    }

    @org.junit.jupiter.api.Test
    void detectCapitalUse_cVe() {

        assertEquals(false, new Main().detectCapitalUse("cVe"));
    }
}