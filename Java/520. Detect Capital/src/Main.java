public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public boolean detectCapitalUse(String word) {

        char[] arr = word.toCharArray();

        boolean isPrevCapital = false;
        for (int i = 0; i < arr.length; i++) {

            char iCh = arr[i];

            if (i == 1 && i < arr.length - 1) {

                if (Character.isUpperCase(iCh) && isPrevCapital)
                    return word == word.toUpperCase();
            }


            if (i > 0 && Character.isUpperCase(iCh) && !isPrevCapital) {
                return false;
            }



            isPrevCapital = Character.isUpperCase(iCh);
        }

        return true;
    }

    public boolean detectCapitalUseLeet(String word) {
        return word.equals(word.toUpperCase()) ||
                word.equals(word.toLowerCase()) ||
                Character.isUpperCase(word.charAt(0)) &&
                        word.substring(1).equals(word.substring(1).toLowerCase());
    }
}
