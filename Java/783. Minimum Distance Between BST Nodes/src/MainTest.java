import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/10/2018.
 */
class MainTest {

    public TreeNode GetT1() {
        TreeNode treeNode4 = new TreeNode(4);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode6 = new TreeNode(6);
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode3 = new TreeNode(3);

        treeNode4.left = treeNode2;
        treeNode4.right = treeNode6;

        treeNode2.left = treeNode1;
        treeNode2.right = treeNode3;

        return treeNode4;
    }

    public TreeNode GetT2() {
        TreeNode treeNode4 = new TreeNode(27);
        TreeNode treeNode2 = new TreeNode(34);
        TreeNode treeNode6 = new TreeNode(58);
        TreeNode treeNode1 = new TreeNode(50);
        TreeNode treeNode3 = new TreeNode(44);

        treeNode4.right = treeNode2;
        treeNode2.right = treeNode6;
        treeNode6.left = treeNode1;
        treeNode1.left = treeNode3;

        return treeNode4;
    }

    @org.junit.jupiter.api.Test
    void minDiffInBST1() {
        assertEquals(1, new Main().minDiffInBST(GetT1()));
    }

    @org.junit.jupiter.api.Test
    void minDiffInBST6() {
//        BTreePrinter.printNode(GetT2());
        assertEquals(6, new Main().minDiffInBST(GetT2()));
    }
}