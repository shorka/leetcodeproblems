import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class Main {

    public TreeNode GetT1() {
        TreeNode treeNode4 = new TreeNode(4);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode6 = new TreeNode(6);
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode3 = new TreeNode(3);

        treeNode4.left = treeNode2;
        treeNode4.right = treeNode6;

        treeNode2.left = treeNode1;
        treeNode2.right = treeNode3;

        return treeNode4;
    }

    public static void main(String[] args) {
        Main m = new Main();
        BTreePrinter.printNode(m.GetT1());

        System.out.println("minDiff: " + m.minDiffInBST(m.GetT1()));
    }

    public int minDiffInBST2(TreeNode root) {
        if (root == null)
            return 0;

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        int minDiff = Integer.MAX_VALUE;
        while (!queue.isEmpty()) {

            TreeNode curr = queue.poll();
            if (curr == null)
                continue;

            int diffLeft = 0;
            if (curr.left != null) {
                diffLeft = Math.abs(curr.val - curr.left.val);
                queue.add(curr.left);
            } else
                diffLeft = Integer.MAX_VALUE;

            int diffRight = 0;
            if (curr.right != null) {
                diffRight = Math.abs(curr.val - curr.right.val);
                queue.add(curr.right);
            } else
                diffRight = Integer.MAX_VALUE;

            int diff = Math.min(diffLeft, diffRight);

            if (diff < minDiff && diff != 0)
                minDiff = diff;

        }

        return minDiff;
    }

    public int minDiffInBST(TreeNode root) {
        if (root == null)
            return 0;

        Set<Integer> set = new HashSet();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        int minDiff = Integer.MAX_VALUE;
        while (!queue.isEmpty()) {

            TreeNode curr = queue.poll();
            if (curr == null)
                continue;

            set.add(curr.val);
            int tmpMin = findMinDiff(set,curr.val);
            if(tmpMin < minDiff)
                minDiff = tmpMin;

            if (curr.left != null) {
                queue.add(curr.left);
            }

            if (curr.right != null) {
                queue.add(curr.right);
            }
        }

        return minDiff;
    }

    private int findMinDiff(Set<Integer> set, int numb) {

        int min = Integer.MAX_VALUE;
        for (Integer item : set) {
            if(item == numb)
                continue;

            int tmp = Math.abs(numb - item);
            if (tmp < min)
                min = tmp;
        }
        return  min;
    }
}
