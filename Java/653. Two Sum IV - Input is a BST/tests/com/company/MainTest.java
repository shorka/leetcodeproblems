package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/6/2018.
 */
class MainTest {

    public TreeNode GetTreeNode1() {
        TreeNode treeNode5 = new TreeNode(5);
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode6 = new TreeNode(6);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode4 = new TreeNode(4);
        TreeNode treeNode7 = new TreeNode(7);

        treeNode5.left = treeNode3;
        treeNode5.right = treeNode6;

        treeNode3.left = treeNode2;
        treeNode3.right = treeNode4;

        treeNode6.right = treeNode7;

        return treeNode5;
    }

    @Test
    void testFind9() {
        Main main = new Main();
        assertEquals(true, main.findTarget(GetTreeNode1(), 9));
    }


}