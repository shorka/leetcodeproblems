public class Main {

    public static void main(String[] args) {
        int[] arr = {7, 1, 5, 3, 6, 4};
        int[] arr2 = {7, 6, 4, 3, 1};
        int[] arr3 = {0, 6, -3, 7};

        System.out.println("MaxProfit: " + new Main().maxProfitOnePass(arr3));
    }

    public int maxProfitBrutte(int[] prices) {

        int maxDiff = 0;
        //buy
        for (int i = 0; i < prices.length-1; i++) {

            int buyPrice = prices[i];
            //sell
            for (int j = i+1; j < prices.length; j++) {

                int diff = prices[j] - buyPrice;
                if( diff > maxDiff)
                {
                    maxDiff = diff;
                }
            }
        }
        return  maxDiff;
    }

    public int maxProfitOnePass(int[] prices) {

        int minprice = Integer.MAX_VALUE;
        int maxprofit = 0;
        for (int i = 0; i < prices.length; i++) {
            if (prices[i] < minprice)
                minprice = prices[i];

            else if (prices[i] - minprice > maxprofit)
                maxprofit = prices[i] - minprice;
        }
        return maxprofit;
    }


}
