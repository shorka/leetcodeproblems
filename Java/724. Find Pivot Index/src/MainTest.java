import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Kirill on 2/17/2018.
 */
class MainTest {

    @org.junit.jupiter.api.Test
    void pivotIndex_3() {
        int[] arr = new int[]{1, 7, 3, 6, 5, 6};
        assertEquals(3, new Main().pivotIndex(arr));
    }

    @org.junit.jupiter.api.Test
    void pivotIndex_Minus1() {
        int[] arr = new int[]{1, 2, 3};
        assertEquals(-1, new Main().pivotIndex(arr));
    }

    @org.junit.jupiter.api.Test
    void pivotIndex_Plus1() {
        int[] arr = new int[]{-1,-1,-1,-1,0,1};
        assertEquals(1, new Main().pivotIndex(arr));
    }

    @org.junit.jupiter.api.Test
    void pivotIndex_0() {
        int[] arr = new int[]{-1,-1,-1,0,1,1};
        assertEquals(0, new Main().pivotIndex(arr));
    }
}